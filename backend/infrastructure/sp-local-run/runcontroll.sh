#!/usr/bin/env bash

KEYCLOAK_HOST=http://localhost:8180

if [ $1 = "up" ]; then
    docker-compose up -d
    until $(curl --output /dev/null --silent --head --fail ${KEYCLOAK_HOST}); do
        printf '.'
        sleep 2
    done
    echo "# Keycloak is up"
    docker-compose exec sp-keycloak sh /opt/jboss/create-keycloakContainer-user.sh

else
    docker-compose $@
fi


