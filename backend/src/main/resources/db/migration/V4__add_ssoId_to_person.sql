ALTER TABLE person
    ADD COLUMN sso_id uuid not null;

ALTER TABLE person
    add constraint const_person_unique_sso_id unique (sso_id);

