ALTER TABLE participation
    ADD COLUMN participating_club_id bigint not null;
ALTER TABLE participation
    ADD CONSTRAINT fk_participation_club FOREIGN KEY (participating_club_id) REFERENCES club (id);

