alter table competition_type
    add constraint const_competition_type_unique_name unique (name);

alter table entry_fees
    add constraint const_entry_fees_unique_label_for_competition unique (label, competition_id);

