alter table participation
    drop constraint const_participation_unique_combination;

alter table participation
    add constraint const_participation_unique_combination unique (competition_id, participant_id);