package de.nidomiro.sportpoint.backend.api.graphql.error

import graphql.ErrorType
import graphql.GraphQLError
import graphql.language.SourceLocation
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ExceptionHandler

@Component
class GraphQLErrorHandler {

    @ExceptionHandler(MyGraphQLExecutionException::class)
    fun handleMyGraphQlErrorWrapper(ex: MyGraphQLExecutionException): GraphQLError {
        val error = ex.error

        return object : GraphQLError {

            override fun getMessage(): String {
                return error.message
            }

            override fun getLocations(): List<SourceLocation>? {

                return ex.dataFetchEnv?.mergedField?.fields?.mapNotNull { it?.sourceLocation }
            }

            override fun getPath(): MutableList<Any>? {
                return ex.dataFetchEnv?.executionStepInfo?.path?.toList()
            }

            override fun getErrorType(): ErrorType {
                return ErrorType.DataFetchingException
            }

            override fun getExtensions(): Map<String, Any> {
                return mapOf("errorCode" to error.errorCode)
            }
        }
    }
}