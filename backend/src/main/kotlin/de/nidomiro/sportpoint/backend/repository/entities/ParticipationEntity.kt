package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "participation")
data class ParticipationEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    @NotNull
    @OneToOne(optional = false)
    var competition: CompetitionEntity,

    @NotNull
    @OneToOne(optional = false)
    var participant: PersonEntity,

    @NotNull
    @ManyToOne(optional = false)
    var participatingSportsClass: SportsClassEntity,

    @NotNull
    @ManyToOne(optional = false)
    var participatingClub: ClubEntity,

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : GenericEntity<Long> {
    companion object {
        fun idOnly(id: Long?) = ParticipationEntity(
            id = id,
            competition = CompetitionEntity.idOnly(null),
            participant = PersonEntity.idOnly(null),
            participatingSportsClass = SportsClassEntity.idOnly(null),
            participatingClub = ClubEntity.idOnly(null),
            isIdOnlyEntity = true
        )
    }
}