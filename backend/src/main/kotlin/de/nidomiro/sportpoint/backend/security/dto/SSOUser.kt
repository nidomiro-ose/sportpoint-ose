package de.nidomiro.sportpoint.backend.security.dto

import java.time.LocalDate
import java.util.*

data class SSOUser(
    val id: UUID,
    val email: String,
    val firstName: String,
    val name: String,
    val birthdate: LocalDate
)