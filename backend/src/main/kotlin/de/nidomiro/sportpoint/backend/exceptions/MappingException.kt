package de.nidomiro.sportpoint.backend.exceptions

open class MappingException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause) {
    companion object {
        fun cannotBeNull(fieldName: String) = NotNullableFieldMappingException("$fieldName cannot be null")

        fun entityNotFound(entityName: String) = EntityNotFoundMappingException("$entityName could not be found")

    }
}

class NotNullableFieldMappingException(message: String? = null, cause: Throwable? = null) :
    MappingException(message, cause)

class EntityNotFoundMappingException(message: String? = null, cause: Throwable? = null) :
    MappingException(message, cause)