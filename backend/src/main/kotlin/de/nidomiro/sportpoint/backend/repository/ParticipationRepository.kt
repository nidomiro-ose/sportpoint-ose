package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.ParticipationEntity
import org.springframework.stereotype.Repository

@Repository
interface ParticipationRepository : SimpleRepository<ParticipationEntity, Long> {

    fun findByCompetitionId(id: Long): List<ParticipationEntity>

}