package de.nidomiro.sportpoint.backend.service.dto.input

data class CreateSportsClassInputDto(
    val name: String
) : InputDto