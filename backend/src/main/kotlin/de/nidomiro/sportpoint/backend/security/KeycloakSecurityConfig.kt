package de.nidomiro.sportpoint.backend.security

import de.nidomiro.sportpoint.backend.service.PersonService
import org.keycloak.adapters.KeycloakConfigResolver
import org.keycloak.adapters.springboot.KeycloakBaseSpringBootConfiguration
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties
import org.keycloak.adapters.springsecurity.KeycloakConfiguration
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter
import org.keycloak.adapters.springsecurity.management.HttpSessionManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper
import org.springframework.security.core.session.SessionRegistry
import org.springframework.security.core.session.SessionRegistryImpl
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy
import org.springframework.security.web.csrf.CookieCsrfTokenRepository
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.web.cors.CorsConfiguration


@KeycloakConfiguration
@EnableConfigurationProperties(KeycloakSpringBootProperties::class)
internal class KeycloakSecurityConfig(
    private val personService: PersonService
) : KeycloakWebSecurityConfigurerAdapter() {

    private var _csrfTokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse()

    @Bean
    fun csrfTokenRepository(): CsrfTokenRepository {
        return _csrfTokenRepository
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        super.configure(http)

        val corsConfig = CorsConfiguration()
        corsConfig.allowCredentials = true
        corsConfig.addAllowedOrigin("*")
        corsConfig.addAllowedHeader("*")
        corsConfig.addAllowedMethod("*")

        http.cors().configurationSource { corsConfig }

        http
            .addFilterAfter(UpdateUserFilter(personService), FilterSecurityInterceptor::class.java)
            .csrf().disable()
            //.csrf().csrfTokenRepository(_csrfTokenRepository).and() // disabled until graphql-java-kickstart:5.9.1 is available
            .authorizeRequests()
            .antMatchers("/graphql/**", "/subscriptions/**").permitAll()
            .antMatchers("/graphiql/**", "/vendor/**").permitAll()

            .antMatchers("/sso**", "/sso/**").permitAll()
            .antMatchers("/favicon*").permitAll()

            .anyRequest().denyAll()

    }

    /**
     * Use Keycloak configuration from properties / yaml
     *
     * @return
     */
    @Bean
    fun keycloakConfigResolver(): KeycloakConfigResolver {
        return KeycloakSpringBootConfigResolver()
    }

    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder) {

        val keycloakAuthenticationProvider = keycloakAuthenticationProvider()

        val grantedAuthorityMapper = SimpleAuthorityMapper()
        grantedAuthorityMapper.setPrefix("ROLE_")
        grantedAuthorityMapper.setConvertToUpperCase(true)
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper)
        auth.authenticationProvider(keycloakAuthenticationProvider)
    }

    @Bean
    override fun sessionAuthenticationStrategy(): SessionAuthenticationStrategy {
        return RegisterSessionAuthenticationStrategy(SessionRegistryImpl() as SessionRegistry?)
    }

    /**
     * Ensures the correct registration of KeycloakSpringBootConfigResolver when Keycloaks AutoConfiguration
     * is explicitly turned off in application.yml `keycloak.enabled: false`.
     */
    @Configuration
    internal class CustomKeycloakBaseSpringBootConfiguration : KeycloakBaseSpringBootConfiguration() {

        fun setKeycloakConfigResolvers(configResolver: KeycloakConfigResolver) {
            // NOOP avoids recursive calls to setKeycloakConfigResolvers
        }
    }

    @Bean
    @ConditionalOnMissingBean(HttpSessionManager::class)
    override fun httpSessionManager() = HttpSessionManager()
}