package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.repository.EntryFeeRepository
import de.nidomiro.sportpoint.backend.repository.entities.EntryFeeEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateEntryFeeInputDto
import org.springframework.stereotype.Component


@Component
class EntryFeeService(
    inputMappingRegister: InputMappingRegister,
    repository: EntryFeeRepository,
    private val competitionService: CompetitionService
) : BasicService<Long, EntryFeeEntity, CreateEntryFeeInputDto, EntryFeeRepository>(
    entityClass = EntryFeeEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {
    fun findByCompetitionId(id: Long) = repository.findByCompetitionId(id)

    override fun checkAndFetchAttributeValuesFromDb(entity: EntryFeeEntity): CallResult<EntryFeeEntity, EntityServiceError> {

        entity.competition = competitionService.findById(entity.competition.id)
            ?: return EntityServiceError.NotFound("No Competition with id '${entity.competition.id}' was found")
                .toFailure()

        return CallResult.Success(entity)
    }
}
