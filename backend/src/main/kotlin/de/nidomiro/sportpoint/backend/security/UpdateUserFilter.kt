package de.nidomiro.sportpoint.backend.security

import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.security.dto.SSOUser
import de.nidomiro.sportpoint.backend.service.PersonService
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

class UpdateUserFilter(
    private val personService: PersonService
) : GenericFilterBean() {


    override fun doFilter(servletRequest: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {

        val request = servletRequest as? HttpServletRequest
        val ssoUser = request?.userPrincipalToSsoUser
        servletRequest?.setAttribute(ATTRIBUTE_SSO_USER, ssoUser)


        val session = request?.getSession(false)
        if (session != null && ssoUser != null) {
            try {
                val loggedInUser = updateUserDataInDb(session, ssoUser)
                servletRequest.loggedInUser = loggedInUser
            } catch (e: IllegalStateException) {
                logger.warn(e)
            }
        }


        chain?.doFilter(request, response)
    }

    private fun updateUserDataInDb(session: HttpSession, ssoUser: SSOUser): PersonEntity? {

        val sessionSSOUser = session.getAttribute(ATTRIBUTE_SSO_USER) as? SSOUser

        val loggedInUser =
            if (sessionSSOUser == null || sessionSSOUser != ssoUser) {
                personService.updateOrCreate(ssoUser)
            } else null

        session.setAttribute(ATTRIBUTE_SSO_USER, ssoUser)

        loggedInUser?.let {
            when (it) {
                is CallResult.Success -> session.setAttribute(ATTRIBUTE_LOGGED_IN_USER, it.value)
                is CallResult.Failure -> logger.warn("Error while 'updateUserDataInDb': $it")
            }
        }

        return session.getAttribute(ATTRIBUTE_LOGGED_IN_USER) as? PersonEntity
    }

}