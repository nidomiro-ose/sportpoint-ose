package de.nidomiro.sportpoint.backend.repository.entities

interface IdentifiableEntity<T> {
    val id: T?
}