package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.PersonSportsClassMappingEntity
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.service.dto.input.person.AddSportsClassToPersonInputDto
import org.springframework.stereotype.Component

@Component
class AddSportsClassToPersonInputMapper : InputMapper<AddSportsClassToPersonInputDto, PersonSportsClassMappingEntity> {
    override val inputClass = AddSportsClassToPersonInputDto::class
    override val outputClass = PersonSportsClassMappingEntity::class

    override fun mapFromInput(input: AddSportsClassToPersonInputDto): CallResult<PersonSportsClassMappingEntity, InputMappingError> =
        CallResult.Success(
            PersonSportsClassMappingEntity(
                id = null,
                person = PersonEntity.idOnly(input.person.id),
                sportsClass = SportsClassEntity.idOnly(input.sportsClass.id)
            )
        )
}