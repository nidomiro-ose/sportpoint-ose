package de.nidomiro.sportpoint.backend.service.dto.input

import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import java.time.LocalDate

data class CreateCompetitionInputDto(
    val name: String,
    val date: LocalDate,
    val organizer: ReferenceClubInputDto,
    val venueAddress: PostalAddressInputDto,
    val maxParticipants: Int,
    val competitionType: ReferenceCompetitionTypeInputDto,
    val description: String,
    val visibility: CompetitionEntity.Visibility
) : InputDto