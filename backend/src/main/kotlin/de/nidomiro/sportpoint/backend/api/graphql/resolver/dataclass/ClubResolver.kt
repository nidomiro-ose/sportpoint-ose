package de.nidomiro.sportpoint.backend.api.graphql.resolver.dataclass

import com.coxautodev.graphql.tools.GraphQLResolver
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.service.CompetitionService
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class ClubResolver(
    private val competitionService: CompetitionService
) : GraphQLResolver<ClubEntity> {

    @Suppress("unused")
    fun competitions(clubDto: ClubEntity, first: Int?, after: String?, env: DataFetchingEnvironment) =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            competitionService.findByClubId(clubDto.id!!, first_vS, creationDateAfter)
        })
            .get(env)

}