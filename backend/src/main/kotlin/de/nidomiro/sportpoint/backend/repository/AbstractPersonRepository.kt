package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.AbstractPersonEntity
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface AbstractPersonRepository<T : AbstractPersonEntity>
    : SimpleRepository<T, Long> {

    fun existsByName(name: String): Boolean
    fun existsByEmailAddress(emailAddress: String): Boolean

}