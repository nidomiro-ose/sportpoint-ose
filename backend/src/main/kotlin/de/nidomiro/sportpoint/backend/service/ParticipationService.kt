package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.repository.ParticipationRepository
import de.nidomiro.sportpoint.backend.repository.entities.ParticipationEntity
import de.nidomiro.sportpoint.backend.repository.entities.ParticipationEntity_
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.specifications.specFactoryByAbstractPersonIdPaged
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import de.nidomiro.sportpoint.backend.service.dto.input.CreateParticipationInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.CreateSelfParticipationInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferencePersonInputDto
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class ParticipationService(
    inputMappingRegister: InputMappingRegister,
    repository: ParticipationRepository,
    private val competitionService: CompetitionService,
    private val personService: PersonService
) : BasicService<Long, ParticipationEntity, CreateParticipationInputDto, ParticipationRepository>(
    entityClass = ParticipationEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {
    fun findByCompetitionId(id: Long) = repository.findByCompetitionId(id)

    fun findAllByPersonId(
        id: Long,
        first: Int,
        afterCreationDate: LocalDateTime?
    ): PageResult<ParticipationEntity> {

        return repository.findFirstBy(
            specFactoryByAbstractPersonIdPaged(id) { root -> root.get(ParticipationEntity_.participant) },
            first,
            afterCreationDate
        )
    }


    override fun checkAndFetchAttributeValuesFromDb(entity: ParticipationEntity): CallResult<ParticipationEntity, EntityServiceError> {

        entity.competition = competitionService.findById(entity.competition.id)
            ?: return EntityServiceError.NotFound("No Competition with id '${entity.competition.id}' was found")
                .toFailure()

        entity.participant = personService.findById(entity.participant.id)
            ?: return EntityServiceError.NotFound("No Person with id '${entity.participant.id}' was found").toFailure()

        return CallResult.Success(entity)
    }

    fun createNew(
        input: CreateSelfParticipationInputDto,
        loggedInUser: PersonEntity
    ): CallResult<ParticipationEntity, EntityServiceError> {

        val userId = loggedInUser.id
            ?: return CallResult.Failure(EntityServiceError.NotFound("The personID of the current User is null"))

        return createNew(
            CreateParticipationInputDto(
                competition = input.competition,
                participant = ReferencePersonInputDto(userId),
                participatingClub = input.participatingClub,
                participatingSportsClass = input.participatingSportsClass
            )
        )
    }
}