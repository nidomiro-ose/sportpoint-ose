package de.nidomiro.sportpoint.backend.repository.entities


import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "abstract_person")
@DiscriminatorColumn(name = "person_type")
abstract class AbstractPersonEntity : GenericEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override val id: Long? = null

    open val name: String = ""

    open val emailAddress: String? = null

    open val phoneNumber: String? = null

    @NotNull
    open val postalAddress: PostalAddressEntity = PostalAddressEntity()

    abstract val displayName: String
}