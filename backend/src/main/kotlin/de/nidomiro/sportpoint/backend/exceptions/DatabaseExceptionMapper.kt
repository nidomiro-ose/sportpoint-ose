package de.nidomiro.sportpoint.backend.exceptions

import de.nidomiro.sportpoint.backend.error.EntityServiceError
import org.springframework.dao.DataIntegrityViolationException
import javax.persistence.EntityExistsException

abstract class DatabaseExceptionMapper {

    companion object {

        private val duplicationConstraintsNames = listOf(
            "const_participation_unique_combination",
            "pk_person_sports_class",
            "const_abstract_person_unique_email",
            "const_competition_type_unique_name",
            "const_entry_fees_unique_label_for_competition"
        )

        fun mapAndThrow(exception: DataIntegrityViolationException) {
            throw mapToException(exception)
        }

        fun mapToException(exception: DataIntegrityViolationException): Exception = when {
            isExceptionOfType(exception, duplicationConstraintsNames) -> EntityExistsException(exception)

            else -> exception
        }

        fun mapToEntityServiceError(exception: DataIntegrityViolationException): EntityServiceError = when {
            isExceptionOfType(
                exception,
                duplicationConstraintsNames
            ) -> EntityServiceError.Duplicate("") //TODO: add Information to the Duplicate, not empty string

            else -> EntityServiceError.Unknown()
        }


        private fun isExceptionOfType(
            exception: DataIntegrityViolationException,
            constraintNameList: List<String>
        ): Boolean {
            constraintNameList.forEach {
                if (exception.message?.toLowerCase()?.contains(it.toLowerCase()) == true) return true
            }
            return false
        }


    }
}