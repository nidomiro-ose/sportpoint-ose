package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.repository.CompetitionTypeRepository
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionTypeInputDto
import org.springframework.stereotype.Service

@Service
class CompetitionTypeService(
    inputMappingRegister: InputMappingRegister,
    repository: CompetitionTypeRepository
) : BasicService<Long, CompetitionTypeEntity, CreateCompetitionTypeInputDto, CompetitionTypeRepository>(
    entityClass = CompetitionTypeEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {

    fun existsByName(name: String?): Boolean = name?.let { repository.existsByName(it) } == true

    override fun checkAndFetchAttributeValuesFromDb(entity: CompetitionTypeEntity): CallResult<CompetitionTypeEntity, EntityServiceError> {
        return CallResult.Success(entity)
    }
}