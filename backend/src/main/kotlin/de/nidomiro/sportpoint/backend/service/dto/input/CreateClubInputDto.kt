package de.nidomiro.sportpoint.backend.service.dto.input

data class CreateClubInputDto(
    val name: String,
    val emailAddress: String? = null,
    val phoneNumber: String? = null,
    val postalAddress: PostalAddressInputDto? = null
) : InputDto