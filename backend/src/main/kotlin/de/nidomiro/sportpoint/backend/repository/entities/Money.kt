package de.nidomiro.sportpoint.backend.repository.entities

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.validation.constraints.Digits
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Embeddable
data class Money(

    @NotNull
    @Digits(integer = 19, fraction = amountInternalScale)
    @Column(precision = 19, scale = amountInternalScale)
    val amount: BigDecimal,

    @NotBlank
    val currency: String
) {
    constructor(amount: Number, currency: String) : this(
        amount = BigDecimal(amount.toString()).setScale(amountInternalScale, BigDecimal.ROUND_HALF_DOWN),
        currency = currency
    )


    val formatted: String
        get() = "${amount.setScale(amountPrintScale, BigDecimal.ROUND_HALF_DOWN)} $currency"


    companion object {
        const val amountInternalScale = 4
        const val amountPrintScale = 2
    }
}