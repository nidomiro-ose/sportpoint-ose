package de.nidomiro.sportpoint.backend

interface AppInfoService {

    val name: String
    val version: String
}