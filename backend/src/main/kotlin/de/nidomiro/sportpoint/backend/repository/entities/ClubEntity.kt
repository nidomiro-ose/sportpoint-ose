package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity
import javax.persistence.PrimaryKeyJoinColumn
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
@Table(name = "club")
@PrimaryKeyJoinColumn(name = "id")
@DiscriminatorValue("C")
data class ClubEntity(

    override var id: Long? = null,

    override var name: String,

    override var emailAddress: String? = null,

    override var phoneNumber: String? = null,

    override var postalAddress: PostalAddressEntity = PostalAddressEntity(),

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : AbstractPersonEntity() {
    override val displayName: String
        get() = name


    companion object {
        const val typeid = "club"

        fun idOnly(id: Long?) =
            ClubEntity(id = id, name = "", postalAddress = PostalAddressEntity(), isIdOnlyEntity = true)
    }

}