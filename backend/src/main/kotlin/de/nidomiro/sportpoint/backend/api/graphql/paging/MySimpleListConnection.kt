package de.nidomiro.sportpoint.backend.api.graphql.paging

import graphql.Assert.assertNotNull
import graphql.Assert.assertTrue
import graphql.relay.*
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import java.lang.String.format
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.Base64.getDecoder
import java.util.Base64.getEncoder

class MySimpleListConnection<T> @JvmOverloads constructor(
    data: List<T>,
    private val prefix: String? = DUMMY_CURSOR_PREFIX
) : DataFetcher<Connection<T>> {
    private val data: List<T> = assertNotNull(data, " data cannot be null")

    init {
        assertTrue(prefix != null && prefix.isNotEmpty(), "prefix cannot be null or empty")
    }

    private fun buildEdges(): List<Edge<T>> {
        val edges = ArrayList<Edge<T>>()
        for ((ix, obj) in data.withIndex()) {
            edges.add(DefaultEdge(obj, DefaultConnectionCursor(createCursor(ix))))
        }
        return edges
    }

    override fun get(environment: DataFetchingEnvironment): Connection<T> {

        val allEdges = buildEdges()
        if (allEdges.isEmpty()) {
            return emptyConnection()
        }

        var reducedEdges = cutListWithAfterAndBefore(environment, allEdges)
        reducedEdges = applyFirstAndLastValueCounts(environment, reducedEdges)
        if (reducedEdges.isEmpty()) {
            return emptyConnection()
        }

        val firstEdge = reducedEdges.first()
        val lastEdge = reducedEdges.last()

        return DefaultConnection(
            allEdges,
            DefaultPageInfo(
                firstEdge.cursor,
                lastEdge.cursor,
                firstEdge.cursor != allEdges.first().cursor,
                lastEdge.cursor != allEdges.last().cursor
            )
        )
    }

    private fun cutListWithAfterAndBefore(
        environment: DataFetchingEnvironment,
        edges: List<Edge<T>>
    ): List<Edge<T>> {
        var edges1 = edges
        val afterOffset = getOffsetFromCursor(environment.getArgument<String>("after"), -1)
        var begin = Math.max(afterOffset, -1) + 1
        val beforeOffset = getOffsetFromCursor(environment.getArgument<String>("before"), edges1.size)
        val end = Math.min(beforeOffset, edges1.size)

        if (begin > end) begin = end

        edges1 = edges1.subList(begin, end)
        return edges1
    }

    private fun applyFirstAndLastValueCounts(
        environment: DataFetchingEnvironment,
        edges: List<Edge<T>>
    ): List<Edge<T>> {
        var edges1 = edges
        val first: Int? = getCheckedParamFirst(environment)
        edges1 = first?.let { edges1.subList(0, Integer.min(it, edges1.size)) } ?: edges1

        val last: Int? = getCheckedParamLast(environment)
        edges1 = last?.let { edges1.subList(Integer.max(edges1.size - it, 0), edges1.size) } ?: edges1
        return edges1
    }

    private fun getCheckedParamFirst(environment: DataFetchingEnvironment): Int? {
        return environment.getArgument<Any>("first")
            ?.let { first ->
                when (first) {
                    is BigInteger -> first.toInt()
                    is Int -> first
                    else -> null
                }
            }
            ?.let { first ->
                if (first < 0) {
                    throw IllegalArgumentException("The page size must not be negative: 'first'=$first")
                }
                first
            }
    }

    private fun getCheckedParamLast(environment: DataFetchingEnvironment): Int? {
        return environment.getArgument<Any>("first")
            ?.let { last ->
                when (last) {
                    is BigInteger -> last.toInt()
                    is Int -> last
                    else -> null
                }
            }
            ?.let { last ->
                if (last < 0) {
                    throw IllegalArgumentException(format("The page size must not be negative: 'last'=%s", last))
                }
                last
            }
    }

    private fun emptyConnection(): Connection<T> {
        val pageInfo = DefaultPageInfo(null, null, false, false)
        return DefaultConnection(emptyList(), pageInfo)
    }

    private fun getOffsetFromCursor(cursor: String?, defaultValue: Int): Int {
        if (cursor == null) {
            return defaultValue
        }
        val decode: ByteArray
        try {
            decode = getDecoder().decode(cursor)
        } catch (e: IllegalArgumentException) {
            throw IllegalArgumentException(format("The cursor is not in base64 format : '%s'", cursor), e)
        }

        val string = String(decode, StandardCharsets.UTF_8)
        if (prefix!!.length > string.length) {
            throw IllegalArgumentException(format("The cursor prefix is missing from the cursor : '%s'", cursor))
        }
        try {
            return Integer.parseInt(string.substring(prefix.length))
        } catch (nfe: NumberFormatException) {
            throw IllegalArgumentException(format("The cursor was not created by this class  : '%s'", cursor), nfe)
        }

    }

    private fun createCursor(offset: Int): String {
        val bytes = (prefix!! + Integer.toString(offset)).toByteArray(StandardCharsets.UTF_8)
        return getEncoder().encodeToString(bytes)
    }

    companion object {
        private const val DUMMY_CURSOR_PREFIX = "simple-cursor"
    }
}
