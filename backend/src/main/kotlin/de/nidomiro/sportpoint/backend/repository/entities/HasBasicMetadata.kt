package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.PreUpdate

typealias CreationDateType = LocalDateTime
typealias LastModifiedType = LocalDateTime

interface HasBasicMetadata {

    var creationDate: CreationDateType

    var lastModified: LastModifiedType


    @PreUpdate
    fun onUpdate() {
        lastModified = LocalDateTime.now()
    }

    companion object {
        const val CREATION_DATE = CompetitionEntity_.CREATION_DATE
        const val LAST_MODIFIED = CompetitionEntity_.LAST_MODIFIED
    }
}