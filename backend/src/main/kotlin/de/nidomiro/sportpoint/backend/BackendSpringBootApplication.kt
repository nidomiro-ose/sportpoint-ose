package de.nidomiro.sportpoint.backend


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor


@SpringBootApplication
class BackendSpringBootApplication {


    //@Bean
    //fun moneyModule() = MoneyModule().withDefaultFormatting()

    @Bean
    fun exceptionTranslation(): PersistenceExceptionTranslationPostProcessor {
        return PersistenceExceptionTranslationPostProcessor()
    }


}


fun main(args: Array<String>) {
    runApplication<BackendSpringBootApplication>(*args)
}
