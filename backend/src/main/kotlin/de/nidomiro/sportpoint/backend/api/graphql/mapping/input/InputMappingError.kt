package de.nidomiro.sportpoint.backend.api.graphql.mapping.input

import de.nidomiro.sportpoint.backend.error.CallError

sealed class InputMappingError(
    override val message: String,
    override val errorCode: String
) : CallError {

    data class GenericError(override val message: String) :
        InputMappingError(message = message, errorCode = "GenericError")

    data class NoMappingFound(override val message: String) :
        InputMappingError(message = message, errorCode = "NoMappingFound")

    data class EntityNotFound(override val message: String) :
        InputMappingError(message = message, errorCode = "EntityNotFound") {

        constructor(what: String, id: Long) : this("Could not find any $what with id=$id")
    }

}