package de.nidomiro.sportpoint.backend.exceptions

class NoMappingFoundException(
    message: String? = null,
    cause: Throwable? = null
) : RuntimeException(message, cause) {

}
