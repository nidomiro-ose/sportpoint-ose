package de.nidomiro.sportpoint.backend.service.dto

data class PageResult<T>(
    val values: List<T>,
    val hasPreviousValues: Boolean,
    val hasNextValues: Boolean
) {
    fun <R> mapValues(mapper: ((T) -> R)) =
        PageResult(
            values = this.values.map(mapper),
            hasPreviousValues = hasPreviousValues,
            hasNextValues = hasNextValues
        )
}