package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.PersonSportsClassMappingEntity
import org.springframework.stereotype.Repository

@Repository
interface PersonSportsClassMappingRepository : SimpleRepository<PersonSportsClassMappingEntity, Long> {

    fun deleteByPersonIdAndSportsClassId(personId: Long, sportsClassId: Long)
    fun findByPersonIdEqualsAndSportsClassIdEquals(personId: Long, sportsClassId: Long): PersonSportsClassMappingEntity?


}