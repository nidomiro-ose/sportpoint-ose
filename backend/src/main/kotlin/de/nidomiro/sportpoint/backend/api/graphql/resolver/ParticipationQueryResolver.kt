package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.kotlin
import de.nidomiro.sportpoint.backend.repository.entities.ParticipationEntity
import de.nidomiro.sportpoint.backend.security.ROLE_REGISTERED_USER
import de.nidomiro.sportpoint.backend.security.loggedInUser
import de.nidomiro.sportpoint.backend.service.ParticipationService
import de.nidomiro.sportpoint.backend.service.dto.input.CreateParticipationInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.CreateSelfParticipationInputDto
import graphql.schema.DataFetchingEnvironment
import graphql.servlet.GraphQLContext
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class ParticipationQueryResolver(
    private val participationService: ParticipationService
) : GraphQLQueryResolver, GraphQLMutationResolver {


    @Suppress("unused")
    fun createParticipation(
        input: CreateParticipationInputDto,
        dataFetchEnv: DataFetchingEnvironment
    ): ParticipationEntity? =
        participationService.createNew(input)
            .getOrThrowGraphQlException(dataFetchEnv)
            .let {
                participationService.findById(it.id)
            }

    @Suppress("unused")
    @PreAuthorize("hasRole('$ROLE_REGISTERED_USER')")
    fun participate(
        input: CreateSelfParticipationInputDto,
        dataFetchEnv: DataFetchingEnvironment
    ): ParticipationEntity? {

        val graphQLContext: GraphQLContext = dataFetchEnv.getContext()

        val loggedInUser = graphQLContext.httpServletRequest.kotlin?.loggedInUser
            ?: return null

        return participationService.createNew(input, loggedInUser)
            .getOrThrowGraphQlException(dataFetchEnv)
            .let {
                participationService.findById(it.id)
            }


    }

}