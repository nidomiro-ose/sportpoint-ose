package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "competition_type")
data class CompetitionTypeEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    @NotBlank
    var name: String,

    @NotNull
    @Column(columnDefinition = "CLOB")
    var description: String,


    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : GenericEntity<Long> {
    companion object {
        fun idOnly(id: Long?) = CompetitionTypeEntity(id = id, name = "", description = "", isIdOnlyEntity = true)
    }
}