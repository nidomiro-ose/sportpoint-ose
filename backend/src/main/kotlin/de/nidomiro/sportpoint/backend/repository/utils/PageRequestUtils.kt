package de.nidomiro.sportpoint.backend.repository.utils

import de.nidomiro.sportpoint.backend.repository.entities.HasBasicMetadata
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

fun onePageSortedByCreationDateWith(elementCount: Int) =
    PageRequest.of(0, elementCount, Sort.by(Sort.Direction.ASC, HasBasicMetadata.CREATION_DATE))