package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "competition")
data class CompetitionEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    @NotBlank
    var name: String,

    @NotNull
    var date: LocalDate,

    @NotNull
    @ManyToOne
    var organizer: ClubEntity,

    @NotNull
    var venueAddress: PostalAddressEntity,

    @NotNull
    var maxParticipants: Int,

    @NotNull
    @ManyToOne
    var competitionType: CompetitionTypeEntity,

    @NotNull
    @Column(columnDefinition = "CLOB")
    var description: String,

    @NotNull
    @Enumerated(EnumType.STRING)
    var visibility: Visibility = Visibility.PUBLIC,

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : GenericEntity<Long> {
    enum class Visibility {
        PUBLIC,
        PRIVATE,
        SEMI_PRIVATE

    }

    companion object {
        fun idOnly(id: Long?) = CompetitionEntity(
            id = id,
            name = "",
            date = LocalDate.MIN,
            organizer = ClubEntity.idOnly(null),
            venueAddress = PostalAddressEntity(),
            maxParticipants = Int.MIN_VALUE,
            competitionType = CompetitionTypeEntity.idOnly(null),
            description = "",
            visibility = Visibility.PRIVATE,
            isIdOnlyEntity = true
        )
    }
}