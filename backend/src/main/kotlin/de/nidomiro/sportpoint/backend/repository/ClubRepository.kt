package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import org.springframework.stereotype.Repository

@Repository
interface ClubRepository : AbstractPersonRepository<ClubEntity>