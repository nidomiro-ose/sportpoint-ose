package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.service.ClubService
import de.nidomiro.sportpoint.backend.service.dto.input.CreateClubInputDto
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class ClubQueryResolver(
    private val clubService: ClubService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    @Suppress("unused")
    fun getClub(id: Long): ClubEntity? = clubService.findById(id)

    @Suppress("unused")
    fun getAllClubs(first: Int?, after: String?, env: DataFetchingEnvironment): Connection<ClubEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            clubService.find(first_vS, creationDateAfter)
        })
            .get(env)


    @Suppress("unused")
    fun createClub(club: CreateClubInputDto, dataFetchEnv: DataFetchingEnvironment): ClubEntity? =
        clubService.createNew(club)
            .getOrThrowGraphQlException(dataFetchEnv)
}