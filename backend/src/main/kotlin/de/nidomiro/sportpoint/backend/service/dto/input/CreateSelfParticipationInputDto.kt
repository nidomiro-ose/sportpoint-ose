package de.nidomiro.sportpoint.backend.service.dto.input

class CreateSelfParticipationInputDto(

    val competition: ReferenceCompetitionInputDto,

    val participatingSportsClass: ReferenceSportsClassInputDto,

    val participatingClub: ReferenceClubInputDto

) : InputDto