package de.nidomiro.sportpoint.backend.service.dto.input

data class PostalAddressInputDto(
    val streetWithNumber: String?,
    val postalCode: String?,
    val city: String?
) : InputDto