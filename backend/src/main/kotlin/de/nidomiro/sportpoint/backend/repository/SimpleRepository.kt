package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.CONST_LocalDateTime_Prostgres_Min
import de.nidomiro.sportpoint.backend.repository.entities.CreationDateType
import de.nidomiro.sportpoint.backend.repository.entities.GenericEntity
import de.nidomiro.sportpoint.backend.repository.specifications.CreationDatePagingFactory
import de.nidomiro.sportpoint.backend.repository.utils.onePageSortedByCreationDateWith
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.PagingAndSortingRepository
import java.time.LocalDateTime
import kotlin.math.min

@NoRepositoryBean
interface SimpleRepository<ENTITY : GenericEntity<ID>, ID> : JpaRepository<ENTITY, ID>,
    CrudRepository<ENTITY, ID>,
    PagingAndSortingRepository<ENTITY, ID>,
    JpaSpecificationExecutor<ENTITY> {

    fun existsByCreationDateBefore(afterCreationDate: LocalDateTime): Boolean


    @JvmDefault
    fun findFirstBy(spec: Specification<ENTITY>, first: Int): List<ENTITY> {
        return findAll(
            spec,
            onePageSortedByCreationDateWith(first)
        ).content
    }

    @JvmDefault
    fun findFirstBy(
        spec: CreationDatePagingFactory<ENTITY>,
        first: Int,
        afterCreationDate: CreationDateType?
    ): PageResult<ENTITY> {

        val afterCreationDateDefined = afterCreationDate ?: CONST_LocalDateTime_Prostgres_Min

        val valuesPlusOne = findFirstBy(
            spec(afterCreationDate),
            first + 1
        )

        val hasPreviousValues = existsByCreationDateBefore(afterCreationDateDefined)

        return PageResult(
            values = valuesPlusOne.subList(0, min(first, valuesPlusOne.size)),
            hasPreviousValues = hasPreviousValues,
            hasNextValues = valuesPlusOne.size > first
        )
    }
}