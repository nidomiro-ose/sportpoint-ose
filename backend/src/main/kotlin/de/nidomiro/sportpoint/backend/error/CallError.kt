package de.nidomiro.sportpoint.backend.error

interface CallError {

    val message: String

    val errorCode: String

}

fun <T : CallError> T.toFailure() = CallResult.Failure(this)
