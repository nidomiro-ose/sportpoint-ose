package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.kotlin
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.security.loggedInUser
import de.nidomiro.sportpoint.backend.service.PersonService
import de.nidomiro.sportpoint.backend.service.PersonSportsClassMappingService
import de.nidomiro.sportpoint.backend.service.dto.input.person.AddSportsClassToPersonInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.person.CreatePersonInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.person.RemoveSportsClassFromPersonInputDto
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import graphql.servlet.GraphQLContext
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest

@Component
class PersonQueryResolver(
    private val personService: PersonService,
    private val sportsClassMappingService: PersonSportsClassMappingService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    @Suppress("unused")
    fun getMyProfile(env: DataFetchingEnvironment): PersonEntity? {
        val request: HttpServletRequest? = env.getContext<GraphQLContext>().httpServletRequest.kotlin

        return request?.loggedInUser
    }



    @Suppress("unused")
    fun getPerson(id: Long): PersonEntity? = personService.findById(id)

    @Suppress("unused")
    fun getAllPersons(first: Int?, after: String?, env: DataFetchingEnvironment): Connection<PersonEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            personService.find(first_vS, creationDateAfter)
        })
            .get(env)

    @Suppress("unused")
    @Transactional
    fun createPerson(input: CreatePersonInputDto, dataFetchEnv: DataFetchingEnvironment): PersonEntity? {

        val person = personService.createNew(input).getOrThrowGraphQlException(dataFetchEnv)
        input.sportsClasses?.forEach {
            sportsClassMappingService.createNew(person, it).getOrThrowGraphQlException()
        }
        return person
    }

    @Suppress("unused")
    fun addSportsClassToPerson(input: AddSportsClassToPersonInputDto): PersonEntity? {
        sportsClassMappingService.createNew(input).getOrThrowGraphQlException()
        return personService.findById(input.person.id)
    }

    @Suppress("unused")
    fun removeSportsClassFromPerson(input: RemoveSportsClassFromPersonInputDto): PersonEntity? {
        val personSportsClassMappingDto = sportsClassMappingService.findBy(input.person.id, input.sportsClass.id)
        sportsClassMappingService.remove(input).getOrThrowGraphQlException()
        return personSportsClassMappingDto?.person
    }
}