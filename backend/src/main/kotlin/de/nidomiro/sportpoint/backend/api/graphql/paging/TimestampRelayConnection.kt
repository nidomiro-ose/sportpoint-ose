package de.nidomiro.sportpoint.backend.api.graphql.paging

import de.nidomiro.sportpoint.backend.repository.entities.HasBasicMetadata
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import graphql.relay.*
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import java.math.BigInteger
import java.time.LocalDateTime

class TimestampRelayConnection<T : HasBasicMetadata>(
    private val valuesService: (first: Int, creationDateAfter: LocalDateTime?) -> PageResult<T>
) : DataFetcher<Connection<T>> {


    override fun get(environment: DataFetchingEnvironment): Connection<T> {

        val first: Int? = getAndCheckParamFirst(environment)

        val edges = first?.let {
            getEdgesFromService(first, environment.getArgument<String>("after"))
        }

        return edges?.let {
            if (it.values.isEmpty()) {
                return@let null
            } else {
                return@let DefaultConnection(
                    it.values,
                    DefaultPageInfo(
                        it.values.first().cursor,
                        it.values.last().cursor,
                        it.hasPreviousValues,
                        it.hasNextValues
                    )
                )
            }
        } ?: emptyConnection()

    }

    private fun getAndCheckParamFirst(environment: DataFetchingEnvironment) =
        environment.getArgument<Any>("first")
            ?.let { first ->
                when (first) {
                    is Number -> first.toInt()
                    is BigInteger -> first.toInt()
                    is Int -> first
                    else -> null
                }
            }
            ?.let { first ->
                if (first < 0) {
                    throw IllegalArgumentException("The page size must not be negative: 'first'=$first")
                }
                first
            }


    private fun getEdgesFromService(first: Int, after: String?): PageResult<Edge<T>> {
        return valuesService(first, after?.let { TimestampCursor(after).timestamp })
            .mapValues { edgeFor(it) }
    }

    private fun edgeFor(value: T): Edge<T> = DefaultEdge(value, TimestampCursor(value))


    private fun emptyConnection(): Connection<T> {
        val pageInfo = DefaultPageInfo(null, null, false, false)
        return DefaultConnection(emptyList(), pageInfo)
    }

}
