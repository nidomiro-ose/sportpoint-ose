package de.nidomiro.sportpoint.backend

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

val <T> Optional<T>.kotlin: T?
    get() {
        return this.orElse(null)
    }


fun Date.toLocalDate(zone: ZoneId = ZoneId.systemDefault()): LocalDate = this.toInstant().atZone(zone).toLocalDate()

val CONST_LocalDateTime_Prostgres_Min: LocalDateTime = LocalDateTime.MIN.withYear(-4710)

