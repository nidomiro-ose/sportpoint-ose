package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PersonRepository : AbstractPersonRepository<PersonEntity> {

    fun findFirstBySsoId(ssoId: UUID): PersonEntity?
}