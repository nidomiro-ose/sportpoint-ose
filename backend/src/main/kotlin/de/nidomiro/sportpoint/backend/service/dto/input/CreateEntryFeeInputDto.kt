package de.nidomiro.sportpoint.backend.service.dto.input

data class CreateEntryFeeInputDto(
    val competition: ReferenceCompetitionInputDto,

    val label: String//,

    //val amount: Money //TODO: implement
) : InputDto