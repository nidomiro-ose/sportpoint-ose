package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl


import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.PostalAddressEntity
import de.nidomiro.sportpoint.backend.service.dto.input.person.CreatePersonInputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class CreatePersonInputMapper(
    private val postalAddressInputMapper: PostalAddressInputMapper
) : InputMapper<CreatePersonInputDto, PersonEntity> {
    override val inputClass: KClass<CreatePersonInputDto> = CreatePersonInputDto::class
    override val outputClass: KClass<PersonEntity> = PersonEntity::class

    override fun mapFromInput(input: CreatePersonInputDto): CallResult<PersonEntity, InputMappingError> {

        val postalAddressCallResult = input.postalAddress?.let { postalAddressInputMapper.mapFromInput(it) }
        val postalAddress = postalAddressCallResult?.asSuccess()?.value

        if (postalAddressCallResult?.isFailure() == true) {
            return postalAddressCallResult.asFailure()!!
        }

        return CallResult.Success(
            PersonEntity(
                id = null,
                firstName = input.firstName,
                name = input.name,
                ssoId = input.ssoId,
                birthDate = input.birthDate,
                emailAddress = input.emailAddress,
                phoneNumber = input.phoneNumber,
                postalAddress = postalAddress ?: PostalAddressEntity()
            )
        )
    }
}