package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "person_sports_class_mapping")
class PersonSportsClassMappingEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    @NotNull
    @OneToOne(optional = false)
    var person: PersonEntity,

    @NotNull
    @OneToOne(optional = false)
    var sportsClass: SportsClassEntity,

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : GenericEntity<Long> {

    companion object {
        fun idOnly(id: Long?) = PersonSportsClassMappingEntity(
            id = id,
            person = PersonEntity.idOnly(null),
            sportsClass = SportsClassEntity.idOnly(null),
            isIdOnlyEntity = true
        )
    }
}