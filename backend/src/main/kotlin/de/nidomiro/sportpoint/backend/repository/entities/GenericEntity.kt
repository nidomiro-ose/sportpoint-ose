package de.nidomiro.sportpoint.backend.repository.entities

interface GenericEntity<ID> : IdentifiableEntity<ID>, HasBasicMetadata {

    val isIdOnlyEntity: Boolean
}