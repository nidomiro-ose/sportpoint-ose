package de.nidomiro.sportpoint.backend.service.dto.input

class CreateParticipationInputDto(

    val competition: ReferenceCompetitionInputDto,

    val participant: ReferencePersonInputDto,

    val participatingSportsClass: ReferenceSportsClassInputDto,

    val participatingClub: ReferenceClubInputDto

) : InputDto