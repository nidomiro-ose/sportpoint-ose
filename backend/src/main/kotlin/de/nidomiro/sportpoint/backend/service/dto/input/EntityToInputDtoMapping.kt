package de.nidomiro.sportpoint.backend.service.dto.input

import de.nidomiro.sportpoint.backend.repository.entities.*
import de.nidomiro.sportpoint.backend.service.dto.input.person.CreatePersonInputDto

fun PostalAddressEntity.toInputDto() = PostalAddressInputDto(
    streetWithNumber, postalCode, city
)


fun ClubEntity.toInputDto() = CreateClubInputDto(
    name, emailAddress, phoneNumber, postalAddress = this.postalAddress.toInputDto()
)

fun PersonEntity.toInputDto() = CreatePersonInputDto(
    firstName, name, ssoId, birthDate, emailAddress, phoneNumber, postalAddress.toInputDto(), null
)

fun SportsClassEntity.toInputDto() = CreateSportsClassInputDto(
    name
)

fun CompetitionEntity.toInputDto(): CreateCompetitionInputDto? {
    return CreateCompetitionInputDto(
        name,
        date,
        ReferenceClubInputDto(organizer.id ?: return null),
        venueAddress.toInputDto(),
        maxParticipants,
        ReferenceCompetitionTypeInputDto(competitionType.id ?: return null),
        description,
        visibility
    )
}

fun CompetitionTypeEntity.toInputDto() = CreateCompetitionTypeInputDto(name, description)

fun ParticipationEntity.toInputDto(): CreateParticipationInputDto? {
    return CreateParticipationInputDto(
        ReferenceCompetitionInputDto(competition.id ?: return null),
        ReferencePersonInputDto(participant.id ?: return null),
        ReferenceSportsClassInputDto(participatingSportsClass.id ?: return null),
        ReferenceClubInputDto(participatingClub.id ?: return null)
    )
}

fun EntryFeeEntity.toInputDto(): CreateEntryFeeInputDto? {
    return CreateEntryFeeInputDto(
        ReferenceCompetitionInputDto(competition.id ?: return null),
        label
    )
}