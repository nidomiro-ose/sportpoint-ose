package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.repository.ClubRepository
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateClubInputDto
import org.springframework.stereotype.Component

@Component
class ClubService(
    inputMappingRegister: InputMappingRegister,
    repository: ClubRepository
) : AbstractPersonService<ClubEntity, CreateClubInputDto, ClubRepository>(
    entityClass = ClubEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {
    override fun createNew(entity: ClubEntity): CallResult<ClubEntity, EntityServiceError> =
        if (existsByName(entity.name)) {
            EntityServiceError.Duplicate("${ClubEntity::class.simpleName} with the name '${entity.name}' already exists")
                .toFailure()
        } else {
            super.createNew(entity)
        }

    override fun checkAndFetchAttributeValuesFromDb(entity: ClubEntity): CallResult<ClubEntity, EntityServiceError> {
        return CallResult.Success(entity)
    }
}