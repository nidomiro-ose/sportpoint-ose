package de.nidomiro.sportpoint.backend.service.dto.input.person

import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferencePersonInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceSportsClassInputDto

data class AddSportsClassToPersonInputDto(
    val person: ReferencePersonInputDto,
    val sportsClass: ReferenceSportsClassInputDto
) : InputDto

data class RemoveSportsClassFromPersonInputDto(
    val person: ReferencePersonInputDto,
    val sportsClass: ReferenceSportsClassInputDto
) : InputDto