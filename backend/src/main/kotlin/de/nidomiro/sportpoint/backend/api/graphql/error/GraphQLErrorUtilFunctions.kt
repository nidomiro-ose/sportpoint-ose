package de.nidomiro.sportpoint.backend.api.graphql.error

import de.nidomiro.sportpoint.backend.error.CallError
import de.nidomiro.sportpoint.backend.error.CallResult
import graphql.schema.DataFetchingEnvironment


fun <S, E : CallError> CallResult<S, E>.getOrThrowGraphQlException(dataFetchEnv: DataFetchingEnvironment? = null) =
    when (this) {
        is CallResult.Success -> this.value
        is CallResult.Failure -> throw MyGraphQLExecutionException(this.error, dataFetchEnv)
    }

class MyGraphQLExecutionException(
    val error: CallError,
    val dataFetchEnv: DataFetchingEnvironment? = null
) : RuntimeException(error.message)

