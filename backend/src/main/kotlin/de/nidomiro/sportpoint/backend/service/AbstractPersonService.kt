package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.repository.AbstractPersonRepository
import de.nidomiro.sportpoint.backend.repository.entities.AbstractPersonEntity
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import kotlin.reflect.KClass


abstract class AbstractPersonService<ENTITY : AbstractPersonEntity, CREATE_DTO : InputDto, R : AbstractPersonRepository<ENTITY>>(
    entityClass: KClass<ENTITY>,
    inputMappingRegister: InputMappingRegister,
    repository: R
) : BasicService<Long, ENTITY, CREATE_DTO, R>(
    entityClass = entityClass,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {

    open fun existsByName(name: String?) = name?.let { repository.existsByName(it) } == true
    open fun existsByEmailAddress(emailAddress: String?) =
        emailAddress?.let { repository.existsByEmailAddress(it) } == true

}