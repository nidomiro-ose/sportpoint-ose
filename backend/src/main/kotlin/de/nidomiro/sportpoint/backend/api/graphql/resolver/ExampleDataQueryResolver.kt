package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.repository.entities.*
import de.nidomiro.sportpoint.backend.service.ParticipationService
import de.nidomiro.sportpoint.backend.service.PersonSportsClassMappingService
import de.nidomiro.sportpoint.backend.service.generator.ExampleGeneratorService
import de.nidomiro.sportpoint.backend.service.generator.PersistExampleDataService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component

@Component
class ExampleDataQueryResolver(
    private val exampleGenerator: ExampleGeneratorService,
    private val persistExampleDataService: PersistExampleDataService,

    private val personSportsClassMappingService: PersonSportsClassMappingService,
    private val participationService: ParticipationService

) : GraphQLQueryResolver, GraphQLMutationResolver {

    @PreAuthorize("hasRole('GLOBAL_ADMIN')")
    @Suppress("unused")
    fun createExampleData(): Boolean? {

        createClubsWithNameOnly()
        val clubs = createClubs()
        val competitionTypes = createCompetitionTypes()
        val competitions = createCompetitions(clubs, competitionTypes)
        val persons = createPersons()
        val sportsClasses = createSportsClasses()
        addSportsClassesToPersons(persons, sportsClasses)
        addParticipations(competitions, persons, clubs)

        return true
    }

    private fun createClubsWithNameOnly(count: Int = 2): List<ClubEntity> {
        return (1..count).mapNotNull {
            persistExampleDataService.clubEntityWithNameOnly(exampleGenerator).asSuccess()?.value
        }
    }

    private fun createClubs(count: Int = 2): List<ClubEntity> {
        return (1..count).mapNotNull {
            persistExampleDataService.clubEntity(exampleGenerator).asSuccess()?.value
        }
    }

    private fun createCompetitionTypes(count: Int = 2): List<CompetitionTypeEntity> {
        return (1..count).mapNotNull {
            persistExampleDataService.competitionTypeEntity(exampleGenerator).asSuccess()?.value
        }
    }

    private fun createCompetitions(
        clubs: List<ClubEntity>,
        competitionTypes: List<CompetitionTypeEntity>,
        count: Int = 5
    ): List<CompetitionEntity> {
        return (1..count).mapNotNull { i ->
            persistExampleDataService.competitionEntity(
                club = clubs[i % clubs.size],
                competitionType = competitionTypes[i % competitionTypes.size],
                exampleGenerator = exampleGenerator
            )
        }
    }

    private fun createPersons(count: Int = 5): List<PersonEntity> {
        return (1..count).mapNotNull {
            persistExampleDataService.personEntity(exampleGenerator).asSuccess()?.value
        }
    }

    private fun createSportsClasses(count: Int = 5): List<SportsClassEntity> {
        return (1..count).mapNotNull {
            persistExampleDataService.sportsClassEntity(exampleGenerator).asSuccess()?.value
        }
    }

    private fun addParticipations(
        competitions: List<CompetitionEntity>,
        persons: List<PersonEntity>,
        clubs: List<ClubEntity>
    ): List<List<CallResult<ParticipationEntity, EntityServiceError>>> {
        return competitions.mapIndexed { i, competition ->
            persons.subList(i % persons.lastIndex, persons.lastIndex)
                .mapNotNull { person ->
                    personSportsClassMappingService.findAllByPersonId(person.id!!, 2)
                        .values
                        .firstOrNull()
                        ?.let { sportsClassMappingEntity ->
                            participationService.createNew(
                                ParticipationEntity(
                                    competition = competition,
                                    participant = person,
                                    participatingSportsClass = sportsClassMappingEntity.sportsClass,
                                    participatingClub = clubs[i % clubs.size]
                                )
                            )
                        }
                }
        }
    }

    private fun addSportsClassesToPersons(
        persons: List<PersonEntity>,
        sportsClasses: List<SportsClassEntity>
    ): MutableList<PersonSportsClassMappingEntity> {
        val sportsClassPersonMappings = mutableListOf<PersonSportsClassMappingEntity>()

        sportsClassPersonMappings.addAll(
            persons.mapIndexedNotNull { i, person ->
                personSportsClassMappingService.createNew(
                    PersonSportsClassMappingEntity(
                        person = person,
                        sportsClass = sportsClasses[i % sportsClasses.size]
                    )
                ).asSuccess()?.value
            }
        )

        sportsClassPersonMappings.addAll(
            persons.mapIndexedNotNull { i, person ->
                if (i % 2 == 0) {
                    personSportsClassMappingService.createNew(
                        PersonSportsClassMappingEntity(
                            person = person,
                            sportsClass = sportsClasses[(i + 1) % sportsClasses.size]
                        )
                    ).asSuccess()?.value
                } else {
                    null
                }
            }
        )
        return sportsClassPersonMappings
    }
}