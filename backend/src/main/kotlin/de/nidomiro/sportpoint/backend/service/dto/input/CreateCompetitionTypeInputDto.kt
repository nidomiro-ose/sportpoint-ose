package de.nidomiro.sportpoint.backend.service.dto.input

data class CreateCompetitionTypeInputDto(
    val name: String,
    val description: String
) : InputDto