package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.repository.CompetitionRepository
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity_
import de.nidomiro.sportpoint.backend.repository.specifications.specFactoryByAbstractPersonIdPaged
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionInputDto
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CompetitionService(
    inputMappingRegister: InputMappingRegister,
    repository: CompetitionRepository,
    private val clubService: ClubService,
    private val competitionTypeService: CompetitionTypeService

) : BasicService<Long, CompetitionEntity, CreateCompetitionInputDto, CompetitionRepository>(
    entityClass = CompetitionEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {

    fun findByClubId(clubId: Long, first: Int, afterCreationDate: LocalDateTime?): PageResult<CompetitionEntity> {

        return repository.findFirstBy(
            specFactoryByAbstractPersonIdPaged(clubId) { root -> root.get(CompetitionEntity_.organizer) },
            first,
            afterCreationDate
        )
    }

    override fun checkAndFetchAttributeValuesFromDb(entity: CompetitionEntity): CallResult<CompetitionEntity, EntityServiceError> {
        entity.organizer = clubService.findById(entity.organizer.id)
            ?: return EntityServiceError.NotFound("No Club with id '${entity.organizer.id}' was found").toFailure()

        entity.competitionType = competitionTypeService.findById(entity.competitionType.id)
            ?: return EntityServiceError.NotFound("No CompetitionType with id '${entity.competitionType.id}' was found")
                .toFailure()

        return CallResult.Success(entity)
    }
}