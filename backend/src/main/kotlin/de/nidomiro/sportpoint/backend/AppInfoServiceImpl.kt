package de.nidomiro.sportpoint.backend

import org.springframework.stereotype.Service
import java.util.*

@Service
class AppInfoServiceImpl : AppInfoService {

    private final val properties = Properties()

    init {
        properties.load(this.javaClass.classLoader.getResourceAsStream("app-info.properties"))
    }

    override val name: String
        get() = properties.getProperty("app.name")

    override val version: String
        get() = properties.getProperty("app.version")
}