package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import org.springframework.stereotype.Repository

@Repository
interface SportsClassRepository : SimpleRepository<SportsClassEntity, Long> {
    fun existsByName(name: String): Boolean
}