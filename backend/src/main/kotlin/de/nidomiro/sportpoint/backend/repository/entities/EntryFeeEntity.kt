package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "entryFees")
data class EntryFeeEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    @NotNull
    @ManyToOne
    var competition: CompetitionEntity,

    @NotBlank
    var label: String,

    @NotNull
    var amount: Money,

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : GenericEntity<Long> {
    companion object {
        fun idOnly(id: Long?) = EntryFeeEntity(
            id = id,
            competition = CompetitionEntity.idOnly(null),
            label = "",
            amount = Money(0, ""),
            isIdOnlyEntity = true
        )
    }
}