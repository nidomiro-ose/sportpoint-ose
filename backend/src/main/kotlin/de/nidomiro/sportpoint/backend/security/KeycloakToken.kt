package de.nidomiro.sportpoint.backend.security

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class KeycloakToken
@JsonCreator
constructor(
    @param:JsonProperty("access_token")
    val accessToken: String
)