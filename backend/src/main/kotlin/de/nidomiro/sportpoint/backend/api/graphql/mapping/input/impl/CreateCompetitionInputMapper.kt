package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl


import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import de.nidomiro.sportpoint.backend.repository.entities.PostalAddressEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionInputDto
import org.springframework.stereotype.Component

@Component
class CreateCompetitionInputMapper(
    private val postalAddressInputMapper: PostalAddressInputMapper
) : InputMapper<CreateCompetitionInputDto, CompetitionEntity> {
    override val inputClass = CreateCompetitionInputDto::class
    override val outputClass = CompetitionEntity::class

    override fun mapFromInput(input: CreateCompetitionInputDto): CallResult<CompetitionEntity, InputMappingError> {

        val venueAddressCallResult = input.venueAddress.let { postalAddressInputMapper.mapFromInput(it) }
        val venueAddress = venueAddressCallResult.asSuccess()?.value ?: PostalAddressEntity()

        if (venueAddressCallResult.isFailure()) {
            return venueAddressCallResult.asFailure()!!
        }

        return CallResult.Success(
            CompetitionEntity(
                id = null,
                name = input.name,
                date = input.date,
                organizer = ClubEntity.idOnly(input.organizer.id),
                venueAddress = venueAddress,
                maxParticipants = input.maxParticipants,
                competitionType = CompetitionTypeEntity.idOnly(input.competitionType.id),
                description = input.description,
                visibility = input.visibility

            )
        )
    }
}