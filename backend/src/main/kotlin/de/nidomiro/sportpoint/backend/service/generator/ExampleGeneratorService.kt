package de.nidomiro.sportpoint.backend.service.generator

import com.github.javafaker.Faker
import de.nidomiro.sportpoint.backend.repository.entities.*
import de.nidomiro.sportpoint.backend.toLocalDate
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.TimeUnit

@Component
class ExampleGeneratorService(
    private val instanceRandom: Random = Random()
) {

    fun clubEntity(id: Long? = null, locale: Locale = Locale.GERMANY, random: Random = instanceRandom): ClubEntity {
        val faker = Faker(locale, random)

        return ClubEntity(
            id = id,
            name = "BSC " + faker.address().cityName(),
            emailAddress = faker.internet().emailAddress(),
            postalAddress = postalAddressEntity(locale),
            phoneNumber = faker.phoneNumber().phoneNumber()
        )
    }

    fun clubEntityWithNameOnly(
        id: Long? = null,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): ClubEntity {
        val faker = Faker(locale, random)

        return ClubEntity(
            id = id,
            name = "BSC " + faker.address().cityName(),
            emailAddress = null,
            postalAddress = PostalAddressEntity(),
            phoneNumber = null
        )
    }

    fun postalAddressEntity(
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): PostalAddressEntity {
        val faker = Faker(locale, random)

        return PostalAddressEntity(
            streetWithNumber = faker.address().streetAddress(),
            city = faker.address().city(),
            postalCode = faker.address().zipCode()
        )
    }

    fun competitionEntity(
        id: Long? = null,
        organizer: ClubEntity,
        competitionType: CompetitionTypeEntity,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): CompetitionEntity {
        val faker = Faker(locale, random)

        return CompetitionEntity(
            id = id,
            name = faker.funnyName().name() + "-Competition",
            date = faker.date().future(5 * 365, TimeUnit.DAYS).toLocalDate(),
            organizer = organizer,
            competitionType = competitionType,
            venueAddress = postalAddressEntity(locale),
            maxParticipants = faker.random().nextInt(0, 200),
            description = faker.lorem().paragraph(50),
            visibility = CompetitionEntity.Visibility.PUBLIC

        )
    }

    fun competitionTypeEntity(
        id: Long? = null,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): CompetitionTypeEntity {
        val faker = Faker(locale, random)

        return CompetitionTypeEntity(
            id = id,
            name = "${faker.ancient().hero()}-${faker.ancient().god()}-Battle",
            description = faker.lorem().paragraph()
        )
    }

    fun personEntity(id: Long? = null, locale: Locale = Locale.GERMANY, random: Random = instanceRandom): PersonEntity {
        val faker = Faker(locale, random)

        return PersonEntity(
            id = id,
            firstName = faker.name().firstName(),
            name = faker.name().lastName(),
            ssoId = UUID.randomUUID(),
            phoneNumber = faker.phoneNumber().phoneNumber(),
            emailAddress = faker.internet().emailAddress(),
            birthDate = faker.date().birthday(10, 100).toLocalDate(),
            postalAddress = postalAddressEntity(locale)
        )
    }

    fun sportsClassEntity(
        id: Long? = null,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): SportsClassEntity {
        val faker = Faker(locale, random)

        return SportsClassEntity(
            id = id,
            name = "${faker.ancient().titan()}-${faker.ancient().god()}-Class"
        )
    }

    fun participationEntity(
        id: Long? = null,
        participant: PersonEntity,
        competition: CompetitionEntity,
        participatingClub: ClubEntity,
        participatingSportsClass: SportsClassEntity,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): ParticipationEntity {

        return ParticipationEntity(
            id = id,
            participant = participant,
            competition = competition,
            participatingClub = participatingClub,
            participatingSportsClass = participatingSportsClass
        )
    }

    fun entryFeeEntity(
        id: Long? = null,
        competition: CompetitionEntity,
        locale: Locale = Locale.GERMANY,
        random: Random = instanceRandom
    ): EntryFeeEntity {
        val faker = Faker(locale, random)

        return EntryFeeEntity(
            id = id,
            competition = competition,
            amount = Money(faker.number().numberBetween(0, 50), "€"),
            label = faker.lorem().characters(4, 50)
        )
    }
}