package de.nidomiro.sportpoint.backend.error

sealed class CallResult<out S, out E : CallError> {

    data class Success<out S>(val value: S) : CallResult<S, Nothing>()
    data class Failure<out E : CallError>(val error: E) : CallResult<Nothing, E>()

    fun <C> map(mapping: (S) -> C): CallResult<C, E> {
        return when (this) {
            is Success -> Success(mapping(this.value))
            is Failure -> this
        }
    }

    fun <C : CallError> mapFailure(mapping: (E) -> C): CallResult<S, C> {
        return when (this) {
            is Success -> this
            is Failure -> Failure(mapping(this.error))
        }
    }

    fun <C> flatMap(successMapping: (S) -> CallResult<C, CallError>): CallResult<C, CallError> {
        return when (this) {
            is Success -> successMapping(this.value)
            is Failure -> this
        }
    }

    /*fun <C, EE: CallError> flatMap(successMapping: (S) -> CallResult<C, EE>,
                    failureMapping: (E) -> CallResult<C, EE>
    ): CallResult<C, EE> {
        return when (this) {
            is Success -> successMapping(this.value).mapFailure { failureMapping(it) }
            is Failure -> failureMapping(this.error)
        }
    }*/

    fun asSuccess(): Success<S>? = when (this) {
        is Success -> this
        is Failure -> null
    }

    fun isSuccess(): Boolean = when (this) {
        is Success -> true
        is Failure -> false
    }

    fun asFailure(): Failure<E>? = when (this) {
        is Success -> null
        is Failure -> this
    }

    fun isFailure(): Boolean = when (this) {
        is Success -> false
        is Failure -> true
    }


}
