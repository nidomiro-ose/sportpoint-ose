package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.service.SportsClassService
import de.nidomiro.sportpoint.backend.service.dto.input.CreateSportsClassInputDto
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class SportsClassQueryResolver(
    private val sportsClassService: SportsClassService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    @Suppress("unused")
    fun getSportsClass(id: Long) = sportsClassService.findById(id)

    @Suppress("unused")
    fun getAllSportsClasses(first: Int?, after: String?, env: DataFetchingEnvironment): Connection<SportsClassEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            sportsClassService.find(first_vS, creationDateAfter)
        })
            .get(env)


    @Suppress("unused")
    fun createSportsClass(
        SportsClass: CreateSportsClassInputDto,
        dataFetchEnv: DataFetchingEnvironment
    ): SportsClassEntity =
        sportsClassService.createNew(SportsClass)
            .getOrThrowGraphQlException(dataFetchEnv)


}