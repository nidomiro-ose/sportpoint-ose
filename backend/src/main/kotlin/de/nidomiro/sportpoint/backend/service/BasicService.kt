package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.EntityServiceError.Companion.mapFrom
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.exceptions.DatabaseExceptionMapper
import de.nidomiro.sportpoint.backend.kotlin
import de.nidomiro.sportpoint.backend.repository.SimpleRepository
import de.nidomiro.sportpoint.backend.repository.entities.GenericEntity
import de.nidomiro.sportpoint.backend.repository.specifications.specFactoryByCreationDateAfter
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import org.springframework.dao.DataIntegrityViolationException
import java.time.LocalDateTime
import javax.transaction.Transactional
import kotlin.reflect.KClass

abstract class BasicService<ID, ENTITY : GenericEntity<ID>, CREATE_DTO : InputDto, R : SimpleRepository<ENTITY, ID>>(
    protected val entityClass: KClass<ENTITY>,
    protected val repository: R,
    protected val inputMappingRegister: InputMappingRegister
) {

    @Transactional
    open fun removeById(id: ID) = repository.deleteById(id)

    @Transactional
    internal open fun remove(entity: ENTITY) = repository.delete(entity)


    open fun findAll(): List<ENTITY> = repository.findAll()

    open fun find(first: Int, afterCreationDate: LocalDateTime?): PageResult<ENTITY> {

        return repository.findFirstBy(
            specFactoryByCreationDateAfter(),
            first,
            afterCreationDate
        )
    }

    open fun findById(id: ID?): ENTITY? = id?.let { repository.findById(id).kotlin }

    open fun existsById(id: ID?): Boolean = id?.let { repository.existsById(it) } == true


    open fun update(entity: ENTITY): CallResult<ENTITY, EntityServiceError> {
        entity.lastModified = LocalDateTime.now()

        return if (entity.id == null || !existsById(entity.id)) {
            EntityServiceError.NotFound("Could not find any entity with the given id=${entity.id}")
                .toFailure()
        } else {
            executeAndMap {
                repository.save(entity)
            }
        }

    }


    open fun createNew(dto: CREATE_DTO): CallResult<ENTITY, EntityServiceError> {
        val mappingResult = mapInputToEntity(dto)
        val result = mappingResult.flatMap {
            when (val entityCallResult = checkAndFetchAttributeValuesFromDb(it)) {
                is CallResult.Success -> createNew(entityCallResult.value)
                is CallResult.Failure -> entityCallResult
            }
        }

        return mapCallResultError(result)
    }

    /**
     * Creates a new Entity in DB and returns it. If the {@code entity} already has a {@code id}, a Failure is triggered.
     * @param entity
     */
    open fun createNew(entity: ENTITY): CallResult<ENTITY, EntityServiceError> {

        if (entity.isIdOnlyEntity) {
            return CallResult.Failure(EntityServiceError.NotFromIdOnly(entityClass))
        }

        return if (entity.id != null) {
            EntityServiceError.IdIsGeneratedByTheServer().toFailure()
        } else {
            executeAndMap {
                repository.save(entity)
            }
        }


    }

    protected fun <T> mapCallResultError(
        result: CallResult<T, CallError>,
        failureMapper: (r: CallError) -> EntityServiceError = EntityServiceError.Companion::mapFrom
    ) =
        result.mapFailure(failureMapper)

    //private fun mapInputToEntity(dto: CREATE_DTO) = modelMapper.map(dto, entityClass)
    private fun mapInputToEntity(dto: CREATE_DTO) = inputMappingRegister.mapInput(dto, entityClass)

    protected fun <T> executeAndMap(block: () -> T): CallResult<T, EntityServiceError> {
        return try {
            CallResult.Success(block())
        } catch (e: DataIntegrityViolationException) {
            DatabaseExceptionMapper.mapToEntityServiceError(e).toFailure()
        }
    }

    abstract fun checkAndFetchAttributeValuesFromDb(entity: ENTITY): CallResult<ENTITY, EntityServiceError>


}