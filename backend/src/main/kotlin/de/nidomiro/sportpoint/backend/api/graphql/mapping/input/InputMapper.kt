package de.nidomiro.sportpoint.backend.api.graphql.mapping.input

import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import kotlin.reflect.KClass

interface InputMapper<I : InputDto, T : Any> {

    val inputClass: KClass<I>
    val outputClass: KClass<T>

    fun mapFromInput(input: I): CallResult<T, InputMappingError>
}