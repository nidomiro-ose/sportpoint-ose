package de.nidomiro.sportpoint.backend.api.graphql.dto

data class AppInfoDto(
    val name: String,
    val version: String
)