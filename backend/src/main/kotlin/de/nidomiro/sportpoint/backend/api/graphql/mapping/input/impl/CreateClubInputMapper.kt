package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.repository.entities.PostalAddressEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateClubInputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class CreateClubInputMapper(
    private val postalAddressInputMapper: PostalAddressInputMapper
) : InputMapper<CreateClubInputDto, ClubEntity> {
    override val inputClass: KClass<CreateClubInputDto> = CreateClubInputDto::class
    override val outputClass: KClass<ClubEntity> = ClubEntity::class

    override fun mapFromInput(input: CreateClubInputDto): CallResult<ClubEntity, InputMappingError> {

        val postalAddressCallResult = input.postalAddress?.let { postalAddressInputMapper.mapFromInput(it) }
        val postalAddress = postalAddressCallResult?.asSuccess()?.value

        if (postalAddressCallResult?.isFailure() == true) {
            return postalAddressCallResult.asFailure()!!
        }

        return CallResult.Success(
            ClubEntity(
                id = null,
                name = input.name,
                emailAddress = input.emailAddress,
                phoneNumber = input.phoneNumber,
                postalAddress = postalAddress ?: PostalAddressEntity()
            )
        )
    }
}