package de.nidomiro.sportpoint.backend.repository.specifications

import de.nidomiro.sportpoint.backend.repository.entities.AbstractPersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.AbstractPersonEntity_
import de.nidomiro.sportpoint.backend.repository.entities.CreationDateType
import de.nidomiro.sportpoint.backend.repository.entities.HasBasicMetadata
import de.nidomiro.sportpoint.backend.repository.utils.getCreationDate
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.Path
import javax.persistence.criteria.Root

typealias CreationDatePagingFactory<T> = (afterCreationDate: CreationDateType?) -> Specification<T>


fun <T : HasBasicMetadata> specFactoryByAbstractPersonIdPaged(
    id: Long,
    personSelector: (root: Root<T>) -> Path<out AbstractPersonEntity>
): CreationDatePagingFactory<T> = { afterCreationDate: CreationDateType? ->
    specByAbstractPersonIdPaged(id, afterCreationDate, personSelector)
}


fun <T : HasBasicMetadata> specByAbstractPersonIdPaged(
    id: Long,
    afterCreationDate: CreationDateType?,
    personSelector: (root: Root<T>) -> Path<out AbstractPersonEntity>
): Specification<T> = Specification { root, query, cb ->

    val person = personSelector(root)
    val personId = person.get(AbstractPersonEntity_.id) // has to be BaseClass, else NPE

    val personIdPredicate = cb.equal(personId, id)


    return@Specification cb.and(
        personIdPredicate,
        specByCreationDateAfter<T>(afterCreationDate).toPredicate(root, query, cb)
    )
}

fun <T : HasBasicMetadata> specFactoryByCreationDateAfter(
): CreationDatePagingFactory<T> = { afterCreationDate: CreationDateType? ->
    specByCreationDateAfter(afterCreationDate)
}

fun <T : HasBasicMetadata> specByCreationDateAfter(
    afterCreationDate: CreationDateType?
): Specification<T> = Specification { root, _, cb ->

    return@Specification afterCreationDate?.let { cb.greaterThan(root.getCreationDate(), afterCreationDate) }
        ?: cb.conjunction()
}