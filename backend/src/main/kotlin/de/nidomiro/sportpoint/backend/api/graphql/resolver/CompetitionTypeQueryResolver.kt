package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import de.nidomiro.sportpoint.backend.service.CompetitionTypeService
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionTypeInputDto
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class CompetitionTypeQueryResolver(
    private val competitionTypeService: CompetitionTypeService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    @Suppress("unused")
    fun getCompetitionType(id: Long) = competitionTypeService.findById(id)

    @Suppress("unused")
    fun getAllCompetitionTypes(
        first: Int?,
        after: String?,
        env: DataFetchingEnvironment
    ): Connection<CompetitionTypeEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            competitionTypeService.find(first_vS, creationDateAfter)
        })
            .get(env)

    @Suppress("unused")
    fun createCompetitionType(
        competitionType: CreateCompetitionTypeInputDto,
        dataFetchEnv: DataFetchingEnvironment
    ): CompetitionTypeEntity =
        competitionTypeService.createNew(competitionType)
            .getOrThrowGraphQlException(dataFetchEnv)


}