package de.nidomiro.sportpoint.backend.security

import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.security.dto.SSOUser
import javax.servlet.ServletRequest

val ATTRIBUTE_SSO_USER = "de.nidomiro.sportpoint.backend.security.SSOUser"

val ATTRIBUTE_LOGGED_IN_USER = "de.nidomiro.sportpoint.backend.LoggedInUser"


var ServletRequest.attributeSsoUser: SSOUser?
    get() = this.getAttribute(ATTRIBUTE_SSO_USER)  as? SSOUser
    set(value) = this.setAttribute(ATTRIBUTE_SSO_USER, value)

var ServletRequest.loggedInUser: PersonEntity?
    get() = this.getAttribute(ATTRIBUTE_LOGGED_IN_USER)  as? PersonEntity
    set(value) = this.setAttribute(ATTRIBUTE_LOGGED_IN_USER, value)