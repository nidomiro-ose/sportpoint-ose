package de.nidomiro.sportpoint.backend.api.graphql.mapping.input

import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class InputMappingRegister(mapper: List<InputMapper<*, *>>) {

    private val register: MutableMap<KClass<out InputDto>, InputMapper<*, *>> = mutableMapOf()

    init {
        mapper.forEach {
            addMapper(it)
        }
    }


    fun <I : InputDto, T : Any> addMapper(mapper: InputMapper<I, T>) {
        register[mapper.inputClass] = mapper
    }


    fun <I : InputDto, T : Any> mapInput(input: I, outputClass: KClass<T>): CallResult<T, InputMappingError> {
        val mapper = register[input::class]

        return when {
            mapper == null -> {
                InputMappingError.NoMappingFound("There is no registered mapper for InputClass '${input::class.simpleName}'")
                    .toFailure()
            }

            mapper.inputClass == input::class && mapper.outputClass == outputClass -> {
                @Suppress("UNCHECKED_CAST") // The if checks the types, so its save to call
                return (mapper as InputMapper<I, T>).mapFromInput(input)
            }

            else -> {
                InputMappingError.NoMappingFound("There is no registered mapper for InputClass '${input::class.simpleName}' and OutputClass '${outputClass.simpleName}")
                    .toFailure()
            }
        }
    }

}

inline fun <I : InputDto, reified T : Any> InputMappingRegister.mapInput(input: I): CallResult<T, InputMappingError> =
    mapInput(input, T::class)