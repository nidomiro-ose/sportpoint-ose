package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.PostalAddressEntity
import de.nidomiro.sportpoint.backend.service.dto.input.PostalAddressInputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class PostalAddressInputMapper : InputMapper<PostalAddressInputDto, PostalAddressEntity> {
    override val inputClass: KClass<PostalAddressInputDto> = PostalAddressInputDto::class
    override val outputClass: KClass<PostalAddressEntity> = PostalAddressEntity::class

    override fun mapFromInput(input: PostalAddressInputDto): CallResult<PostalAddressEntity, InputMappingError> {
        return CallResult.Success(
            PostalAddressEntity(
                streetWithNumber = input.streetWithNumber,
                postalCode = input.postalCode,
                city = input.city
            )
        )
    }
}