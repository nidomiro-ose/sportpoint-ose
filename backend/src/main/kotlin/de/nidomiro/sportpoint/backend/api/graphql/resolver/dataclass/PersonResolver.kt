package de.nidomiro.sportpoint.backend.api.graphql.resolver.dataclass

import com.coxautodev.graphql.tools.GraphQLResolver
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.repository.entities.ParticipationEntity
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.service.ClubService
import de.nidomiro.sportpoint.backend.service.ParticipationService
import de.nidomiro.sportpoint.backend.service.PersonSportsClassMappingService
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class PersonResolver(
    private val personSportsClassMappingService: PersonSportsClassMappingService,
    private val participationService: ParticipationService,
    private val clubService: ClubService
) : GraphQLResolver<PersonEntity> {

    @Suppress("unused")
    fun sportsClasses(
        personDto: PersonEntity,
        first: Int?,
        after: String?,
        env: DataFetchingEnvironment
    ): Connection<SportsClassEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            personSportsClassMappingService.findAllByPersonId(personDto.id!!, first_vS, creationDateAfter)
                .mapValues { it.sportsClass }
        })
            .get(env)


    @Suppress("unused")
    fun participations(
        personDto: PersonEntity,
        first: Int?,
        after: String?,
        env: DataFetchingEnvironment
    ): Connection<ParticipationEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            participationService.findAllByPersonId(personDto.id!!, first_vS, creationDateAfter)
        })
            .get(env)


    @Suppress("unused")
    fun clubs(
        personDto: PersonEntity,
        first: Int?,
        after: String?,
        env: DataFetchingEnvironment
    ): Connection<ClubEntity>? =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            clubService.find(first_vS, creationDateAfter) // TODO: SP-46
        })
            .get(env)


}