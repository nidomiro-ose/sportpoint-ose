package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import org.springframework.stereotype.Repository

@Repository
interface CompetitionRepository : SimpleRepository<CompetitionEntity, Long>