package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.api.graphql.paging.TimestampRelayConnection
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import de.nidomiro.sportpoint.backend.service.CompetitionService
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionInputDto
import graphql.relay.Connection
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class CompetitionQueryResolver(
    private val competitionService: CompetitionService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    @Suppress("unused")
    fun getCompetition(id: Long) = competitionService.findById(id)

    @Suppress("unused")
    fun getAllCompetitions(first: Int?, after: String?, env: DataFetchingEnvironment): Connection<CompetitionEntity> =
        TimestampRelayConnection(valuesService = { first_vS: Int, creationDateAfter: LocalDateTime? ->
            competitionService.find(first_vS, creationDateAfter)
        })
            .get(env)


    @Suppress("unused")
    fun createCompetition(
        competition: CreateCompetitionInputDto,
        dataFetchEnv: DataFetchingEnvironment
    ): CompetitionEntity =
        competitionService.createNew(competition)
            .getOrThrowGraphQlException(dataFetchEnv)

}