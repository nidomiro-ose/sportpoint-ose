package de.nidomiro.sportpoint.backend.security

import de.nidomiro.sportpoint.backend.security.dto.SSOUser
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.keycloak.representations.AccessToken
import java.time.LocalDate
import java.util.*
import javax.servlet.http.HttpServletRequest


val HttpServletRequest.userPrincipalToSsoUser: SSOUser?
    get() {

        val authToken = this.userPrincipal as? KeycloakAuthenticationToken
        val principal = authToken?.principal as? KeycloakPrincipal<*>
        val context = principal?.keycloakSecurityContext


        return if (context != null && context.token != null) {
            val accessToken: AccessToken = context.token

            SSOUser(
                UUID.fromString(accessToken.subject),
                accessToken.email,
                accessToken.givenName,
                accessToken.familyName,
                LocalDate.parse(accessToken.birthdate)
            )
        } else null


    }