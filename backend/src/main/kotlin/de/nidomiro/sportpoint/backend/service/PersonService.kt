package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.repository.PersonRepository
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.PostalAddressEntity
import de.nidomiro.sportpoint.backend.security.dto.SSOUser
import de.nidomiro.sportpoint.backend.service.dto.input.person.CreatePersonInputDto
import org.springframework.stereotype.Component
import java.util.*

@Component
class PersonService(
    inputMappingRegister: InputMappingRegister,
    repository: PersonRepository
) : AbstractPersonService<PersonEntity, CreatePersonInputDto, PersonRepository>(
    entityClass = PersonEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {

    override fun checkAndFetchAttributeValuesFromDb(entity: PersonEntity): CallResult<PersonEntity, EntityServiceError> {
        return CallResult.Success(entity)
    }

    fun updateOrCreate(ssoUser: SSOUser): CallResult<PersonEntity, EntityServiceError> {

        val entity = repository.findFirstBySsoId(ssoUser.id)
        return if (entity != null) {
            entity.firstName = ssoUser.firstName
            entity.name = ssoUser.name
            entity.emailAddress = ssoUser.email
            update(entity)
        } else {
            createNew(
                PersonEntity(
                    ssoId = ssoUser.id,
                    firstName = ssoUser.firstName,
                    name = ssoUser.name,
                    emailAddress = ssoUser.email,
                    birthDate = ssoUser.birthdate,
                    postalAddress = PostalAddressEntity() //TODO: retrieve from Keycloak
                )
            )
        }
    }

    fun findBySsoId(id: UUID): CallResult<PersonEntity?, EntityServiceError> {
        return CallResult.Success(repository.findFirstBySsoId(id))
    }
}
