package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateSportsClassInputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class CreateSportsClassInputMapper : InputMapper<CreateSportsClassInputDto, SportsClassEntity> {
    override val inputClass: KClass<CreateSportsClassInputDto> = CreateSportsClassInputDto::class
    override val outputClass: KClass<SportsClassEntity> = SportsClassEntity::class

    override fun mapFromInput(input: CreateSportsClassInputDto): CallResult<SportsClassEntity, InputMappingError> =
        CallResult.Success(
            SportsClassEntity(
                id = null,
                name = input.name
            )
        )
}