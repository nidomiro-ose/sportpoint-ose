package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.exceptions.MappingException
import de.nidomiro.sportpoint.backend.repository.PersonSportsClassMappingRepository
import de.nidomiro.sportpoint.backend.repository.entities.PersonEntity
import de.nidomiro.sportpoint.backend.repository.entities.PersonSportsClassMappingEntity
import de.nidomiro.sportpoint.backend.repository.entities.PersonSportsClassMappingEntity_
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.repository.specifications.specFactoryByAbstractPersonIdPaged
import de.nidomiro.sportpoint.backend.service.dto.PageResult
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceSportsClassInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.person.AddSportsClassToPersonInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.person.RemoveSportsClassFromPersonInputDto
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import javax.transaction.Transactional

@Component
class PersonSportsClassMappingService(
    inputMappingRegister: InputMappingRegister,
    repository: PersonSportsClassMappingRepository,
    private val personService: PersonService
) : BasicService<Long, PersonSportsClassMappingEntity, AddSportsClassToPersonInputDto, PersonSportsClassMappingRepository>(
    entityClass = PersonSportsClassMappingEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {

    fun findAllByPersonId(
        id: Long,
        first: Int,
        afterCreationDate: LocalDateTime? = null
    ): PageResult<PersonSportsClassMappingEntity> {

        return repository.findFirstBy(
            specFactoryByAbstractPersonIdPaged(id) { root -> root.get(PersonSportsClassMappingEntity_.person) },
            first,
            afterCreationDate
        )
    }


    @Transactional
    override fun remove(entity: PersonSportsClassMappingEntity) {
        if (entity.id != null) {
            super.remove(entity)
        } else {
            repository.deleteByPersonIdAndSportsClassId(
                entity.person.id ?: throw MappingException.cannotBeNull("person.id"),
                entity.sportsClass.id ?: throw MappingException.cannotBeNull("sportsClass.id")
            )
        }
    }

    fun findBy(personId: Long, sportsClassId: Long): PersonSportsClassMappingEntity? =
        repository.findByPersonIdEqualsAndSportsClassIdEquals(personId, sportsClassId)

    @Transactional
    fun remove(input: RemoveSportsClassFromPersonInputDto): CallResult<Boolean, EntityServiceError> {
        val result = inputMappingRegister.mapInput(input, PersonSportsClassMappingEntity::class)
            .map { remove(it) }
            .map { true }

        return mapCallResultError(result)
    }

    override fun checkAndFetchAttributeValuesFromDb(entity: PersonSportsClassMappingEntity): CallResult<PersonSportsClassMappingEntity, EntityServiceError> {
        entity.person = personService.findById(entity.person.id)
            ?: return EntityServiceError.NotFound("No Person with id '${entity.person.id}' was found").toFailure()

        return CallResult.Success(entity)
    }

    fun createNew(
        person: PersonEntity,
        sportsClass: ReferenceSportsClassInputDto
    ): CallResult<PersonSportsClassMappingEntity, EntityServiceError> {
        return createNew(
            PersonSportsClassMappingEntity(
                person = person,
                sportsClass = SportsClassEntity.idOnly(sportsClass.id) // TODO: Rethink if position here is right
            )
        )
    }


}
