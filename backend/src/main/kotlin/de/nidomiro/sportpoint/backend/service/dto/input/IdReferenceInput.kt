package de.nidomiro.sportpoint.backend.service.dto.input

import de.nidomiro.sportpoint.backend.service.dto.HasId

interface IdReferenceInput<T> : HasId<T>, InputDto {

    companion object
}