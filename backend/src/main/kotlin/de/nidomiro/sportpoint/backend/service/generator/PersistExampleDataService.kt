package de.nidomiro.sportpoint.backend.service.generator

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.repository.entities.*
import de.nidomiro.sportpoint.backend.service.*
import org.springframework.stereotype.Service

@Service
class PersistExampleDataService(
    private val clubService: ClubService,
    private val personService: PersonService,
    private val competitionTypeService: CompetitionTypeService,
    private val competitionService: CompetitionService,
    private val entryFeeService: EntryFeeService,
    private val sportsClassService: SportsClassService,
    private val participationService: ParticipationService,
    private val personSportsClassMappingService: PersonSportsClassMappingService
) {


    fun clubEntity(exampleGenerator: ExampleGeneratorService) =
        clubService.createNew(exampleGenerator.clubEntity())

    fun clubEntityWithNameOnly(exampleGenerator: ExampleGeneratorService) =
        clubService.createNew(
            exampleGenerator.clubEntityWithNameOnly()
        )

    fun personEntity(
        exampleGenerator: ExampleGeneratorService,
        sportsClasses: List<SportsClassEntity> = listOf()
    ): CallResult<PersonEntity, EntityServiceError> {
        val person = personService.createNew(exampleGenerator.personEntity())
        if (person.isSuccess()) {
            sportsClasses.forEach {
                personSportsClassMappingService.createNew(
                    PersonSportsClassMappingEntity(
                        person = person.asSuccess()?.value!!,
                        sportsClass = it
                    )
                )
            }
        }

        return person
    }


    fun competitionTypeEntity(exampleGenerator: ExampleGeneratorService) =
        competitionTypeService.createNew(exampleGenerator.competitionTypeEntity())

    fun competitionEntity(
        exampleGenerator: ExampleGeneratorService,
        club: ClubEntity = clubEntity(exampleGenerator).getOrThrowGraphQlException(),
        competitionType: CompetitionTypeEntity = competitionTypeEntity(exampleGenerator).getOrThrowGraphQlException()
    ): CompetitionEntity {

        val competition = exampleGenerator.competitionEntity(
            organizer = club,
            competitionType = competitionType
        )
        return competitionService.createNew(competition).getOrThrowGraphQlException()
    }

    fun entryFeeEntity(
        exampleGenerator: ExampleGeneratorService,
        competition: CompetitionEntity
    ): EntryFeeEntity {
        val entryFee = exampleGenerator.entryFeeEntity(competition = competition)
        return entryFeeService.createNew(entryFee).getOrThrowGraphQlException()
    }

    fun sportsClassEntity(exampleGenerator: ExampleGeneratorService) = exampleGenerator.sportsClassEntity()
        .let { sportsClassService.createNew(it) }


    fun participationEntity(
        exampleGenerator: ExampleGeneratorService,
        competition: CompetitionEntity
    ): CallResult<ParticipationEntity, EntityServiceError> {
        val sportsClass = sportsClassEntity(exampleGenerator).getOrThrowGraphQlException()

        return participationService.createNew(
            exampleGenerator.participationEntity(
                competition = competition,
                participatingSportsClass = sportsClass,
                participant = personEntity(
                    exampleGenerator = exampleGenerator,
                    sportsClasses = listOf(sportsClass)
                ).getOrThrowGraphQlException(),
                participatingClub = clubEntity(exampleGenerator).getOrThrowGraphQlException()
            )
        )
    }


}