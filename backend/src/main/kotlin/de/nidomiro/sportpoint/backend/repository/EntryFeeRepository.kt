package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.EntryFeeEntity
import org.springframework.stereotype.Repository

@Repository
interface EntryFeeRepository : SimpleRepository<EntryFeeEntity, Long> {

    fun findByCompetitionId(id: Long): List<EntryFeeEntity>
}