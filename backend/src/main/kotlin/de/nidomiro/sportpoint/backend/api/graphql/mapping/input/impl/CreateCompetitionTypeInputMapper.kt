package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateCompetitionTypeInputDto
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

@Component
class CreateCompetitionTypeInputMapper : InputMapper<CreateCompetitionTypeInputDto, CompetitionTypeEntity> {
    override val inputClass: KClass<CreateCompetitionTypeInputDto> = CreateCompetitionTypeInputDto::class
    override val outputClass: KClass<CompetitionTypeEntity> = CompetitionTypeEntity::class

    override fun mapFromInput(input: CreateCompetitionTypeInputDto): CallResult<CompetitionTypeEntity, InputMappingError> =
        CallResult.Success(
            CompetitionTypeEntity(
                id = null,
                name = input.name,
                description = input.description
            )
        )
}