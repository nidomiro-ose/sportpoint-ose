package de.nidomiro.sportpoint.backend.repository.entities

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity
import javax.persistence.PrimaryKeyJoinColumn
import javax.persistence.Table
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Table(name = "person")
@PrimaryKeyJoinColumn(name = "id")
@DiscriminatorValue("P")
data class PersonEntity(

    override var id: Long? = null,

    var ssoId: UUID,

    @NotBlank
    var firstName: String,

    override var name: String,

    @NotNull
    var birthDate: LocalDate,

    override var emailAddress: String,

    override var phoneNumber: String? = null,

    @NotNull
    override var postalAddress: PostalAddressEntity = PostalAddressEntity(),

    @NotNull
    override var creationDate: LocalDateTime = LocalDateTime.now(),

    @NotNull
    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false

) : AbstractPersonEntity() {
    override val displayName: String
        get() = "$firstName $name"

    companion object {
        fun idOnly(id: Long?) = PersonEntity(
            id = id,
            ssoId = UUID.randomUUID(),
            firstName = "",
            name = "",
            birthDate = LocalDate.MIN,
            emailAddress = "",
            postalAddress = PostalAddressEntity(),
            isIdOnlyEntity = true
        )
    }
}