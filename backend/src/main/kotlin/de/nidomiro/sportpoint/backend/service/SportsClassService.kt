package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingRegister
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.error.toFailure
import de.nidomiro.sportpoint.backend.repository.SportsClassRepository
import de.nidomiro.sportpoint.backend.repository.entities.SportsClassEntity
import de.nidomiro.sportpoint.backend.service.dto.input.CreateSportsClassInputDto
import org.springframework.stereotype.Component

@Component
class SportsClassService(
    inputMappingRegister: InputMappingRegister,
    repository: SportsClassRepository
) : BasicService<Long, SportsClassEntity, CreateSportsClassInputDto, SportsClassRepository>(
    entityClass = SportsClassEntity::class,
    inputMappingRegister = inputMappingRegister,
    repository = repository
) {
    fun existsByName(name: String?) = name?.let { repository.existsByName(it) } == true

    override fun createNew(entity: SportsClassEntity): CallResult<SportsClassEntity, EntityServiceError> =
        if (existsByName(entity.name)) {
            EntityServiceError.Duplicate("${SportsClassEntity::class.simpleName} with the name '${entity.name}' already exists")
                .toFailure()
        } else {
            super.createNew(entity)
        }

    override fun checkAndFetchAttributeValuesFromDb(entity: SportsClassEntity): CallResult<SportsClassEntity, EntityServiceError> {
        return CallResult.Success(entity)
    }

}