package de.nidomiro.sportpoint.backend.repository.utils

import de.nidomiro.sportpoint.backend.repository.entities.CreationDateType
import de.nidomiro.sportpoint.backend.repository.entities.HasBasicMetadata
import de.nidomiro.sportpoint.backend.repository.entities.LastModifiedType
import javax.persistence.criteria.Root

fun <T : HasBasicMetadata> Root<T>.getCreationDate() = this.get<CreationDateType>(HasBasicMetadata.CREATION_DATE)
fun <T : HasBasicMetadata> Root<T>.getLastModified() = this.get<LastModifiedType>(HasBasicMetadata.LAST_MODIFIED)