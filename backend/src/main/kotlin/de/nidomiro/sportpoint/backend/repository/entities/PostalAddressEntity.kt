package de.nidomiro.sportpoint.backend.repository.entities

import javax.persistence.Embeddable

@Embeddable
data class PostalAddressEntity(

    var streetWithNumber: String? = null,

    var postalCode: String? = null,

    var city: String? = null
)