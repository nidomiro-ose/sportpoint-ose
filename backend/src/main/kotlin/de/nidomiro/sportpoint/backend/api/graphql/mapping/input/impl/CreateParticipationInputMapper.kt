package de.nidomiro.sportpoint.backend.api.graphql.mapping.input.impl

import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMapper
import de.nidomiro.sportpoint.backend.api.graphql.mapping.input.InputMappingError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.repository.entities.*
import de.nidomiro.sportpoint.backend.service.dto.input.CreateParticipationInputDto
import org.springframework.stereotype.Component

@Component
class CreateParticipationInputMapper : InputMapper<CreateParticipationInputDto, ParticipationEntity> {
    override val inputClass = CreateParticipationInputDto::class
    override val outputClass = ParticipationEntity::class


    override fun mapFromInput(input: CreateParticipationInputDto): CallResult<ParticipationEntity, InputMappingError> =
        CallResult.Success(
            ParticipationEntity(
                id = null,
                participant = PersonEntity.idOnly(input.participant.id),
                competition = CompetitionEntity.idOnly(input.competition.id),
                participatingClub = ClubEntity.idOnly(input.participatingClub.id),
                participatingSportsClass = SportsClassEntity.idOnly(input.participatingSportsClass.id)
            )
        )
}