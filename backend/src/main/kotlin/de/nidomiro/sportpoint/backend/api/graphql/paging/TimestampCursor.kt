package de.nidomiro.sportpoint.backend.api.graphql.paging

import de.nidomiro.sportpoint.backend.repository.entities.HasBasicMetadata
import graphql.relay.ConnectionCursor
import java.time.LocalDateTime
import java.util.*

class TimestampCursor(
    val timestamp: LocalDateTime
) : ConnectionCursor {

    constructor(value: HasBasicMetadata) : this(value.creationDate)
    constructor(cursor: String) : this(getTimestampValueOfCursor(cursor))


    override fun getValue(): String {
        return listOf(
            prefix,
            timestamp.toString()
        )
            .joinToString(delimiter)
            .let {
                Base64.getEncoder().encodeToString(
                    it.toByteArray(Charsets.UTF_8)
                )
            }
    }


    companion object {
        private const val delimiter = "<::>"
        private const val prefix = "TimestampCursor"

        fun getTimestampValueOfCursor(cursor: String): LocalDateTime {
            return LocalDateTime.parse(
                String(Base64.getDecoder().decode(cursor))
                    .split(delimiter)[1]
            )
        }
    }

    override fun toString(): String = value


}