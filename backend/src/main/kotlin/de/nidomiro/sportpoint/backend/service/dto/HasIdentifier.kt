package de.nidomiro.sportpoint.backend.service.dto

interface HasId<I> {
    val id: I
}