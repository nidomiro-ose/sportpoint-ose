package de.nidomiro.sportpoint.backend.exceptions


class IdIsGeneratedByTheServerException(msg: String = "") : Exception(msg)
