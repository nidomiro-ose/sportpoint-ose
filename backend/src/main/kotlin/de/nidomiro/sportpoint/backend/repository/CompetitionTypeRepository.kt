package de.nidomiro.sportpoint.backend.repository

import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import org.springframework.stereotype.Repository

@Repository
interface CompetitionTypeRepository : SimpleRepository<CompetitionTypeEntity, Long> {

    fun existsByName(name: String): Boolean
}