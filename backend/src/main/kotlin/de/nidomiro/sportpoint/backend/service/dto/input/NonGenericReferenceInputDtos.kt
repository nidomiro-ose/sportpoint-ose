package de.nidomiro.sportpoint.backend.service.dto.input


data class ReferenceSportsClassInputDto(override val id: Long) : IdReferenceInput<Long>
data class ReferencePersonInputDto(override val id: Long) : IdReferenceInput<Long>
data class ReferenceClubInputDto(override val id: Long) : IdReferenceInput<Long>
data class ReferenceCompetitionTypeInputDto(override val id: Long) : IdReferenceInput<Long>
data class ReferenceCompetitionInputDto(override val id: Long) : IdReferenceInput<Long>