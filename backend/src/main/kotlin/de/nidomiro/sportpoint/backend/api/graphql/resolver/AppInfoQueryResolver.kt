package de.nidomiro.sportpoint.backend.api.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.sportpoint.backend.AppInfoService
import de.nidomiro.sportpoint.backend.api.graphql.dto.AppInfoDto
import org.springframework.stereotype.Component

@Component
class AppInfoQueryResolver(
    private val appInfoServiceService: AppInfoService
) : GraphQLQueryResolver {

    @Suppress("unused")
    fun getAppInfo(): AppInfoDto {
        return AppInfoDto(
            name = appInfoServiceService.name,
            version = appInfoServiceService.version
        )
    }
}