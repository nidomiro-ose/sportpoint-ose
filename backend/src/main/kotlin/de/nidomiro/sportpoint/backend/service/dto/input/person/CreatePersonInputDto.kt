package de.nidomiro.sportpoint.backend.service.dto.input.person

import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import de.nidomiro.sportpoint.backend.service.dto.input.PostalAddressInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceSportsClassInputDto
import java.time.LocalDate
import java.util.*

data class CreatePersonInputDto(
    val firstName: String,
    val name: String,
    val ssoId: UUID,
    val birthDate: LocalDate,
    val emailAddress: String,
    val phoneNumber: String?,
    val postalAddress: PostalAddressInputDto?,
    val sportsClasses: List<ReferenceSportsClassInputDto>?
) : InputDto