#!/usr/bin/env bash

if [ -z ${KEYCLOAK_HOST+x} ]; then
    KEYCLOAK_HOST=http://localhost:8080
fi

SERVER=${KEYCLOAK_HOST}/auth

APP_REALM="sp-test"
APP_USER_NAME="globadmin"
APP_USER_PASS="globadmin"


logInAsAdmin() {
    echo "# Logging in"
    /opt/jboss/keycloak/bin/kcadm.sh config credentials --server ${SERVER} --realm master --user admin --password admin
}

createUser() {
    local username=$1
    local password=$2
    local firstName=$3
    local lastName=$4
    local userRoles=$5
    local userId=""
    local birthday=$(date -d "-$(( $RANDOM % 3650 + 7300 )) days" --iso-8601='date') # 3650 Days = 10 years


    echo "#  [$username] Creating user"
    userId=$(/opt/jboss/keycloak/bin/kcadm.sh create users --server ${SERVER} -r ${APP_REALM} \
     -s username=${username} \
     -s "email=${username}" \
     -s "firstName=${firstName}" \
     -s "lastName=${lastName}" \
     -s "attributes.birthdate=${birthday}" \
     -s enabled=true -o --fields id | jq '.id' | tr -d '"')

    echo "## [$username] User created: ${userId}"

    echo "## [$username] Setting user password"
    /opt/jboss/keycloak/bin/kcadm.sh update --server ${SERVER} users/${userId}/reset-password -r ${APP_REALM} -s type=password -s value=${password} -s temporary=false -n &

    local ADDR=(${userRoles//,/ })
    for role in "${ADDR[@]}"; do
        echo "## [$username] Add role '${role}' to user"
        /opt/jboss/keycloak/bin/kcadm.sh add-roles --server ${SERVER} --uusername ${username} --rolename ${role} -r ${APP_REALM} &

    done


}

logInAsAdmin

createUser "globadmin@example.org" "globadmin" "Global" "Admin" "global_admin,registered_user"

for i in {1..10}
do
    logInAsAdmin # Counter slow execution in GitLab
    createUser "user${i}@example.org" "userpass" "User$i" "Vader$i" "registered_user"
done

wait




echo "# Execution finished"


