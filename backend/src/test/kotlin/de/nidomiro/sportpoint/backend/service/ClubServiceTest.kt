package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.service.dto.input.toInputDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


class ClubServiceTest : AbstractBaseTestWithoutGraphQL() {


    @Autowired
    private lateinit var clubService: ClubService


    @Test
    fun existsByName() {
        val club = exampleGenerator.clubEntity().toInputDto()

        assertThat(clubService.existsByName(club.name)).isFalse()
        clubService.createNew(club)
        assertThat(clubService.existsByName(club.name)).isTrue()
    }

    @Test
    fun createNewExpectSuccess() {
        val club = exampleGenerator.clubEntity().toInputDto()
        assertThat(clubService.createNew(club).getOrThrowGraphQlException().id).isNotNull()
    }

    @Test
    fun createNewWithDuplicateEmailExpectFailure() {
        val club = exampleGenerator.clubEntity().toInputDto()
        clubService.createNew(club).getOrThrowGraphQlException().id

        val club2 = exampleGenerator.clubEntity().toInputDto().copy(emailAddress = club.emailAddress)
        val result = clubService.createNew(club2)
        assertFailure(result, EntityServiceError.Duplicate::class)
    }

    @Test
    fun createNewWithDuplicateNameExpectFailure() {
        val club = exampleGenerator.clubEntity().toInputDto()
        clubService.createNew(club).getOrThrowGraphQlException()


        val club2 = exampleGenerator.clubEntity().toInputDto().copy(name = club.name)
        val result = clubService.createNew(club2)

        assertFailure(result, EntityServiceError.Duplicate::class)
    }

    @Test
    fun createNewWithExistingIdExpectFailure() {
        val result = clubService.createNew(exampleGenerator.clubEntity(id = 5))
        assertFailure(result, EntityServiceError.IdIsGeneratedByTheServer::class)
    }

    @Test
    fun findUnknownID() {
        val result = clubService.findById(-1)
        assertThat(result).isNull()
    }

    @Test
    fun testGetAllPaging() {
        (1..10).map {
            val club = exampleGenerator.clubEntity().toInputDto()
            clubService.createNew(club).getOrThrowGraphQlException()
        }

        val firstFetch = clubService.find(2, null)
        assertThat(firstFetch.values).hasSize(2)
        assertThat(firstFetch.hasPreviousValues).isFalse()
        assertThat(firstFetch.hasNextValues).isTrue()
        assertThat(firstFetch.values.first().creationDate).isBefore(firstFetch.values.last().creationDate)


        val secondFetch = clubService.find(3, firstFetch.values.last().creationDate)
        assertThat(secondFetch.values).hasSize(3)
        assertThat(secondFetch.values).doesNotContainAnyElementsOf(firstFetch.values)
        assertThat(secondFetch.hasPreviousValues).isTrue()
        assertThat(secondFetch.hasNextValues).isTrue()

        val thirdFetch = clubService.find(20, firstFetch.values.last().creationDate)
        assertThat(thirdFetch.values).hasSize(8)
        assertThat(thirdFetch.values).doesNotContainAnyElementsOf(firstFetch.values)
        assertThat(thirdFetch.hasPreviousValues).isTrue()
        assertThat(thirdFetch.hasNextValues).isFalse()

    }

}