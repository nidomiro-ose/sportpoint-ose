package de.nidomiro.sportpoint.backend.api.graphql

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GraphQLAppInfoTest : AbstractGraphQLBaseTest() {

    @Test
    fun appInfo() {
        val response = graphQLTemplate.perform("classpath:graphQLs/getAppInfo.graphql")
        println(response.rawResponse.body)

        assertThat(response.statusCode.is2xxSuccessful).isTrue()

        assertThat(response["$.data.result.name"]).isNotNull()
        assertThat(response["$.data.result.version"]).isNotNull()

    }
}