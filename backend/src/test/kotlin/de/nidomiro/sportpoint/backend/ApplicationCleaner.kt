package de.nidomiro.sportpoint.backend

import de.nidomiro.sportpoint.backend.repository.*
import org.springframework.stereotype.Service

@Service
class ApplicationCleaner(
    private val clubRepository: ClubRepository,
    private val competitionRepository: CompetitionRepository,
    private val competitionTypeRepository: CompetitionTypeRepository,
    private val entryFeeRepository: EntryFeeRepository,
    private val participationRepository: ParticipationRepository,
    private val personRepository: PersonRepository,
    private val sportsClassRepository: SportsClassRepository,
    private val sportsClassMappingRepository: PersonSportsClassMappingRepository
) {

    fun clearAll() {
        participationRepository.deleteAllInBatch()
        entryFeeRepository.deleteAllInBatch()
        competitionRepository.deleteAllInBatch()
        sportsClassMappingRepository.deleteAllInBatch()
        personRepository.deleteAllInBatch()
        competitionTypeRepository.deleteAllInBatch()
        clubRepository.deleteAllInBatch()
        sportsClassRepository.deleteAllInBatch()
    }


}