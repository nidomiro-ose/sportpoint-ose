package de.nidomiro.sportpoint.backend.dbTimezone

import de.nidomiro.sportpoint.backend.repository.entities.GenericEntity
import java.time.*
import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
@Table(name = "timezone_test_table")
data class DbTimezoneTestEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override val id: Long? = null,

    @NotNull
    val instant: Instant,

    @NotNull
    val localDate: LocalDate,

    @NotNull
    val localTime: LocalTime,

    @NotNull
    val localDateTime: LocalDateTime,

    @NotNull
    val offsetTime: OffsetTime,

    @NotNull
    val offsetDateTime: OffsetDateTime,

    @NotNull
    val zonedDateTime: ZonedDateTime,


    override var creationDate: LocalDateTime = LocalDateTime.now(),

    override var lastModified: LocalDateTime = creationDate,

    @Transient
    override val isIdOnlyEntity: Boolean = false


) : GenericEntity<Long>