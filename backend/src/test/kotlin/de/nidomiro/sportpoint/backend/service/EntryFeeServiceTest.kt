package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.service.generator.PersistExampleDataService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class EntryFeeServiceTest : AbstractBaseTestWithoutGraphQL() {

    @Autowired
    lateinit var persistExampleDataService: PersistExampleDataService

    @Autowired
    lateinit var entryFeeService: EntryFeeService


    @Test
    fun createNewExpectSuccess() {
        val competition = persistExampleDataService.competitionEntity(exampleGenerator)


        val entryFee = exampleGenerator.entryFeeEntity(competition = competition)
        entryFeeService.createNew(entryFee)
    }

    @Test
    fun createNewWithSameNameForSameCompetitionExpectFailure() {
        val competition = persistExampleDataService.competitionEntity(exampleGenerator)
        val entryFee = exampleGenerator.entryFeeEntity(competition = competition)
        entryFeeService.createNew(entryFee)

        val result = entryFeeService.createNew(
            exampleGenerator.entryFeeEntity(competition = competition)
                .copy(label = entryFee.label)
        )
        assertFailure(result, EntityServiceError.Duplicate::class)
    }


    @Test
    fun getFeesForCompetitionTest() {
        val competition = persistExampleDataService.competitionEntity(exampleGenerator)
        (1..5).map {
            persistExampleDataService.entryFeeEntity(
                competition = competition,
                exampleGenerator = exampleGenerator
            )
        }

        val entryFeesFomDb = entryFeeService.findByCompetitionId(competition.id!!)

        assertThat(entryFeesFomDb.size).isEqualTo(entryFeesFomDb.size)
    }

}