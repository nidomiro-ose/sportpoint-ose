package de.nidomiro.sportpoint.backend

val ERROR_CONTENT_TYPE = "application/vnd.error+json;charset=UTF-8"

val URL_DOMAIN_PATTERN = """https?://[\w\d\.]*"""