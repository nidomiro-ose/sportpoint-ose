package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SportsClassQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun testGetSportsClassNotFound() {
        val values = mapOf(
            "id" to -10
        )

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getSportsClass.graphql",
            values
        )
        println(result.rawResponse.body)

        assertThat(result["$.data.result", Any::class.java]).isNull()

    }

    @Test
    fun testCreateSportsClass() {

        val values = mapOf(
            "name" to "Test SportsClass"
        )

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createSportsClass.graphql",
            mapOf("input" to values)
        )
        println(result.rawResponse.body)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")

    }


    @Test
    fun getAllSportsClasses() {

        val pageSize = 8
        val valuesCount = 10

        (1..valuesCount).map { graphQLClient.createSportsClass() }


        val firstValues = graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllSportsClasses",
            first = pageSize
        )


        graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllSportsClasses",
            first = pageSize,
            after = firstValues["$.data.result.edges[${pageSize - 1}].cursor"]
        )


    }

}