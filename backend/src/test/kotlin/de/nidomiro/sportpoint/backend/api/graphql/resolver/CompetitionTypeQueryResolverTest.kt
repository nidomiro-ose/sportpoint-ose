package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CompetitionTypeQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun testGetCompetitionTypeNotFound() {
        val values = mapOf(
            "id" to -10
        )

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getCompetitionType.graphql",
            values
        )
        println(result.rawResponse.body)

        assertThat(result["$.data.result", Any::class.java]).isNull()

    }

    @Test
    fun testCreateCompetitionType() {

        val values = mapOf(
            "name" to "Test Competition",
            "description" to "TestDescription"
        )

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createCompetitionType.graphql",
            mapOf("input" to values)
        )
        println(result.rawResponse.body)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")
        compareResult(compareBinding, "description")

    }


    @Test
    fun getAllCompetitionTypes() {

        val pageSize = 8
        val valuesCount = 10

        (1..valuesCount).map { graphQLClient.createCompetitionType() }


        val firstValues = graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllCompetitionTypes",
            first = pageSize
        )


        graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllCompetitionTypes",
            first = pageSize,
            after = firstValues["$.data.result.edges[${pageSize - 1}].cursor"]
        )


    }


}