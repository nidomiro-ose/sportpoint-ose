package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import de.nidomiro.sportpoint.backend.security.TestUser
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceClubInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceCompetitionInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferencePersonInputDto
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceSportsClassInputDto
import de.nidomiro.sportpoint.backend.service.graphql.MyGraphQLResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ParticipationQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun testCreateParticipation() {

        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val person = graphQLClient.createPerson()
        val competition = graphQLClient.createCompetition()
        val sportsClass = graphQLClient.createSportsClass()

        val result = graphQLClient.createParticipation(
            competition = ReferenceCompetitionInputDto(competition.id ?: throw AssertionError("id is null")),
            participant = ReferencePersonInputDto(person.id ?: throw AssertionError("id is null")),
            sportsClass = ReferenceSportsClassInputDto(sportsClass.id ?: throw AssertionError("id is null")),
            club = ReferenceClubInputDto(club.id ?: throw AssertionError("id is null"))
        )

        checkParticipationResult(result)

    }

    @Test
    fun testParticipate() {

        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competition = graphQLClient.createCompetition()
        val sportsClass = graphQLClient.createSportsClass()

        val result = graphQLClient.participate(
            accessToken = ssoClient.getAccessToken(TestUser.User1),
            competition = ReferenceCompetitionInputDto(competition.id ?: throw AssertionError("id is null")),
            sportsClass = ReferenceSportsClassInputDto(sportsClass.id ?: throw AssertionError("id is null")),
            club = ReferenceClubInputDto(club.id ?: throw AssertionError("id is null"))
        )

        checkParticipationResult(result)
    }

    @Test
    fun `test participate without login expect failure`() {

        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competition = graphQLClient.createCompetition()
        val sportsClass = graphQLClient.createSportsClass()

        val result = graphQLClient.participate(
            competition = ReferenceCompetitionInputDto(competition.id ?: throw AssertionError("id is null")),
            sportsClass = ReferenceSportsClassInputDto(sportsClass.id ?: throw AssertionError("id is null")),
            club = ReferenceClubInputDto(club.id ?: throw AssertionError("id is null")),
            assertResultNotNull = false
        )
        assertThat(result["$.errors[0].type"]).isEqualTo("AccessDeniedException")

    }

    private fun checkParticipationResult(result: MyGraphQLResponse) {
        assertThat(result["$.data.result.id"]).isNotNull()

        assertThat(result["$.data.result.competition.id"]).isNotNull()
        assertThat(result["$.data.result.competition.name"]).isNotNull()
        assertThat(result["$.data.result.competition.date"]).isNotNull()

        assertThat(result["$.data.result.participant.id"]).isNotNull()
        assertThat(result["$.data.result.participant.displayName"]).isNotNull()
        assertThat(result["$.data.result.participant.emailAddress"]).isNotNull()

        assertThat(result["$.data.result.participatingSportsClass.id"]).isNotNull()
        assertThat(result["$.data.result.participatingSportsClass.name"]).isNotNull()

        assertThat(result["$.data.result.participatingClub.id"]).isNotNull()
        assertThat(result["$.data.result.participatingClub.displayName"]).isNotNull()
    }
}
