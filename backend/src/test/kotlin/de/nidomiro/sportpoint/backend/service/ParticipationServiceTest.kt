package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionEntity
import de.nidomiro.sportpoint.backend.service.generator.PersistExampleDataService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class ParticipationServiceTest : AbstractBaseTestWithoutGraphQL() {

    @Autowired
    lateinit var persistExampleDataService: PersistExampleDataService

    @Autowired
    lateinit var participationService: ParticipationService


    lateinit var competition: CompetitionEntity


    override fun setup() {
        super.setup()
        competition = persistExampleDataService.competitionEntity(exampleGenerator)
    }

    @Test
    fun findAll() {
        val participations = (1..5).map {
            persistExampleDataService.participationEntity(
                competition = competition,
                exampleGenerator = exampleGenerator
            ).getOrThrowGraphQlException()
        }

        assertThat(participationService.findAll()).containsAll(participations)
    }

    @Test
    fun findById() {
        val participations = (1..5).map {
            persistExampleDataService.participationEntity(
                competition = competition,
                exampleGenerator = exampleGenerator
            ).getOrThrowGraphQlException()
        }
        val valueToFind = participations[0]

        val valueFromDb = participationService.findById(valueToFind.id)

        assertThat(valueFromDb).isEqualTo(valueToFind)
    }

    @Test
    fun existsById() {
        val participations = (1..2).map {
            persistExampleDataService.participationEntity(
                competition = competition,
                exampleGenerator = exampleGenerator
            ).getOrThrowGraphQlException()
        }

        participations.forEach {
            assertThat(participationService.existsById(it.id)).isTrue()
        }
    }

    @Test
    fun findByCompetitionId() {
        val participations = (1..5).map {
            persistExampleDataService.participationEntity(
                competition = competition,
                exampleGenerator = exampleGenerator
            ).getOrThrowGraphQlException()
        }
        (1..5).map {
            persistExampleDataService.participationEntity(
                competition = persistExampleDataService.competitionEntity(
                    exampleGenerator
                ), exampleGenerator = exampleGenerator
            )
        }

        assertThat(participationService.findByCompetitionId(competition.id!!)).containsAll(participations)

    }

    @Test
    fun createNewExpectSuccess() {
        val sportsClass =
            persistExampleDataService.sportsClassEntity(exampleGenerator).getOrThrowGraphQlException()
        val person = persistExampleDataService.personEntity(
            sportsClasses = listOf(sportsClass),
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()
        val club = persistExampleDataService.clubEntity(exampleGenerator).getOrThrowGraphQlException()

        val participation = exampleGenerator.participationEntity(
            participant = person,
            competition = competition,
            participatingClub = club,
            participatingSportsClass = sportsClass
        )
        val participationFromDb = participationService.createNew(participation).getOrThrowGraphQlException()

        assertThat(participationFromDb).isEqualTo(participation.copy(id = participationFromDb.id))
    }

    @Test
    fun createNewDuplicateEntryExpectFailure() {
        val sportsClass =
            persistExampleDataService.sportsClassEntity(exampleGenerator).getOrThrowGraphQlException()
        val person = persistExampleDataService.personEntity(
            sportsClasses = listOf(sportsClass),
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()
        val club = persistExampleDataService.clubEntity(exampleGenerator).getOrThrowGraphQlException()

        val participation = exampleGenerator.participationEntity(
            participant = person,
            competition = competition,
            participatingClub = club,
            participatingSportsClass = sportsClass
        )
        participationService.createNew(participation)

        val result = participationService.createNew(participation.copy(id = null))
        assertFailure(result, EntityServiceError.Duplicate::class)
    }

    @Test
    fun updateWithoutExistingEntryExpectFailure() {
        val existingRandomDto = persistExampleDataService.participationEntity(
            competition = competition,
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()
        val result = participationService.update(existingRandomDto.copy(id = 123456))
        assertFailure(result, EntityServiceError.NotFound::class)
    }

    @Test
    fun updateWithoutidEntryExpectFailure() {
        val existingRandomDto = persistExampleDataService.participationEntity(
            competition = competition,
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()
        val result = participationService.update(existingRandomDto.copy(id = null))
        assertFailure(result, EntityServiceError.NotFound::class)
    }

    @Test
    fun updateExpectSuccess() {
        val existingRandomDto = persistExampleDataService.participationEntity(
            competition = competition,
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()

        val newSportsClass =
            persistExampleDataService.sportsClassEntity(exampleGenerator).getOrThrowGraphQlException()
        val newPerson = persistExampleDataService.personEntity(
            sportsClasses = listOf(newSportsClass),
            exampleGenerator = exampleGenerator
        ).getOrThrowGraphQlException()

        participationService.update(
            existingRandomDto.copy(
                participant = newPerson,
                participatingSportsClass = newSportsClass
            )
        )

        val newParticipation = participationService.findById(existingRandomDto.id)

        assertThat(newParticipation).isNotNull
        assertThat(newParticipation?.id).isEqualTo(existingRandomDto.id)

        assertThat(newParticipation?.participant).isNotNull
        assertThat(newParticipation?.participant).isEqualTo(newPerson)

        assertThat(newParticipation?.participatingSportsClass).isNotNull
        assertThat(newParticipation?.participatingSportsClass).isEqualTo(newSportsClass)


    }

}