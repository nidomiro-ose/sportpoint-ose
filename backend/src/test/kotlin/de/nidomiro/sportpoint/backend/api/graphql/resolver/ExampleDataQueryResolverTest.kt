package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import de.nidomiro.sportpoint.backend.security.TestUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ExampleDataQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun createExampleData() {
        graphQLClient.createExampleData(accessToken = ssoClient.getAccessToken(TestUser.Admin))
    }

    @Test
    fun `createExampleData without Token expect Failure`() {
        val response = graphQLClient.createExampleData(assertResultNotNull = false)
        assertThat(response["$.errors[0].type"]).isEqualTo("AccessDeniedException")


        val list = listOf(1, 2, 3, 4, 5, 6)
        val liststr = list.map { it.toString() }


    }
}