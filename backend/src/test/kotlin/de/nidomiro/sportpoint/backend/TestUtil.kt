package de.nidomiro.sportpoint.backend


import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import de.nidomiro.sportpoint.backend.error.CallError
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.service.dto.input.IdReferenceInput
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import de.nidomiro.sportpoint.backend.service.graphql.MyGraphQLResponse
import org.assertj.core.api.Assertions.assertThat
import kotlin.reflect.KClass


fun dtoValuesToGraphQlMap(dto: InputDto?, removeNullFromRoot: Boolean = false): MutableMap<String, Any?> {
    if (dto == null) {
        return mutableMapOf()
    }

    val objectMapper = ObjectMapper()
        .apply {
            registerModule(JavaTimeModule())
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        }

    val valueMap =
        objectMapper.convertValue<MutableMap<String, Any?>>(dto, object : TypeReference<MutableMap<String, Any?>>() {})

    /*if (if dto is HasId<Long>
                && dto.id == null) {
        valueMap.remove("id")
    }*/

    findAndDeleteChildObjectNullValues(valueMap, removeNullFromRoot)

    return valueMap
}

fun findAndDeleteChildObjectNullValues(map: MutableMap<String, Any?>, isChild: Boolean = false) {
    val toRemove = mutableListOf<String>()
    for ((key, value) in map) {
        if (value == null && isChild) {
            toRemove.add(key)
        } else if (value is MutableMap<*, *>) {
            @Suppress("UNCHECKED_CAST")
            findAndDeleteChildObjectNullValues(value as MutableMap<String, Any?>, true)
        }
    }
    toRemove.forEach { map.remove(it) }
}


fun <T : CallError> assertFailure(result: CallResult<*, T>, instanceOf: KClass<out T>) {
    assertThat(result.asFailure()?.error).isInstanceOf(instanceOf.java)
}


inline fun <reified T : IdReferenceInput<Long>> IdReferenceInput.Companion.of(response: MyGraphQLResponse): T {
    return T::class.constructors.first().call(response.id ?: throw AssertionError("id is null"))
}


