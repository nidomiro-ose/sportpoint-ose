package de.nidomiro.sportpoint.backend.spring.initializers

import de.nidomiro.sportpoint.backend.util.KGenericContainer
import org.apache.logging.log4j.LogManager
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.BindMode
import org.testcontainers.containers.output.Log4J2LogConsumer
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy
import org.testcontainers.utility.MountableFile
import java.time.Duration

class KeycloakSpringInitializer :
    ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {

        val activeSpringProfiles = configurableApplicationContext.environment.activeProfiles
        val propMap = mutableMapOf<String, String>()

        if (activeSpringProfiles.contains("CI_start_keycloak_container") ||
            activeSpringProfiles.contains("testcontainersTest")
        ) {
            val keycloakUrl = setupKeycloak()
            propMap["keycloak.auth-server-url"] = keycloakUrl
        }


        TestPropertyValues.of(
            propMap.map { "${it.key}=${it.value}" }
        ).applyTo(configurableApplicationContext.environment)
    }

    private fun setupKeycloak(): String {
        if (!keycloakContainer.isRunning) {
            keycloakContainer.start()
            keycloakContainer.followOutput(
                Log4J2LogConsumer(
                    keyCloakContainerLogger
                )
            )

            val commandResult = keycloakContainer.execInContainer("sh", "/opt/jboss/create-keycloakContainer-user.sh")

            for (line in commandResult.stdout.split("\n")) {
                keyCloakContainerLogger.info("[SCRIPT-STDOUT]: $line")
            }
            for (line in commandResult.stderr.split("\n")) {
                keyCloakContainerLogger.info("[SCRIPT-STDERR]: $line")
            }
            assert(commandResult.exitCode == 0)
        }
        return "http://" + keycloakContainer.containerIpAddress + ":" + keycloakContainer.getMappedPort(8080) + "/auth"
    }


    companion object {

        private val keyCloakContainerLogger = LogManager.getLogger("KeycloakOutput")

        val keycloakContainer: KGenericContainer =
            KGenericContainer("jboss/keycloak:6.0.1")
                .withExposedPorts(8080)
                .withEnv("KEYCLOAK_USER", "admin")
                .withEnv("KEYCLOAK_PASSWORD", "admin")
                .withEnv("KEYCLOAK_IMPORT", "/tmp/realm.json")
                .withClasspathResourceMapping("keycloak/test-realm-export.json", "/tmp/realm.json", BindMode.READ_ONLY)
                .withCopyFileToContainer(
                    MountableFile.forClasspathResource("keycloak/create-keycloakContainer-user.sh", 700),
                    "/opt/jboss/create-keycloakContainer-user.sh"
                )
                .waitingFor(
                    HttpWaitStrategy()
                        .forPath("/auth")
                        .withStartupTimeout(Duration.ofSeconds(120))
                )
    }
}