package de.nidomiro.sportpoint.backend.security

enum class TestUser(val username: String, val password: String) {
    Admin("globadmin@example.org", "globadmin"),
    User1("user1@example.org", "userpass"),
    User2("user2@example.org", "userpass"),
    User3("user3@example.org", "userpass"),
    User4("user4@example.org", "userpass"),
    User5("user5@example.org", "userpass"),
    User6("user6@example.org", "userpass"),
    User7("user7@example.org", "userpass"),
    User8("user8@example.org", "userpass"),
    User9("user9@example.org", "userpass"),
    User10("user10@example.org", "userpass")


}