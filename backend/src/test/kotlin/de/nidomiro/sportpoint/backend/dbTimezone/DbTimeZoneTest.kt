package de.nidomiro.sportpoint.backend.dbTimezone

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import org.assertj.core.api.Assertions.assertThat
import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.jdbc.core.JdbcTemplate
import java.time.*
import java.util.*


class DbTimeZoneTest : AbstractBaseTestWithoutGraphQL() {


    /* companion object {
        init {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT+2"))
        }
    }*/


    @Autowired
    private lateinit var repository: DbTimeZoneTestRepository

    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    private lateinit var e: Environment


    override fun setup() {
        super.setup()
        repository.deleteAllInBatch()
    }


    @Test
    @Ignore
    fun testDbTimeZones() {

        val localDate = LocalDate.of(2000, Month.JANUARY, 2)
        val localTime = LocalTime.of(2, 0, 0, 0)

        val localDateTime = LocalDateTime.of(localDate, localTime)
        val zonedDateTime = localDateTime.atZone(TimeZone.getDefault().toZoneId())

        val testEntityToSave = DbTimezoneTestEntity(
            instant = zonedDateTime.toInstant(),
            localDate = localDate,
            localTime = localTime,
            localDateTime = localDateTime,
            offsetDateTime = zonedDateTime.toOffsetDateTime(),
            offsetTime = zonedDateTime.toOffsetDateTime().toOffsetTime(),
            zonedDateTime = zonedDateTime
        )

        println("### ${TimeZone.getDefault().toZoneId()}: $localDateTime")


        val id = repository.save(testEntityToSave.copy()).id!!

        val fromDatabase = repository.getOne(id)

        assertThat(fromDatabase.instant)
            .isEqualTo(testEntityToSave.instant)
        assertThat(fromDatabase.localDate)
            .isEqualTo(testEntityToSave.localDate)
        assertThat(fromDatabase.localTime)
            .isEqualTo(testEntityToSave.localTime)
        assertThat(fromDatabase.localDateTime)
            .isEqualTo(testEntityToSave.localDateTime)
        assertThat(fromDatabase.offsetDateTime.isEqual(testEntityToSave.offsetDateTime))
            .isTrue()
        assertThat(fromDatabase.offsetTime)
            .isEqualTo(testEntityToSave.offsetTime)
        assertThat(fromDatabase.zonedDateTime)
            .isEqualTo(testEntityToSave.zonedDateTime)

        jdbcTemplate.query(
            "select to_char(local_date_time, 'YYYY-MM-DD\"T\"HH24:MI:SS') from timezone_test_table"
        ) { rs ->
            println("### dbResult: ${rs.getString(1)}")
            assertThat(
                rs.getString(1)
            ).startsWith(localDateTime.atZone(ZoneOffset.UTC).toLocalDateTime().toString())
        }

    }


}