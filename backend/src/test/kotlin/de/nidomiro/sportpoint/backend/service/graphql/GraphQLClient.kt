package de.nidomiro.sportpoint.backend.service.graphql

import de.nidomiro.sportpoint.backend.dtoValuesToGraphQlMap
import de.nidomiro.sportpoint.backend.of
import de.nidomiro.sportpoint.backend.repository.entities.ClubEntity
import de.nidomiro.sportpoint.backend.repository.entities.CompetitionTypeEntity
import de.nidomiro.sportpoint.backend.service.dto.input.*
import de.nidomiro.sportpoint.backend.service.dto.input.person.AddSportsClassToPersonInputDto
import de.nidomiro.sportpoint.backend.service.generator.ExampleGeneratorService
import org.assertj.core.api.Assertions


class GraphQLClient(
    protected val graphQLTemplate: TokenGraphQLTestTemplate,
    private val exampleGenerator: ExampleGeneratorService
) {

    fun createClub(
        accessToken: String? = null,
        values: Map<String, Any?> = dtoValuesToGraphQlMap(exampleGenerator.clubEntity().toInputDto()),
        assertResultNotNull: Boolean = true
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            graphqlResource = "classpath:graphQLs/createClub.graphql",
            variables = mapOf("input" to values),
            accessToken = accessToken
        )
        println("createClub: ${result.rawBody}")
        if (assertResultNotNull) {
            Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        }
        return result
    }

    fun createPerson(
        accessToken: String? = null,
        values: Map<String, Any?> = dtoValuesToGraphQlMap(exampleGenerator.personEntity().toInputDto())
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createPerson.graphql",
            mapOf("input" to values),
            accessToken
        )
        println("createPerson: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        return result
    }

    fun createCompetitionType(
        accessToken: String? = null,
        values: Map<String, Any?> = dtoValuesToGraphQlMap(exampleGenerator.competitionTypeEntity().toInputDto())
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createCompetitionType.graphql",
            mapOf("input" to values),
            accessToken
        )
        println("createCompetitionType: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        return result
    }

    fun createCompetition(
        accessToken: String? = null,
        organizer: ReferenceClubInputDto,
        type: ReferenceCompetitionTypeInputDto
    ): MyGraphQLResponse =
        createCompetition(
            accessToken = accessToken,
            values = dtoValuesToGraphQlMap(
                exampleGenerator.competitionEntity(
                    organizer = ClubEntity.idOnly(organizer.id),
                    competitionType = CompetitionTypeEntity.idOnly(type.id)
                ).toInputDto()
            )
        )


    fun createCompetition(
        accessToken: String? = null,
        values: MutableMap<String, Any?> = dtoValuesToGraphQlMap(
            exampleGenerator.competitionEntity(
                organizer = ClubEntity.idOnly(
                    createClub(accessToken = accessToken).id ?: throw AssertionError("id is null")
                ),
                competitionType = CompetitionTypeEntity.idOnly(
                    createCompetitionType(accessToken = accessToken).id ?: throw AssertionError("id is null")
                )
            ).toInputDto()
        )

    ): MyGraphQLResponse {

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createCompetition.graphql",
            mapOf("input" to values),
            accessToken
        )
        println("CreateCompetitionResponse: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull

        return result
    }

    fun createSportsClass(
        accessToken: String? = null,
        values: Map<String, Any?> = dtoValuesToGraphQlMap(exampleGenerator.sportsClassEntity().toInputDto())
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createSportsClass.graphql",
            mapOf("input" to values),
            accessToken
        )
        println("createSportsClass: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        return result
    }

    fun addSportsClassToPerson(
        accessToken: String? = null,
        person: MyGraphQLResponse,
        sportsClassToAdd: MyGraphQLResponse
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/addSportsClassToPerson.graphql",
            mapOf(
                "input" to dtoValuesToGraphQlMap(
                    AddSportsClassToPersonInputDto(
                        person = IdReferenceInput.of(person),
                        sportsClass = IdReferenceInput.of(sportsClassToAdd)
                    )
                )
            ),
            accessToken
        )

        println("addSportsClassToPerson: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        return result
    }

    fun createParticipation(
        accessToken: String? = null,
        competition: ReferenceCompetitionInputDto,
        participant: ReferencePersonInputDto,
        sportsClass: ReferenceSportsClassInputDto,
        club: ReferenceClubInputDto
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createParticipation.graphql",
            mapOf(
                "input" to dtoValuesToGraphQlMap(
                    CreateParticipationInputDto(
                        competition,
                        participant,
                        sportsClass,
                        club
                    )
                )
            ),
            accessToken
        )
        println("createParticipation: ${result.rawBody}")
        Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        return result
    }

    fun participate(
        accessToken: String? = null,
        competition: ReferenceCompetitionInputDto,
        sportsClass: ReferenceSportsClassInputDto,
        club: ReferenceClubInputDto,
        assertResultNotNull: Boolean = true
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/participate.graphql",
            mapOf(
                "input" to dtoValuesToGraphQlMap(
                    CreateSelfParticipationInputDto(
                        competition,
                        sportsClass,
                        club
                    )
                )
            ),
            accessToken
        )
        println("participate: ${result.rawBody}")
        if (assertResultNotNull) {
            Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        }
        return result
    }

    fun createExampleData(
        accessToken: String? = null,
        assertResultNotNull: Boolean = true
    ): MyGraphQLResponse {
        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createExampleData.graphql",
            mapOf(),
            accessToken
        )
        println("createExampleData: ${result.rawBody}")
        if (assertResultNotNull) {
            Assertions.assertThat(result["$.data.result", Any::class.java]).isNotNull
        }
        return result
    }


    fun executeAndCheckConnectionRequest(
        accessToken: String? = null,
        fileName: String,
        first: Int,
        after: String? = null
    ): MyGraphQLResponse {
        val firstPageResult = graphQLTemplate.perform(
            "classpath:graphQLs/$fileName.graphql",
            mapOf(
                "first" to first,
                "after" to after
            ),
            accessToken
        )
        println(firstPageResult.rawBody)

        val edges = firstPageResult["$.data.result.edges", List::class.java]

        Assertions.assertThat(firstPageResult["$.data.result", Any::class.java]).isNotNull
        Assertions.assertThat(edges?.size).isLessThanOrEqualTo(first)

        return firstPageResult
    }


}