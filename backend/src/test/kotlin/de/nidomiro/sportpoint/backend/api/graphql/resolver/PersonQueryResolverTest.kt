package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import de.nidomiro.sportpoint.backend.dtoValuesToGraphQlMap
import de.nidomiro.sportpoint.backend.service.PersonService
import de.nidomiro.sportpoint.backend.service.dto.input.toInputDto
import de.nidomiro.sportpoint.backend.service.graphql.MyGraphQLResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


class PersonQueryResolverTest : AbstractGraphQLBaseTest() {

    @Autowired
    lateinit var clubService: PersonService

    @Test
    fun personByIdExpectNotFound() {

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getPerson.graphql",
            mapOf("id" to -10)
        )
        println(result.rawResponse.body)
        assertThat(result["$.data.result"]).isNull()

    }

    @Test
    fun createPersonTest() {

        val values = dtoValuesToGraphQlMap(exampleGenerator.personEntity().toInputDto())

        val result = graphQLClient.createPerson(values = values)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")
        compareResult(compareBinding, "emailAddress")
        compareResult(compareBinding, "phoneNumber")

        assertThat(result["$.data.result.postalAddress", Any::class.java]).isNotNull
        compareResult(compareBinding, "postalAddress", "streetWithNumber")
        compareResult(compareBinding, "postalAddress", "postalCode")
        compareResult(compareBinding, "postalAddress", "city")
        assertThat(result["$.data.result.sportsClasses", Any::class.java]).isNotNull

    }

    @Test
    fun personById() {

        val person = graphQLClient.createPerson()

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getPerson.graphql",
            mapOf("id" to person["$.data.result.id"])
        )
        assertThat(result["$.data.result", Any::class.java]).isNotNull
    }

    @Test
    fun getAllPersons() {

        val pageSize = 8
        val valuesCount = 10

        (1..valuesCount).map { graphQLClient.createPerson() }


        val firstValues = graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllPersons",
            first = pageSize
        )


        graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllPersons",
            first = pageSize,
            after = firstValues["$.data.result.edges[${pageSize - 1}].cursor"]
        )

    }

    @Test
    fun getSportsClassesTest() {
        graphQLClient.createSportsClass() // Create random SportsClass

        createPersonWithSportsClassAndVerify() // Check first Person has one SportsClass

        createPersonWithSportsClassAndVerify() // Check second Person has one SportsClass
    }

    private fun createPersonWithSportsClassAndVerify() {
        val sportsClassId = createSportsClassAndReturnId()
        val person = createPersonWithSportsClasses(sportsClassId)

        val personSportsClasses = person["$.data.result.sportsClasses.edges", List::class.java]

        assertThat(personSportsClasses).hasSize(1)
        @Suppress("UNCHECKED_CAST")
        assertThat(person["$.data.result.sportsClasses.edges[0].node.id"]).isEqualTo(sportsClassId)
    }

    @Test
    fun addSportsClassToPersonTest() {
        val sportsClassId = createSportsClassAndReturnId()

        val value = dtoValuesToGraphQlMap(exampleGenerator.personEntity().toInputDto())
        value["sportsClasses"] = listOf(mapOf("id" to sportsClassId))
        val person = graphQLClient.createPerson(values = value)
        val personSportsClasses = person["$.data.result.sportsClasses.edges", List::class.java]

        assertThat(personSportsClasses).hasSize(1)


        val sportsClassToAdd = graphQLClient.createSportsClass()

        val result = graphQLClient.addSportsClassToPerson(person = person, sportsClassToAdd = sportsClassToAdd)

        val personSportsClassesFromResult = result["$.data.result.sportsClasses.edges", List::class.java]
        assertThat(personSportsClassesFromResult).hasSize(2)


    }


    @Test
    fun removeSportsClassToPersonByReference() {

        val sportsClassId = createSportsClassAndReturnId()

        val person = createPersonWithSportsClasses(sportsClassId)
        val personSportsClasses = person["$.data.result.sportsClasses.edges", List::class.java]
        assertThat(personSportsClasses).hasSize(1)

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/removeSportsClassFromPerson.graphql",
            mapOf(
                "input" to
                        mapOf(
                            "person" to
                                    mapOf("id" to person["$.data.result.id"]),
                            "sportsClass" to
                                    mapOf("id" to sportsClassId)
                        )
            )
        )
        println(result.rawResponse.body)

        val resultSportsClasses = result["$.data.result.sportsClasses.edges", List::class.java]
        assertThat(resultSportsClasses).hasSize(0)


    }


    private fun createPersonWithSportsClasses(vararg sportsClassId: String? = listOf(createSportsClassAndReturnId()).toTypedArray()): MyGraphQLResponse {
        val value = dtoValuesToGraphQlMap(exampleGenerator.personEntity().toInputDto())
        value["sportsClasses"] = sportsClassId.map { mapOf("id" to it) }
        return graphQLClient.createPerson(values = value)
    }

    private fun createSportsClassAndReturnId(): String? {
        val sportsClass = graphQLClient.createSportsClass()
        return sportsClass["$.data.result.id"]
    }


}







