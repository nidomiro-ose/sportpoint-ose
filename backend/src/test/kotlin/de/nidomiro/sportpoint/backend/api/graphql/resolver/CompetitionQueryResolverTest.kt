package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import de.nidomiro.sportpoint.backend.security.TestUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CompetitionQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun testGetCompetitionNotFound() {

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getCompetition.graphql",
            mapOf("id" to -10)
        )
        println(result.rawResponse.body)
        assertThat(result["$.data.result"]).isNull()
    }


    @Test
    fun createCompetitionTest() {


        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competitionType = graphQLClient.createCompetitionType()

        val values = mapOf(
            "name" to "Test Competition",
            "date" to LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE),
            "organizer" to
                    mapOf("id" to club["$.data.result.id"]),
            "venueAddress" to
                    mapOf(
                        "streetWithNumber" to "Mustertraße 1",
                        "postalCode" to "12345",
                        "city" to "Musterstadt"
                    ),
            "maxParticipants" to 10,
            "competitionType" to
                    mapOf("id" to competitionType["$.data.result.id"]),
            "description" to "TestDescription",
            "visibility" to "PUBLIC"
        )

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/createCompetition.graphql",
            mapOf("input" to values)
        )
        println(result.rawResponse.body)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")
        compareResult(compareBinding, "date")
        compareResult(compareBinding, "organizer", "id")
        compareResult(compareBinding, "venueAddress", "streetWithNumber")
        compareResult(compareBinding, "venueAddress", "postalCode")
        compareResult(compareBinding, "venueAddress", "city")
        compareResult(compareBinding, "maxParticipants")
        compareResult(compareBinding, "competitionType", "id")
        compareResult(compareBinding, "description")
        compareResult(compareBinding, "visibility")


    }


    @Test
    fun getAllCompetitions() {

        val pageSize = 8
        val valuesCount = 10

        (1..valuesCount).map { graphQLClient.createCompetition() }

        val firstValues = graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllCompetitions",
            first = pageSize
        )


        graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllCompetitions",
            first = pageSize,
            after = firstValues["$.data.result.edges[${pageSize - 1}].cursor"]
        )

    }

}