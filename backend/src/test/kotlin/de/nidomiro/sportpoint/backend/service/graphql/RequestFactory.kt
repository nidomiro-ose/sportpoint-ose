package de.nidomiro.sportpoint.backend.service.graphql

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.LinkedMultiValueMap

internal object RequestFactory {

    fun forJson(json: String, headers: HttpHeaders): HttpEntity<Any> {
        headers.contentType = MediaType.APPLICATION_JSON_UTF8
        return HttpEntity(json, headers)
    }

    fun forMultipart(query: String, variables: String, headers: HttpHeaders): HttpEntity<Any> {
        headers.contentType = MediaType.MULTIPART_FORM_DATA
        val values = LinkedMultiValueMap<String, Any>()
        values.add("query", forJson(query, HttpHeaders()))
        values.add(
            "variables",
            forJson(variables, HttpHeaders())
        )
        return HttpEntity(values, headers)
    }

}// utility class