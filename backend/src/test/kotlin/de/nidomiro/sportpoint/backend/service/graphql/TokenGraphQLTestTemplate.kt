package de.nidomiro.sportpoint.backend.service.graphql

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.util.StreamUtils
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.*

@Component
class TokenGraphQLTestTemplate(
    private val resourceLoader: ResourceLoader,
    private val csrfTokenRepository: CsrfTokenRepository,

    @Value("\${graphql.servlet.mapping:/graphql}")
    private val graphqlMapping: String = "/graphql"

) {
    @Autowired(required = false) // This is needed; In don't know why, but the object will be there.
    var restTemplate: TestRestTemplate? = null

    val permanentHeaderValues: MultiValueMap<String, String> = LinkedMultiValueMap()
    private val objectMapper: ObjectMapper = ObjectMapper()


    fun perform(
        graphqlResource: String,
        variables: Map<String, Any?> = mapOf(),
        accessToken: String? = null
    ): MyGraphQLResponse {

        val header = constructHeader(accessToken)

        return postMultipart(
            loadResource(graphqlResource),
            ObjectMapper().writeValueAsString(variables),
            header
        )
    }


    fun performPlain(
        query: String,
        variables: Map<String, Any?> = mapOf(),
        accessToken: String? = null
    ): MyGraphQLResponse {
        return postRequest(
            RequestFactory.forJson(
                query,
                constructHeader(accessToken)
            )
        )
    }


    protected fun postMultipart(
        query: String,
        variablesAsJson: String,
        header: HttpHeaders = constructHeader()
    ): MyGraphQLResponse {
        return postRequest(
            RequestFactory.forMultipart(
                query,
                variablesAsJson,
                header
            )
        )
    }

    @Throws(IOException::class)
    protected fun loadResource(queryFile: String): String {
        val resource = resourceLoader.getResource(queryFile)
        return loadResource(resource)
    }

    @Throws(IOException::class)
    protected fun loadResource(resource: Resource): String {
        resource.inputStream.use { inputStream -> return StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8) }
    }

    protected fun postRequest(request: HttpEntity<Any>): MyGraphQLResponse {
        val response = restTemplate?.exchange<String>(graphqlMapping, HttpMethod.POST, request) ?: ResponseEntity.of(
            Optional.empty()
        )
        return MyGraphQLResponse(response, objectMapper)
    }


    private fun constructHeader(accessToken: String? = null): HttpHeaders {
        val header = HttpHeaders()
        header.addAll(permanentHeaderValues)

        addCsrfToken(header)
        addAccessToken(header, accessToken)

        return header
    }

    private fun addCsrfToken(header: HttpHeaders) {
        val csrfToken = csrfTokenRepository.generateToken(null)
        header.add(csrfToken.headerName, csrfToken.token)
        header.add("Cookie", "XSRF-TOKEN=" + csrfToken.token)
    }

    private fun addAccessToken(header: HttpHeaders, accessToken: String?) {
        accessToken?.let {
            header.add("Authorization", "Bearer $accessToken")
        }
    }

}