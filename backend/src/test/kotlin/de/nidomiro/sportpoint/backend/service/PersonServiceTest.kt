package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.service.dto.input.toInputDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class PersonServiceTest : AbstractBaseTestWithoutGraphQL() {


    @Autowired
    lateinit var personService: PersonService

    @Test
    fun existsByName() {
        val person = exampleGenerator.personEntity().toInputDto()

        assertThat(personService.existsByName(person.name)).isFalse()
        personService.createNew(person)
        assertThat(personService.existsByName(person.name)).isTrue()
    }


    @Test
    fun createNewExpectSuccess() {
        val person = exampleGenerator.personEntity().toInputDto()
        assertThat(personService.createNew(person).getOrThrowGraphQlException().id).isNotNull()
    }

    @Test
    fun createNewWithDuplicateEmailExpectFailure() {
        val person = exampleGenerator.personEntity().toInputDto()
        personService.createNew(person)

        val result = personService.createNew(
            exampleGenerator.personEntity().toInputDto().copy(emailAddress = person.emailAddress)
        )
        assertFailure(result, EntityServiceError.Duplicate::class)
    }

    @Test
    fun createNewWithExistingIdExpectFailure() {
        val result = personService.createNew(exampleGenerator.personEntity(id = 5))
        assertFailure(result, EntityServiceError.IdIsGeneratedByTheServer::class)
    }
}