package de.nidomiro.sportpoint.backend.baseClassesForTests

import de.nidomiro.sportpoint.backend.ApplicationCleaner
import de.nidomiro.sportpoint.backend.security.SSOClient
import de.nidomiro.sportpoint.backend.service.generator.ExampleGeneratorService
import de.nidomiro.sportpoint.backend.spring.initializers.KeycloakSpringInitializer
import de.nidomiro.sportpoint.backend.spring.initializers.PostgresSpringInitializer
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.test.context.ContextConfiguration
import java.util.*

@ContextConfiguration(
    initializers = [
        PostgresSpringInitializer::class,
        KeycloakSpringInitializer::class
    ]
)
abstract class AbstractCommonTestProperties {

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    protected lateinit var applicationCleaner: ApplicationCleaner

    @Value("\${keycloak.auth-server-url}")
    protected lateinit var keycloakAddress: String

    @Value("\${keycloak.resource}")
    protected lateinit var keycloakClientId: String

    @Value("\${keycloak.realm}")
    protected lateinit var keycloakRealm: String

    @Autowired
    protected lateinit var ssoClient: SSOClient

    protected lateinit var exampleGenerator: ExampleGeneratorService


    @Before
    open fun setup() {
        exampleGenerator =
            ExampleGeneratorService(Random(0)) // Keeps example data stable across tests and test-execution orders
        applicationCleaner.clearAll()
    }


}