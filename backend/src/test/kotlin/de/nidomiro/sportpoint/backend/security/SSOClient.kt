package de.nidomiro.sportpoint.backend.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

@Component
class SSOClient(
    @Value("\${keycloak.auth-server-url}")
    protected val keycloakAddress: String,

    @Value("\${keycloak.resource}")
    protected val keycloakClientId: String,

    @Value("\${keycloak.realm}")
    protected val keycloakRealm: String,

    @Value("\${keycloak.credentials.secret:null}")
    protected val clientSecret: String?

) {

    fun getAccessToken(
        user: TestUser,
        clientId: String = keycloakClientId,
        realm: String = keycloakRealm
    ): String = getAccessToken(user.username, user.password, clientId, realm)

    fun getAccessToken(
        username: String,
        password: String,
        clientId: String = keycloakClientId,
        realm: String = keycloakRealm
    ): String {
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED

        val map: MultiValueMap<String, String> = LinkedMultiValueMap()

        map.setAll(
            mapOf(
                "grant_type" to "password",
                "client_id" to clientId,
                "username" to username,
                "password" to password
            )
        )
        clientSecret?.let { map.set("client_secret", it) }

        //println("# KeycloakTokenUrl: \"$keycloakAddress/realms/$realm/protocol/openid-connect/token\"")


        val token = restTemplate.postForObject(
            "$keycloakAddress/realms/$realm/protocol/openid-connect/token",
            HttpEntity(map, headers),
            KeycloakToken::class.java
        )!!

        return token.accessToken
    }
}