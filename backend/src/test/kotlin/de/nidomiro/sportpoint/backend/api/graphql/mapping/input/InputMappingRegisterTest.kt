package de.nidomiro.sportpoint.backend.api.graphql.mapping.input

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.CallResult
import de.nidomiro.sportpoint.backend.service.dto.input.InputDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

class InputMappingRegisterTest : AbstractBaseTestWithoutGraphQL() {

    @Autowired
    private lateinit var register: InputMappingRegister

    @Test
    fun autowireRegistrationTest() {
        val testInput = TestInputDto(1)

        val result = register.mapInput(testInput, TestEntity::class)

        assertThat(result.asSuccess()?.value?.id).isEqualTo(testInput.id)
    }

    @Test
    fun `test mapping with no mapper`() {
        val testInput = TestInputDtoNoMapper(1)

        val result = register.mapInput(testInput, TestEntity::class)

        assertThat(result.asSuccess()?.value).isNull()
        assertThat(result.asFailure()?.error?.errorCode).isEqualTo("NoMappingFound")
    }

    @Test
    fun `test mapping with wrong output type for mapper`() {
        val testInput = TestInputDto2(1)

        val result = register.mapInput(testInput, TestEntity::class)

        assertThat(result.asSuccess()?.value).isNull()
        assertThat(result.asFailure()?.error?.errorCode).isEqualTo("NoMappingFound")
    }
}


data class TestInputDto(val id: Long) : InputDto
data class TestInputDto2(val id: Long) : InputDto
data class TestInputDtoNoMapper(val id: Long) : InputDto

data class TestEntity(val id: Long)
data class TestEntity2(val id: Long)

@Component
class TestInputDtoMapper : InputMapper<TestInputDto, TestEntity> {

    override val inputClass: KClass<TestInputDto> = TestInputDto::class
    override val outputClass: KClass<TestEntity> = TestEntity::class

    override fun mapFromInput(input: TestInputDto): CallResult<TestEntity, InputMappingError> {
        return CallResult.Success(TestEntity(input.id))
    }

}

@Component
class TestInputDtoMapper2 : InputMapper<TestInputDto2, TestEntity2> {

    override val inputClass: KClass<TestInputDto2> = TestInputDto2::class
    override val outputClass: KClass<TestEntity2> = TestEntity2::class

    override fun mapFromInput(input: TestInputDto2): CallResult<TestEntity2, InputMappingError> {
        return CallResult.Success(TestEntity2(input.id))
    }

}