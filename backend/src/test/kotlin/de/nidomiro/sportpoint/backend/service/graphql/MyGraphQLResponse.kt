package de.nidomiro.sportpoint.backend.service.graphql

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.jayway.jsonpath.JsonPath
import com.jayway.jsonpath.ReadContext
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.io.IOException

class MyGraphQLResponse(
    private val responseEntity: ResponseEntity<String>,
    private val mapper: ObjectMapper
) {

    val context: ReadContext? = try {
        JsonPath.parse(responseEntity.body)
    } catch (e: Exception) {
        null
    }

    val id: Long? get() = this["$.data.result.id", Long::class.java]

    val statusCode: HttpStatus get() = responseEntity.statusCode

    val rawResponse: ResponseEntity<String> get() = responseEntity

    val rawBody: String? get() = responseEntity.body


    @Throws(IOException::class)
    fun readTree(): JsonNode = mapper.readTree(responseEntity.body)

    operator fun get(path: String): String? = context?.read(path)

    operator fun <T> get(path: String, type: Class<T>): T? = context?.read(path, type)

}