package de.nidomiro.sportpoint.backend.spring.initializers

import de.nidomiro.sportpoint.backend.util.KPostgreSQLContainer
import org.apache.logging.log4j.LogManager
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.output.Log4J2LogConsumer

class PostgresSpringInitializer :
    ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {

        val activeSpringProfiles = configurableApplicationContext.environment.activeProfiles
        val propMap = mutableMapOf<String, String>()


        if (activeSpringProfiles.contains("CI_start_postgres_container") ||
            activeSpringProfiles.contains("testcontainersTest")
        ) {
            setupPostgres()
            propMap["spring.datasource.url"] = dbContainer.jdbcUrl
            propMap["spring.datasource.username"] = dbContainer.username
            propMap["spring.datasource.password"] = dbContainer.password
            propMap["spring.datasource.driver-class-name"] = "org.postgresql.Driver"
        }

        TestPropertyValues.of(
            propMap.map { "${it.key}=${it.value}" }
        ).applyTo(configurableApplicationContext.environment)
    }

    private fun setupPostgres() {
        if (!dbContainer.isRunning) {
            dbContainer.start()
            dbContainer.followOutput(Log4J2LogConsumer(postgresContainerLogger))
        }

    }


    companion object {
        private val postgresContainerLogger = LogManager.getLogger("PostgresOutput")

        val dbContainer = KPostgreSQLContainer("postgres:11") // Static reference => no GC

    }
}