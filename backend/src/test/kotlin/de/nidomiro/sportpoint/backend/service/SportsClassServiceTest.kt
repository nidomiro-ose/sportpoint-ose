package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import de.nidomiro.sportpoint.backend.service.dto.input.toInputDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired


class SportsClassServiceTest : AbstractBaseTestWithoutGraphQL() {


    @Autowired
    lateinit var sportsClassService: SportsClassService

    @Test
    fun createEntityExpectSuccess() {
        val sportsClass = exampleGenerator.sportsClassEntity()
        val sportsClassFromDb = sportsClassService.createNew(sportsClass).getOrThrowGraphQlException()
        assertThat(sportsClassFromDb).isEqualTo(sportsClass.copy(id = sportsClassFromDb.id))
    }

    @Test
    fun createEntityWithDuplicateNameExpectFailure() {
        val sportsClass = exampleGenerator.sportsClassEntity().toInputDto()
        sportsClassService.createNew(sportsClass)

        val result =
            sportsClassService.createNew(exampleGenerator.sportsClassEntity().toInputDto().copy(name = sportsClass.name))
        assertFailure(result, EntityServiceError.Duplicate::class)

    }


}