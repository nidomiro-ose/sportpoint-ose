package de.nidomiro.sportpoint.backend.api.graphql.resolver

import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractGraphQLBaseTest
import de.nidomiro.sportpoint.backend.of
import de.nidomiro.sportpoint.backend.security.TestUser
import de.nidomiro.sportpoint.backend.service.dto.input.IdReferenceInput
import de.nidomiro.sportpoint.backend.service.dto.input.ReferenceCompetitionTypeInputDto
import de.nidomiro.sportpoint.backend.service.graphql.MyGraphQLResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ClubQueryResolverTest : AbstractGraphQLBaseTest() {

    @Test
    fun clubByIdExpectNotFound() {

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getClub.graphql",
            mapOf("id" to -10)
        )
        println(result.rawResponse.body)
        assertThat(result["$.data.result"]).isNull()

    }

    @Test
    fun createClubTestFail() {
        val values = mapOf(
            "name" to "1",
            "emailAddress" to "",
            "postalAddress" to
                    mapOf(
                        "streetWithNumber" to "",
                        "postalCode" to "",
                        "city" to ""
                    )
        )

        val result =
            graphQLClient.createClub(
                values = values,
                assertResultNotNull = false,
                accessToken = ssoClient.getAccessToken(TestUser.Admin)
            )
        println(result.rawResponse.body)
        val result2 =
            graphQLClient.createClub(
                values = values,
                assertResultNotNull = false,
                accessToken = ssoClient.getAccessToken(TestUser.Admin)
            )
        println(result2.rawResponse.body)

        assertThat(result2["$.errors[0].errorType"]).isEqualTo("DataFetchingException")
        assertThat(result2["$.errors[0].message", Any::class.java]).isNotNull

        assertThat(result2["$.errors[0].extensions.errorCode"]).isEqualTo("Duplicate")


    }

    @Test
    fun createClubTest() {

        val values = mapOf(
            "name" to "BSC Musterstadt",
            "emailAddress" to "mail@example.org",
            "postalAddress" to
                    mapOf(
                        "streetWithNumber" to "Mustertraße 1",
                        "postalCode" to "12345",
                        "city" to "Musterstadt"
                    )
        )

        val result = graphQLClient.createClub(
            values = values,
            accessToken = ssoClient.getAccessToken(TestUser.Admin)
        )
        println(result.rawResponse.body)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")
        compareResult(compareBinding, "emailAddress")
        compareResult(compareBinding, "phoneNumber")

        assertThat(result["$.data.result.postalAddress", Any::class.java]).isNotNull
        compareResult(compareBinding, "postalAddress", "streetWithNumber")
        compareResult(compareBinding, "postalAddress", "postalCode")
        compareResult(compareBinding, "postalAddress", "city")
        assertThat(result["$.data.result.competitions", Any::class.java]).isNotNull


    }

    @Test
    fun createClubTestWithoutRole() {

        val values = mapOf(
            "name" to "BSC Musterstadt",
            "emailAddress" to "mail@example.org",
            "postalAddress" to
                    mapOf(
                        "streetWithNumber" to "Mustertraße 1",
                        "postalCode" to "12345",
                        "city" to "Musterstadt"
                    )
        )

        val result = graphQLClient.createClub(values = values, accessToken = ssoClient.getAccessToken(TestUser.Admin))
        println(result.rawResponse.body)

        val compareBinding = bindCompareResult(result, values)

        assertThat(result["$.data.result", Any::class.java]).isNotNull
        assertThat(result["$.data.result.id"]).isNotNull()
        compareResult(compareBinding, "name")
        compareResult(compareBinding, "emailAddress")
        compareResult(compareBinding, "phoneNumber")

        assertThat(result["$.data.result.postalAddress", Any::class.java]).isNotNull
        compareResult(compareBinding, "postalAddress", "streetWithNumber")
        compareResult(compareBinding, "postalAddress", "postalCode")
        compareResult(compareBinding, "postalAddress", "city")
        assertThat(result["$.data.result.competitions", Any::class.java]).isNotNull


    }

    @Test
    fun clubById() {

        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))

        val result = graphQLTemplate.perform(
            "classpath:graphQLs/getClub.graphql",
            mapOf("id" to club["$.data.result.id"])
        )
        assertThat(result["$.data.result", Any::class.java]).isNotNull
    }


    @Test
    fun competitionsOfClub() {

        val competitionTypeDto: ReferenceCompetitionTypeInputDto =
            IdReferenceInput.of(graphQLClient.createCompetitionType())

        val club1 = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competition1 = graphQLClient.createCompetition(
            organizer = IdReferenceInput.of(club1),
            type = competitionTypeDto
        )

        val club2 = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competition2 = graphQLClient.createCompetition(
            organizer = IdReferenceInput.of(club2),
            type = competitionTypeDto
        )


        val returnClub1 = graphQLTemplate.perform(
            "classpath:graphQLs/getClub.graphql",
            mapOf("id" to club1["$.data.result.id"])
        )
        println("returnClub1: ${returnClub1.rawResponse.body}")

        val competitionsOfClub1 = returnClub1["$.data.result.competitions.edges", List::class.java]
        assertThat(competitionsOfClub1).isNotNull
        assertThat(competitionsOfClub1).hasSize(1)
        assertThat(returnClub1["$.data.result.competitions.edges[0].node.id"])
            .isEqualTo(competition1["$.data.result.id"])


        val resultClub2 = graphQLTemplate.perform(
            "classpath:graphQLs/getClub.graphql",
            mapOf("id" to club2["$.data.result.id"])
        )
        val competitionsOfClub2 = resultClub2["$.data.result.competitions.edges", List::class.java]
        assertThat(competitionsOfClub2).isNotNull
        assertThat(competitionsOfClub2).hasSize(1)
        assertThat(resultClub2["$.data.result.competitions.edges[0].node.id"])
            .isEqualTo(competition2["$.data.result.id"])
    }

    @Test
    fun multipleCompetitionsOfClub() {

        val competitionTypeDto: ReferenceCompetitionTypeInputDto =
            IdReferenceInput.of(graphQLClient.createCompetitionType())

        val club = graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin))
        val competitions = listOf(
            graphQLClient.createCompetition(
                organizer = IdReferenceInput.of(club),
                type = competitionTypeDto
            ),
            graphQLClient.createCompetition(
                organizer = IdReferenceInput.of(club),
                type = competitionTypeDto
            )
        )

        val returnClub = graphQLTemplate.perform(
            "classpath:graphQLs/getClub.graphql",
            mapOf("id" to club["$.data.result.id"])
        )
        val competitionsOfClub = returnClub["$.data.result.competitions.edges", List::class.java]
        assertThat(competitionsOfClub).isNotNull
        assertThat(competitionsOfClub).hasSize(2)

        fun checkResult(competitionNum: Int, competitionGQLResponse: MyGraphQLResponse) {
            assertThat(returnClub["$.data.result.competitions.edges[$competitionNum].node.id"]).isEqualTo(
                competitionGQLResponse["$.data.result.id"]
            )
        }
        competitions.forEachIndexed { index, MyGraphQLResponse -> checkResult(index, MyGraphQLResponse) }


    }


    @Test
    fun getAllClubs() {

        val pageSize = 8
        val valuesCount = 10

        (1..valuesCount).map { graphQLClient.createClub(accessToken = ssoClient.getAccessToken(TestUser.Admin)) }


        val firstValues = graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllClubs",
            first = pageSize
        )


        graphQLClient.executeAndCheckConnectionRequest(
            fileName = "getAllClubs",
            first = pageSize,
            after = firstValues["$.data.result.edges[${pageSize - 1}].cursor"]
        )

    }


}







