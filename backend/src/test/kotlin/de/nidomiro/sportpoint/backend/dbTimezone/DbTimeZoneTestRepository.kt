package de.nidomiro.sportpoint.backend.dbTimezone

import de.nidomiro.sportpoint.backend.repository.SimpleRepository
import org.springframework.stereotype.Repository

@Repository
interface DbTimeZoneTestRepository : SimpleRepository<DbTimezoneTestEntity, Long> {
}