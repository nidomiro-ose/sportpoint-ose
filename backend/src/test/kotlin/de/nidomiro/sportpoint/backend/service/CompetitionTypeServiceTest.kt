package de.nidomiro.sportpoint.backend.service

import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.assertFailure
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.error.EntityServiceError
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class CompetitionTypeServiceTest : AbstractBaseTestWithoutGraphQL() {


    @Autowired
    lateinit var competitionTypeService: CompetitionTypeService

    @Test
    fun existsByName() {
        val value = exampleGenerator.competitionTypeEntity()

        assertThat(competitionTypeService.existsByName(value.name)).isFalse()

        competitionTypeService.createNew(value)

        assertThat(competitionTypeService.existsByName(value.name)).isTrue()
    }

    @Test
    fun findAll() {
        val values = (1..5)
            .asSequence()
            .map { exampleGenerator.competitionTypeEntity() }
            .map { competitionTypeService.createNew(it).getOrThrowGraphQlException() }
            .toList()

        assertThat(competitionTypeService.findAll()).containsAll(values)
    }

    @Test
    fun findById() {
        val value = exampleGenerator.competitionTypeEntity()
            .let { competitionTypeService.createNew(it) }
            .getOrThrowGraphQlException()

        assertThat(competitionTypeService.findById(value.id)).isEqualTo(value)
    }

    @Test
    fun createNewWithSuccess() {
        val value = exampleGenerator.competitionTypeEntity()
        competitionTypeService.createNew(value)
    }

    @Test
    fun createNewWithDuplicateNameExpectFailure() {
        val value = exampleGenerator.competitionTypeEntity()
        competitionTypeService.createNew(value)

        val value2 = exampleGenerator.competitionTypeEntity().copy(name = value.name)
        val result = competitionTypeService.createNew(value2)
        assertFailure(result, EntityServiceError.Duplicate::class)
    }

    @Test
    fun update() {
        val value = exampleGenerator.competitionTypeEntity()
        val savedValue = competitionTypeService.createNew(value).getOrThrowGraphQlException()

        val alteredValue = savedValue.copy(description = "Updated description")
        val alteredSavedValue = competitionTypeService.update(alteredValue).getOrThrowGraphQlException()
        assertThat(alteredSavedValue).isEqualTo(alteredValue.copy(lastModified = alteredSavedValue.lastModified))

        val alteredValueById = competitionTypeService.findById(savedValue.id)!!
        assertThat(alteredValueById).isEqualTo(alteredValue.copy(lastModified = alteredValueById.lastModified))
    }

    @Test
    fun `check lastModified updates correctly`() {
        val value = exampleGenerator.competitionTypeEntity()
        val savedValue = competitionTypeService.createNew(value).getOrThrowGraphQlException()

        savedValue.description = "Updated description"
        competitionTypeService.update(savedValue).getOrThrowGraphQlException()


        val alteredValueById = competitionTypeService.findById(savedValue.id)!!
        assertThat(alteredValueById.creationDate).isBefore(alteredValueById.lastModified)
    }


}