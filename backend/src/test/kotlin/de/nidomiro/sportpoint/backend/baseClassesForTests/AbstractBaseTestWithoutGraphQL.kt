package de.nidomiro.sportpoint.backend.baseClassesForTests

import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@TestPropertySource(
    properties = [
        "spring.autoconfigure.exclude=com.oembedler.moon.graphql.boot.GraphQLWebAutoConfiguration",
        "spring.autoconfigure.exclude=com.oembedler.moon.graphql.boot.GraphQLWebsocketAutoConfiguration"
    ]
)
@RunWith(SpringRunner::class)
@SpringBootTest
abstract class AbstractBaseTestWithoutGraphQL : AbstractCommonTestProperties()