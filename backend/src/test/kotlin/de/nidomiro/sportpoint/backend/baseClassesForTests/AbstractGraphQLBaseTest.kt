package de.nidomiro.sportpoint.backend.baseClassesForTests

import de.nidomiro.sportpoint.backend.service.graphql.GraphQLClient
import de.nidomiro.sportpoint.backend.service.graphql.MyGraphQLResponse
import de.nidomiro.sportpoint.backend.service.graphql.TokenGraphQLTestTemplate
import junit.framework.TestCase.fail
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class AbstractGraphQLBaseTest : AbstractCommonTestProperties() {

    @LocalServerPort
    protected var serverPort: Int = 0

    @Autowired
    protected lateinit var graphQLTemplate: TokenGraphQLTestTemplate

    protected lateinit var graphQLClient: GraphQLClient


    override fun setup() {
        super.setup()
        graphQLClient = GraphQLClient(graphQLTemplate, exampleGenerator)
    }


    fun compareResult(binding: (paths: Array<out String>) -> Unit, vararg paths: String) = binding(paths)

    fun bindCompareResult(
        response: MyGraphQLResponse,
        data: Map<String, Any?>,
        responsePrefix: String = "$.data.result"
    ): (paths: Array<out String>) -> Unit {
        return { paths: Array<out String> ->
            this.compareResult(response, data, responsePrefix, *paths)
        }
    }


    fun compareResult(
        response: MyGraphQLResponse,
        data: Map<String, Any?>,
        responsePrefix: String,
        vararg paths: String
    ) {
        assert(paths.isNotEmpty())

        var equalToValue: Any? = null
        var currentData: Map<String, Any?> = data
        paths.forEachIndexed { i, path ->
            val tmp = currentData[path]

            when {
                i == paths.lastIndex -> equalToValue = tmp

                tmp is Map<*, *> ->
                    @Suppress("UNCHECKED_CAST")
                    currentData = tmp as Map<String, Any?>

                else -> fail("Multiple paths given, but no Map in data")
            }
        }

        val responseKey = responsePrefix + "." + paths.joinToString(".")
        assertThat(response[responseKey, Any::class.java]?.toString()).isEqualTo(equalToValue?.toString())
    }




















}