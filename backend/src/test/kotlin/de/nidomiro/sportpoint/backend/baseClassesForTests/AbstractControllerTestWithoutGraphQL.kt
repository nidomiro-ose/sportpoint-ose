package de.nidomiro.sportpoint.backend.baseClassesForTests

import org.junit.Assert
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.mock.http.MockHttpOutputMessage
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import java.io.IOException
import java.nio.charset.Charset

@WebAppConfiguration
@AutoConfigureMockMvc
abstract class AbstractControllerTestWithoutGraphQL(
    protected val controllerAddress: String,
    protected val contentType: MediaType = MediaType(
        MediaType.APPLICATION_JSON.type,
        MediaType.APPLICATION_JSON.subtype,
        Charset.forName("utf8")
    )
) : AbstractBaseTestWithoutGraphQL() {

    private lateinit var mappingJackson2HttpMessageConverter: HttpMessageConverter<in Any>

    @Autowired
    protected lateinit var mockMvc: MockMvc


    @Autowired
    fun setConverters(converters: Array<HttpMessageConverter<*>>) {

        @Suppress("UNCHECKED_CAST")
        this.mappingJackson2HttpMessageConverter = converters.first { it is MappingJackson2HttpMessageConverter }
                as HttpMessageConverter<in Any>

        Assert.assertNotNull(
            "the JSON message converter must not be null",
            this.mappingJackson2HttpMessageConverter
        )
    }

    @Throws(IOException::class)
    protected fun json(o: Any): String {
        val mockHttpOutputMessage = MockHttpOutputMessage()
        this.mappingJackson2HttpMessageConverter.write(
            o, MediaType.APPLICATION_JSON, mockHttpOutputMessage
        )
        return mockHttpOutputMessage.bodyAsString
    }

}