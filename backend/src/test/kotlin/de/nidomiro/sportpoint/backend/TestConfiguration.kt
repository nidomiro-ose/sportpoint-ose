package de.nidomiro.sportpoint.backend

import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy

//@Configuration
class TestConfig {

    //@Bean
    //@Primary
    fun clean(): FlywayMigrationStrategy {
        return FlywayMigrationStrategy { flyway ->
            flyway?.clean()
            flyway?.migrate()
        }
    }
}