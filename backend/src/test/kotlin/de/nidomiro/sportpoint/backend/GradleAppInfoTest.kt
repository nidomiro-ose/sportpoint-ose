package de.nidomiro.sportpoint.backend

import org.junit.Test
import java.util.*


class GradleAppInfoTest {

    @Test
    fun testPropertiesFile() {
        val prop = Properties()
        prop.load(this.javaClass.classLoader.getResourceAsStream("app-info.properties"))
        println(prop.toString())
    }
}