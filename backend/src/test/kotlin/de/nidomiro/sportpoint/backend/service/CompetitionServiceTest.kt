package de.nidomiro.sportpoint.backend.service


import de.nidomiro.sportpoint.backend.api.graphql.error.getOrThrowGraphQlException
import de.nidomiro.sportpoint.backend.baseClassesForTests.AbstractBaseTestWithoutGraphQL
import de.nidomiro.sportpoint.backend.service.dto.input.toInputDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

internal class CompetitionServiceTest : AbstractBaseTestWithoutGraphQL() {

    @Autowired
    lateinit var competitionService: CompetitionService

    @Autowired
    lateinit var clubService: ClubService

    @Autowired
    lateinit var competitionTypeService: CompetitionTypeService

    @Test
    fun createNew() {

        val club = createRandomClub().getOrThrowGraphQlException()
        val competitionType = createRandomCompetitionType().getOrThrowGraphQlException()

        val competition = exampleGenerator.competitionEntity(
            organizer = club,
            competitionType = competitionType
        )

        val savedCompetition = competitionService.createNew(competition).getOrThrowGraphQlException()


        val allCompetitions = competitionService.findAll()

        assertThat(allCompetitions.size).isEqualTo(1)
        assertThat(allCompetitions[0]).isEqualTo(competition.copy(id = savedCompetition.id))

    }

    private fun createRandomClub() = clubService.createNew(exampleGenerator.clubEntity().toInputDto())

    private fun createRandomCompetitionType() =
        competitionTypeService.createNew(exampleGenerator.competitionTypeEntity().toInputDto())
}