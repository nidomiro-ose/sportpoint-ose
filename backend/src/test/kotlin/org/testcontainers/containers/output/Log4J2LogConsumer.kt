package org.testcontainers.containers.output

import org.apache.logging.log4j.Logger

class Log4J2LogConsumer(private val logger: Logger) : BaseConsumer<Log4J2LogConsumer>() {
    private var prefix = ""

    fun withPrefix(prefix: String): Log4J2LogConsumer {
        this.prefix = "[$prefix] "
        return this
    }

    override fun accept(outputFrame: OutputFrame) {
        val outputType = outputFrame.type

        var utf8String = outputFrame.utf8String
        utf8String = utf8String.replace(FrameConsumerResultCallback.LINE_BREAK_AT_END_REGEX.toRegex(), "")
        when (outputType) {
            OutputFrame.OutputType.END -> {
            }
            OutputFrame.OutputType.STDOUT, OutputFrame.OutputType.STDERR -> logger.info(
                "{}{}: {}",
                prefix,
                outputType,
                utf8String
            )
            else -> throw IllegalArgumentException("Unexpected outputType $outputType")
        }
    }
}