import PersonDto from "../app/dto/PersonDto";
import CompetitionDto, {Visibility} from "../app/dto/CompetitionDto";
import ClubDto from "../app/dto/ClubDto";
import SportsClassDto from "../app/dto/SportsClassDto";
import {oc} from "ts-optchain.macro";


export const exampleCompetition: CompetitionDto = {
    id: 1,
    name: "Eddie Bull-Competition",
    date: "2022-05-21",
    organizer: {
        id: 5,
        displayName: "BSC Uhlemannland"
    },
    venueAddress: {
        streetWithNumber: "Adolfsstr. 40b",
        postalCode: "85636",
        city: "Klein Deliaburg"
    },
    maxParticipants: 22,
    currentParticipants: 0,
    competitionType: {
        id: 2,
        name: "Clytemnestra-Zeus-Battle",
        description: "Voluptate dolor labore id. Consequatur illo reiciendis. Eius repellat at quod aspernatur."
    },
    description: "Inventore numquam unde. Laborum culpa quis quis at hic animi dolor. Dignissimos et vel. Fugit vitae quod quia amet dicta neque omnis. Velit nisi aut qui quia id quia. Voluptatum laborum culpa quis unde sit. Soluta tempore assumenda cumque aut. Sint aut id minima maiores in esse. Vel mollitia consequatur aperiam aliquam dolor qui. Earum repudiandae temporibus qui sequi quidem accusantium. Consequuntur esse temporibus. Ut porro id quaerat et ut sed occaecati. Consequuntur consequuntur eum est quo provident aut. Porro dolores distinctio officia non rem et. Et architecto dicta voluptas iste numquam exercitationem suscipit. Praesentium in quam et corrupti labore. Est minima doloribus omnis iure. Ex dignissimos atque. Quaerat sit ea. Reprehenderit ipsa nihil ut labore. Ratione eaque enim quo sit aliquam dolorum sit. Maxime et aut delectus ad molestiae soluta. Quis est et eligendi. Debitis facere illo ut sed veritatis autem culpa. Sint modi non quia. Rerum hic nesciunt fuga id aut cum tempore. Tempore voluptatem molestiae voluptatem ipsum asperiores sit. Omnis omnis qui architecto. Dolorem iusto consequatur vel dolor occaecati. In et nisi voluptas. Dignissimos dolorem voluptas doloribus voluptas. Maiores labore sit quia optio. Molestiae neque inventore. Quos velit aut corrupti pariatur maxime. Est nihil cumque aut quia cumque ut. Aperiam quidem et dolores et sit labore eos. Mollitia delectus voluptatem. Rem eum labore inventore. Earum eum deleniti ipsum nostrum quia blanditiis. Fugit cumque error vitae. Earum et molestiae porro alias. Mollitia harum sed fugit necessitatibus vel. Et aperiam aut praesentium est consectetur sapiente magni. Nobis tempora autem ipsum veritatis dolorum sunt. Ducimus tempora tempora reiciendis deleniti harum doloremque. At eos labore. Repellendus ut voluptas nemo ut. Sed numquam veritatis aspernatur dolorem. Ut est sint quis enim consequatur sit. Ut qui neque unde iste qui.",
    visibility: Visibility.PUBLIC
};

export const exampleSportsClasses: Array<SportsClassDto> = [
    {
        id: 1,
        name: "Example Sports Class"
    },
    {
        id: 2,
        name: "Another Sports Class"
    },
    {
        id: 3,
        name: "A third Sports Class"
    }
];

export const exampleClubs: Array<ClubDto> = [
    {
        id: 1,
        displayName: "BSC A"
    },
    {
        id: 2,
        displayName: "BSC B"
    },
    {
        id: 3,
        displayName: "BSC C"
    }
];

export const examplePerson: PersonDto = {

    displayName: "Peter ABC",

    sportsClasses: {
        edges: exampleSportsClasses.map(x => ({node: x}))
    },
    clubs: {
        edges: exampleClubs.map(x => ({node: x}))
    }
};

export const examplePersonWithOneClassAndClub: PersonDto = {

    displayName: "Jürgen ABC",
    sportsClasses: {
        edges: exampleSportsClasses.slice(0, 1).map(x => ({node: x}))
    },
    clubs: {
        edges: exampleClubs.slice(1, 2).map(x => ({node: x}))
    }
};

const participants = [
    {
        competition: exampleCompetition,
        participant: examplePerson,
        participatingClub: oc(examplePerson).clubs.edges[0].node(),
        participatingSportsClass: oc(examplePerson).sportsClasses.edges[0].node(),
    },
    {
        competition: exampleCompetition,
        participant: examplePersonWithOneClassAndClub,
        participatingClub: oc(examplePersonWithOneClassAndClub).clubs.edges[0].node(),
        participatingSportsClass: oc(examplePersonWithOneClassAndClub).sportsClasses.edges[0].node(),
    }
];

export const exampleCompetitionWithParticipants: CompetitionDto = {
    ...exampleCompetition,
    participants: {
        edges: participants.map(x => ({node: x})),
    },
};
exampleCompetitionWithParticipants.currentParticipants = oc(exampleCompetitionWithParticipants).participants.edges.length(0);

