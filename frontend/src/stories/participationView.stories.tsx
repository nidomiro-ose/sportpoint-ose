import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs/react';


import React from "react";
import ParticipateView from "../app/ui/view/ParticipateView";
import {action} from "@storybook/addon-actions";
import {exampleCompetition, examplePerson} from "./DummyData";
import {oc} from 'ts-optchain.macro';
import StoriesDefaultUIProvider from "./StoriesDefaultUIProvider";


function createCommitParticipationCallBack() {
    return (sportsClassId: Number, clubId: Number) => {
        action("commitParticipation")({sportsClassId, clubId});
    };
}

storiesOf('ParticipateView', module)
    .addDecorator(withKnobs)
    .add("Empty", () => (
            <>
                <StoriesDefaultUIProvider>
                    <ParticipateView
                        competition={null}
                        person={null}
                        commitParticipation={createCommitParticipationCallBack()}/>
                </StoriesDefaultUIProvider>
            </>
        )
    )
    .add("With Values", () => (
            <>
                <StoriesDefaultUIProvider>
                    <ParticipateView
                        competition={exampleCompetition}
                        person={examplePerson}
                        commitParticipation={createCommitParticipationCallBack()}

                        backOnSuccess={false}

                        preselectClub={oc(examplePerson).clubs.edges[0].node()}
                        preselectSportsClass={oc(examplePerson).sportsClasses.edges[0].node()}
                    />
                </StoriesDefaultUIProvider>
            </>
        )
    );


/*
.add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
.add('with some emoji', () => (
    <Button onClick={action('clicked')}>
  <span role="img" aria-label="so cool">
    😀 😎 👍 💯
  </span>
    </Button>
));*/