import * as React from "react";
import UICustomizations from "../UICutomizations";
import {HashRouter} from "react-router-dom";


interface Props {
    children: JSX.Element[] | JSX.Element
}

const StoriesDefaultUIProvider: React.FC<Props> = (props: Props) => {
    return (<>
        <HashRouter>
            <UICustomizations>
                {props.children}
            </UICustomizations>
        </HashRouter>
    </>)
};

export default StoriesDefaultUIProvider;