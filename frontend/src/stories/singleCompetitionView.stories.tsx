import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs/react';


import React from "react";
import {exampleCompetition, exampleCompetitionWithParticipants} from "./DummyData";
import StoriesDefaultUIProvider from "./StoriesDefaultUIProvider";
import DisplayCompetitionView from "../app/ui/view/DisplayCompetitionView";


storiesOf('Single Competition view', module)
    .addDecorator(withKnobs)
    .add("Empty Competition", () => (
            <>
                <StoriesDefaultUIProvider>
                    <DisplayCompetitionView
                        competition={null}
                    />
                </StoriesDefaultUIProvider>
            </>
        )
    )
    .add("No Participants", () => (
            <>
                <StoriesDefaultUIProvider>
                    <DisplayCompetitionView
                        competition={exampleCompetition}
                    />
                </StoriesDefaultUIProvider>
            </>
        )
    )
    .add("Two Participants", () => (
            <>
                <StoriesDefaultUIProvider>
                    <DisplayCompetitionView
                        competition={exampleCompetitionWithParticipants}
                    />
                </StoriesDefaultUIProvider>
            </>
        )
    )

;
