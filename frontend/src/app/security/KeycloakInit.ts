import Keycloak from "keycloak-js";
import {AuthTokenProvider} from "../ApolloInit";


const keycloak = Keycloak({
    "realm": "sp-test",
    "url": "http://localhost:8180/auth",
    "ssl-required": "external",
    "clientId": "sp-frontend"
});

export enum SSOState {
    NONE,
    WAITING,
    AUTHENTICATED,
    AUTHENTICATION_FAILURE
}

export const keycloakAuthTokenProvider: AuthTokenProvider = {
    getAuthToken(): string | undefined {
        return keycloak.token
    }
};

export default keycloak;
