import React from "react";
import {Query} from "react-apollo";
import {gql} from "apollo-boost";
import {oc} from "ts-optchain.macro";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import PersonDto from "../dto/PersonDto";
import {PersonProp} from "./PropInterfaces";


const GET_PERSON_BY_ID_QUERY = gql`
    query ($id: ID!) {
        result: getPerson(id : $id) {
            id
            firstName
            name
            displayName
            birthDate
            emailAddress
            phoneNumber
            postalAddress {
                streetWithNumber
                postalCode
                city
            }
            sportsClasses {
                edges {
                    cursor,
                    node {
                        id
                        name
                    }
                },
                pageInfo {
                    hasPreviousPage,
                    hasNextPage,
                    startCursor,
                    endCursor
                }
            }
        }
    }
`;

interface Data {
    result: PersonDto;
}

interface Variables {
    id: number;
}

interface OwnProps {
    personId: number

    children(props: PersonProp): JSX.Element
}

type Props = OwnProps

const SinglePersonByIdService: React.FC<Props> = (props: Props) => {
    return (
        <Query<Data, Variables> query={GET_PERSON_BY_ID_QUERY} variables={{id: props.personId}}>
            {
                ({loading, error, data}) => {
                    if (loading) return <LoadingSpinnerView/>;
                    if (error) console.log(error);

                    console.log(data);

                    return props.children({
                        person: oc(data).result({})
                    })
                }
            }
        </Query>
    );
};


export default SinglePersonByIdService;