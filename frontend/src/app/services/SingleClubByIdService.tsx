import React from "react";
import {Query} from "react-apollo";
import {gql} from "apollo-boost";
import {oc} from "ts-optchain.macro";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import ClubDto from "../dto/ClubDto";
import {ClubProp} from "./PropInterfaces";


const GET_CLUB_BY_ID_QUERY = gql`
    query ($id: ID!) {
        result: getClub(id : $id) {
            id
            name
            displayName
            emailAddress
            phoneNumber
            postalAddress {
                streetWithNumber
                postalCode
                city
            }
            competitions {
                edges {
                    cursor,
                    node {
                        id
                        name
                        date
                        organizer {
                            id
                            displayName
                        }
                        venueAddress {
                            streetWithNumber
                            postalCode
                            city
                        }
                        maxParticipants
                        competitionType {
                            id
                            name
                            description
                        }
                        description
                        visibility
                    }
                }
            }
        }

    }
`;

interface Data {
    result: ClubDto;
}

interface Variables {
    id: number;
}

interface OwnProps {
    clubId: number

    children(clubProp: ClubProp): JSX.Element
}

type Props = OwnProps

const SingleClubByIdService: React.FC<Props> = (props: Props) => {
    return (
        <Query<Data, Variables> query={GET_CLUB_BY_ID_QUERY} variables={{id: props.clubId}}>
            {
                ({loading, error, data}) => {
                    if (loading) return <LoadingSpinnerView/>;
                    if (error) console.log(error);

                    console.log(data);

                    return props.children({
                        club: oc(data).result({})
                    })
                }
            }
        </Query>
    );
};


export default SingleClubByIdService;