import PersonDto from "../dto/PersonDto";
import ClubDto from "../dto/ClubDto";
import CompetitionDto from "../dto/CompetitionDto";


export interface PersonProp {
    person: PersonDto | null | undefined
}

export interface ClubProp {
    club: ClubDto | null | undefined
}

export interface CompetitionProp {
    competition: CompetitionDto | null | undefined
}

export interface CompetitionsProp {
    competitions: Array<CompetitionDto> | null | undefined
}

export interface PersonCompetitionProp extends PersonProp {
    competition: CompetitionDto | null | undefined
}

export interface SelfParticipateProp extends PersonCompetitionProp {
    participate(sportsClassId: Number, clubId: Number): Boolean
}