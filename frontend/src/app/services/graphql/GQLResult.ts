import {ApolloError} from "apollo-client";


export interface GQLResult<TData> {
    data?: TData;
    error?: ApolloError;
    loading: boolean;
    called: boolean;
}