import React from "react";
import {Query} from "react-apollo";
import {gql} from "apollo-boost";
import CompetitionDto from "../dto/CompetitionDto";
import {oc} from "ts-optchain.macro";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import {CompetitionProp} from "./PropInterfaces";


const GET_COMPETITION_BY_ID_QUERY = gql`
    query ($id: ID!) {
        result: getCompetition(id : $id) {
            id
            name
            date
            organizer {
                id
                name
                displayName
            }
            venueAddress {
                streetWithNumber
                postalCode
                city
            }
            maxParticipants
            competitionType {
                id
                name
            }
            description
            visibility
        }

    }
`;

interface Data {
    result: CompetitionDto;
}

interface Variables {
    id: number;
}

interface OwnProps {
    competitionId: number

    children(competitionProp: CompetitionProp): JSX.Element
}

type Props = OwnProps

const SingleCompetitionByIdService: React.FC<Props> = (props: Props) => {
    return (
        <Query<Data, Variables> query={GET_COMPETITION_BY_ID_QUERY} variables={{id: props.competitionId}}>
            {
                ({loading, error, data}) => {
                    if (loading) return <LoadingSpinnerView/>;
                    if (error) console.log(error);

                    console.log(data);

                    return props.children({
                        competition: oc(data).result({})
                    })
                }
            }
        </Query>
    );
};


export default SingleCompetitionByIdService;