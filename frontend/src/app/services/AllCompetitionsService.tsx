import React from "react";
import {Query} from "react-apollo";
import {gql} from "apollo-boost";
import CompetitionDto from "../dto/CompetitionDto";
import {oc} from "ts-optchain.macro";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import FeedValuesDto from "../dto/FeedValuesDto";
import {CompetitionsProp} from "./PropInterfaces";


const GET_COMPETITION_BY_ID_QUERY = gql`
    query ($first: Int!, $after: String) {
        result: getAllCompetitions(first: $first, after: $after) @connection(key: "allCompetitions"){
            edges {
                cursor,
                node {
                    id
                    name
                    date
                    organizer {
                        id
                        displayName
                    }
                    venueAddress {
                        streetWithNumber
                        postalCode
                        city
                    }
                    maxParticipants
                    competitionType {
                        id
                        name
                        description
                    }
                    description
                    visibility
                }
            },
            pageInfo {
                hasPreviousPage,
                hasNextPage,
                startCursor,
                endCursor
            }
        }
    }
`;

interface Data {
    result: FeedValuesDto<CompetitionDto>;
}

interface Variables {
    first: number;
    after?: string | null | undefined;
}

interface Props {
    children(competitions: CompetitionsProp): JSX.Element
}

const AllCompetitionsService: React.FC<Props> = (props: Props) => {
    return (
        <Query<Data, Variables> query={GET_COMPETITION_BY_ID_QUERY} variables={
            {
                first: 1000
            }
        }>
            {
                ({loading, error, data, fetchMore}) => {
                    if (loading) return <LoadingSpinnerView/>;
                    if (error) console.log(error);

                    console.log(data);

                    return props.children({
                        competitions: oc(data).result.edges([]).map(edge => edge.node)
                    })
                }
            }
        </Query>
    );
};


export default AllCompetitionsService;