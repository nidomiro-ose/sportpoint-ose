import React from "react";
import {Query} from "react-apollo";
import {gql} from "apollo-boost";
import {oc} from "ts-optchain.macro";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import PersonDto from "../dto/PersonDto";
import {useKeycloak} from "react-keycloak";
import {PersonProp} from "./PropInterfaces";


const GET_MY_PROFILE_QUERY = gql`
    query  {
        result: getMyProfile {
            id
            firstName
            name
            displayName
            birthDate
            emailAddress
            phoneNumber
            postalAddress {
                streetWithNumber
                postalCode
                city
            }
            sportsClasses {
                edges {
                    cursor,
                    node {
                        id
                        name
                    }
                },
                pageInfo {
                    hasPreviousPage,
                    hasNextPage,
                    startCursor,
                    endCursor
                }
            }
        }
    }
`;

interface Data {
    result: PersonDto;
}

interface Props {
    children(props: PersonProp): JSX.Element
}


const MyProfileService: React.FC<Props> = (props: Props) => {

    const {keycloak, initialized} = useKeycloak();

    if (initialized) {
        if (keycloak.authenticated) {
            return (
                <Query<Data> query={GET_MY_PROFILE_QUERY}>
                    {
                        ({loading, error, data}) => {
                            if (loading) return <LoadingSpinnerView message={"Loading Profile"}/>;
                            if (error) console.log("QueryError: " + error);

                            return props.children({
                                person: oc(data).result({})
                            })
                        }
                    }
                </Query>
            )
        } else {
            return <p>You need to <a href={keycloak.createLoginUrl()}>Login</a> to access your Profile </p>
        }

    } else {
        return <LoadingSpinnerView message={"Waiting for SSO"}/>
    }
};


export default MyProfileService;