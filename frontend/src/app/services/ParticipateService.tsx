import {gql} from "apollo-boost";
import PersonDto from "../dto/PersonDto";
import React from "react";
import {useKeycloak} from "react-keycloak";
import {Mutation, MutationFn, MutationResult, Query} from "react-apollo";
import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import {oc} from "ts-optchain.macro";
import CompetitionDto from "../dto/CompetitionDto";
import CreateSelfParticipationInputDto from "../dto/input/CreateSelfParticipationInputDto";
import ParticipationDto from "../dto/ParticipationDto";
import {GQLResult} from "./graphql/GQLResult";


const GET_PARTICIPATION_VALUES_QUERY = gql`
    query ($competitionId: ID!){
        result: getMyProfile {
            id
            firstName
            name
            displayName
            birthDate
            emailAddress
            phoneNumber
            postalAddress {
                streetWithNumber
                postalCode
                city
            }
            sportsClasses(first: 100) {
                edges {
                    cursor,
                    node {
                        id
                        name
                    }
                },
                pageInfo {
                    hasPreviousPage,
                    hasNextPage,
                    startCursor,
                    endCursor
                }
            }
            clubs(first: 100) {
                edges {
                    cursor,
                    node {
                        id
                        name
                        displayName
                        emailAddress
                        phoneNumber
                        postalAddress {
                            streetWithNumber
                            postalCode
                            city
                        }
        
                        creationDate
                        lastModified
                    }
                },
                pageInfo {
                    hasPreviousPage,
                    hasNextPage,
                    startCursor,
                    endCursor
                }
            }
        }
        competition: getCompetition(id: $competitionId) {
            id
            name
            date
            organizer {
                id
                name
                displayName
            }
            venueAddress {
                streetWithNumber
                postalCode
                city
            }
            maxParticipants
            competitionType {
                id
                name
            }
            description
            visibility
    
            creationDate
            lastModified
        }
    }
`;

const PARTICIPATE_MUTATION = gql`
    mutation ($input: CreateSelfParticipationInput!) {
        result: participate(
            input: $input
        ) {
            id
            competition {
                id
                name
                date
            }
            participant {
                id
                displayName
            }
            participatingSportsClass {
                id
                name
            }
            participatingClub {
                id
                displayName
            }
        }
    
    }
`;


interface Props {
    competitionId: Number

    children(props: {
        participate(sportsClassId: Number, clubId: Number): void,
        person: any,
        competition: any,
        mutationResult?: GQLResult<ParticipationDto>
    }): JSX.Element
}


interface QueryData {
    result: PersonDto
    competition: CompetitionDto
}

interface QueryVariables {
    competitionId: Number
}

interface MutationData {
    result: ParticipationDto
}

interface MutationVariables {
    input: CreateSelfParticipationInputDto
}

const ParticipateService: React.FC<Props> = (props: Props) => {

    const {keycloak, initialized} = useKeycloak();

    if (initialized) {
        if (keycloak.authenticated) {
            return (
                <Query<QueryData, QueryVariables> query={GET_PARTICIPATION_VALUES_QUERY}
                                                  variables={{competitionId: props.competitionId}}>
                    {
                        ({loading, error, data}) => {
                            if (loading) return <LoadingSpinnerView message={"Loading Data"}/>;
                            if (error) console.log("QueryError: " + error);

                            return (<>
                                <Mutation<MutationData, MutationVariables> mutation={PARTICIPATE_MUTATION}>
                                    {
                                        (mutateFn: MutationFn<MutationData, MutationVariables>, result: MutationResult<MutationData>) => {


                                            return props.children({
                                                person: oc(data).result({}),
                                                competition: oc(data).competition({}),
                                                mutationResult: {
                                                    ...result,
                                                    data: oc(result.data).result()
                                                },
                                                participate(sportsClassId: Number, clubId: Number) {
                                                    let input = {
                                                        competition: {id: props.competitionId},
                                                        participatingClub: {id: clubId},
                                                        participatingSportsClass: {id: sportsClassId}
                                                    };
                                                    // noinspection JSIgnoredPromiseFromCall
                                                    mutateFn({variables: {input}});
                                                }
                                            })
                                        }
                                    }

                                </Mutation>
                            </>);
                        }
                    }
                </Query>
            )
        } else {
            return <p>You need to <a href={keycloak.createLoginUrl()}>Login/Register</a> to participate at a Competition
            </p>
        }

    } else {
        return <LoadingSpinnerView message={"Waiting for SSO"}/>
    }
};


export default ParticipateService;