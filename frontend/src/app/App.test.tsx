import React, {ComponentType} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {HashRouter} from "react-router-dom";
import keycloak from "./security/KeycloakInit";
import {forceCast} from "./Utils";
import LoadingSpinnerView from "./ui/components/LoadingSpinnerView";
import {KeycloakProvider} from "react-keycloak";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <KeycloakProvider keycloak={keycloak}
                          initConfig={{onLoad: 'check-sso'}}
                          LoadingComponent={forceCast<ComponentType>(<LoadingSpinnerView
                              message={"Initializing App"}/>)}
                          onToken={(token: string) => {
                              console.log("New Token from SSO: ", token)
                          }}
        >
            <HashRouter>
                <App/>
            </HashRouter>
        </KeycloakProvider>
        , div);
    ReactDOM.unmountComponentAtNode(div);
});
