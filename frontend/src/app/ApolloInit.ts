import {ApolloClient, DefaultOptions} from 'apollo-client'
import {createHttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory'
import {ApolloLink, NextLink, Operation} from 'apollo-link';


let sharedApolloCache = new InMemoryCache();

const defaultOptions: DefaultOptions = {
    watchQuery: {
        fetchPolicy: 'cache-and-network'
    },
};

const httpLink = createHttpLink({
    uri: 'http://localhost:8090/graphql',
    credentials: 'include'
});

export interface AuthTokenProvider {
    getAuthToken(): string | undefined
}

function createAuthLink(authTokenProvider: AuthTokenProvider) {
    return new ApolloLink((operation: Operation, forward?: NextLink) => {

        let authToken = authTokenProvider.getAuthToken();

        console.log("Calling ApolloLink with keycloakToken existent:", !(authToken == null || authToken === ""));

        // Use the setContext method to set the HTTP headers.
        operation.setContext({
            headers: authToken ? {
                Authorization: `Bearer ${authToken}`
            } : {}
        });

        // Call the next link in the middleware chain.
        if (forward != null) {
            return forward(operation);
        } else {
            return null;
        }
    });
}

export function createApolloClient(authTokenProvider: AuthTokenProvider) {

    console.log("createApolloClient called");

    return new ApolloClient({
        link: createAuthLink(authTokenProvider).concat(httpLink),
        cache: sharedApolloCache,
        defaultOptions: defaultOptions
    })
}
