export function forceCast<T>(input: any): T {
    // @ts-ignore <-- forces TS compiler to compile this as-is
    return input;
}

export function origin(): string {
    return window.location.origin
}


export function updateStateIfChanged<T>(state: T, setState: (newState: T) => void, newStateValues: Partial<T>) {
    let newSate = {
        ...state,
        ...newStateValues
    };
    if (newSate !== state) {
        setState(newSate)
    }
}
