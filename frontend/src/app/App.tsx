import React from 'react';
import {CssBaseline, makeStyles, Theme} from "@material-ui/core";
import {RouteComponentProps, withRouter} from "react-router";
import Routes from "./navigation/Routes";
import ApplicationAppBarView from "./ui/components/ApplicationAppBarView";


const styles = makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(2),
        height: '100vh',
        overflow: 'auto',
    },
    appBarSpacer: theme.mixins.toolbar,
    })
);


type Props = RouteComponentProps<any>

const App: React.FC<Props> = (props: Props) => {

    const classes = styles();

    return (<>
        <CssBaseline/>
        <div className={classes.root}>
            <ApplicationAppBarView title={"SportPoint"}/>
            <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Routes/>
            </main>
        </div>
    </>);
};

export default withRouter(App)
