import ReferenceInputDto from "./ReferenceInputDto";


export default interface CreateSelfParticipationInputDto {
    competition: ReferenceInputDto
    participatingSportsClass: ReferenceInputDto
    participatingClub: ReferenceInputDto
}