import ClubDto from "./ClubDto";
import PostalAddressDto from "./PostalAddressDto";
import CompetitionTypeDto from "./CompetitionTypeDto";
import ParticipationDto from "./ParticipationDto";
import FeedValuesDto from "./FeedValuesDto";


export enum Visibility {
    PUBLIC = "PUBLIC",
    PRIVATE = "PRIVATE",
    SEMI_PRIVATE = "SEMI_PRIVATE"
}


export default interface CompetitionDto {
    id?: number,

    name?: string,

    date?: string,

    organizer?: ClubDto,

    venueAddress?: PostalAddressDto,

    maxParticipants?: number,

    currentParticipants?: number,

    participants?: FeedValuesDto<ParticipationDto>,

    competitionType?: CompetitionTypeDto,

    description?: string,

    visibility?: Visibility
}

