export default interface FeedValuesDto<T> {
    edges?: EdgeDto<T>[]

    pageInfo?: PageInfoDto
}

export interface EdgeDto<T> {
    cursor?: String
    node: T
}

export interface PageInfoDto {
    hasPreviousPage?: boolean
    hasNextPage?: boolean
    startCursor?: string
    endCursor?: string
}
