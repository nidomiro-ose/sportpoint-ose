import PostalAddressDto from "./PostalAddressDto";
import SportsClassDto from "./SportsClassDto";
import FeedValuesDto from "./FeedValuesDto";
import ClubDto from "./ClubDto";


export default interface PersonDto {
    id?: number,

    firstName?: string,

    name?: string,

    displayName?: string,

    birthDate?: string,

    emailAddress?: string,

    phoneNumber?: string,

    postalAddress?: PostalAddressDto,

    sportsClasses?: FeedValuesDto<SportsClassDto>,

    clubs?: FeedValuesDto<ClubDto>
}