import PostalAddressDto from "./PostalAddressDto";
import CompetitionDto from "./CompetitionDto";
import FeedValuesDto from "./FeedValuesDto";


export default interface ClubDto {
    id?: number,

    name?: string,

    displayName?: string,

    emailAddress?: string,

    phoneNumber?: string,

    postalAddress?: PostalAddressDto,

    competitions?: FeedValuesDto<CompetitionDto>
}