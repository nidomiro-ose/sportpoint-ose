export default interface CompetitionTypeDto {
    id?: number,

    name?: string,

    description?: string
}