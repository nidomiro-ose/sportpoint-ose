import SportsClassDto from "./SportsClassDto";
import ClubDto from "./ClubDto";
import CompetitionDto from "./CompetitionDto";
import PersonDto from "./PersonDto";


export default interface ParticipationDto {
    id?: number,

    competition?: CompetitionDto,

    participant?: PersonDto,

    participatingSportsClass?: SportsClassDto,

    participatingClub?: ClubDto
}