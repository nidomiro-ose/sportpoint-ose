import LoadingSpinnerView from "../ui/components/LoadingSpinnerView";
import {Route, RouteComponentProps, Switch} from "react-router";
import * as React from "react";
import {Typography} from "@material-ui/core";
import SingleClubByIdService from "../services/SingleClubByIdService";
import SinglePersonByIdService from "../services/SinglePersonByIdService";
import MyProfileService from "../services/MyProfileService";
import Links from "./Links";
import DisplayPerson from "../ui/view/DisplayPersonView";
import DisplayClub from "../ui/view/DisplayClubView";
import DisplayCompetition from "../ui/view/DisplayCompetitionView";
import DisplayCompetitionList from "../ui/view/DisplayCompetitionListView";
import AdminPageView from "../ui/view/AdminPageView";
import ParticipateService from "../services/ParticipateService";
import ParticipateView from "../ui/view/ParticipateView";


const Home = React.lazy(() => import("../ui/view/HomeView"));
const SingleCompetitionByIdService = React.lazy(() => import("../services/SingleCompetitionByIdService"));
const AllCompetitionsService = React.lazy(() => import("../services/AllCompetitionsService"));

function Routes() {
    return (
        <React.Suspense fallback={<LoadingSpinnerView/>}>
            <Switch>
                <Route exact path={Links.Root} render={() => <Home/>}/>

                <Route exact path={Links.AllCompetitions} render={
                    () => (
                        <AllCompetitionsService>
                            {competitionsProp => (
                                <DisplayCompetitionList competitions={competitionsProp.competitions}/>)}
                        </AllCompetitionsService>)
                }/>

                <Route exact path={Links.CompetitionById} render={
                    (props: RouteComponentProps<any>) => (
                        <SingleCompetitionByIdService competitionId={props.match.params.id}>
                            {competitionProp => (<DisplayCompetition competition={competitionProp.competition}/>)}
                        </SingleCompetitionByIdService>)
                }/>

                <Route exact path={Links.ClubById} render={
                    (props: RouteComponentProps<any>) => (
                        <SingleClubByIdService clubId={props.match.params.id}>
                            {clubProp => (<DisplayClub club={clubProp.club}/>)}
                        </SingleClubByIdService>)
                }/>

                <Route exact path={Links.PersonById} render={
                    (props: RouteComponentProps<any>) => (
                        <SinglePersonByIdService personId={props.match.params.id}>
                            {personProp => (<DisplayPerson person={personProp.person}/>)}
                        </SinglePersonByIdService>)
                }/>

                <Route exact path={Links.MyProfile} render={
                    (props: RouteComponentProps<any>) => (
                        <MyProfileService>
                            {personProp => (<DisplayPerson person={personProp.person}/>)}
                        </MyProfileService>)
                }/>

                <Route exact path={Links.AdminPage} render={() => <AdminPageView/>}/>

                <Route exact path={Links.ParticipateAtCompetitionById} render={
                    (props: RouteComponentProps<any>) => (
                        <ParticipateService competitionId={props.match.params.id}>
                            {personClubProp => (<ParticipateView person={personClubProp.person}
                                                                 competition={personClubProp.competition}
                                                                 mutationResult={personClubProp.mutationResult}
                                                                 commitParticipation={personClubProp.participate}/>)}
                        </ParticipateService>)
                }/>

                ParticipateAtCompetitionById


                <Route component={NoMatch}/>
            </Switch>
        </React.Suspense>
    )

}

const NoMatch = () => (
    <Typography variant={"h2"}>Nothing here (404)</Typography>
);


export default Routes;