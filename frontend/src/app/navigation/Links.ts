enum Links {
    Root = "/",

    AllCompetitions = "/competitions",
    CompetitionById = "/competition/:id",
    ParticipateAtCompetitionById = "/competition/:id/participate",

    ClubById = "/club/:id",
    PersonById = "/person/:id",

    MyProfile = "/myprofile",

    AdminPage = "/admin"

}


export default Links