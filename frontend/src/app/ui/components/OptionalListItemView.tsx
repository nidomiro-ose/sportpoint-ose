import {ListItem, ListItemText} from "@material-ui/core";
import * as React from "react";


interface Props<T> {
    label: string,
    value?: T,
    transform?: (value: T) => string,
    hideOnNullValue?: boolean,
    onClick?: (event: React.MouseEvent) => void
}

function OptionalListItemView<T>(props: Props<T>) {
    let {
        label,
        value,
        transform,
        hideOnNullValue,
        onClick
    } = props;


    let listItemProps = {};
    if (onClick != null) {
        listItemProps = {
            onClick: onClick,
            button: true
        }
    }

    let valToDisplay: string | null = calcValueToDisplay(value, transform, hideOnNullValue);


    if (valToDisplay == null) {
        return null;
    }
    return (<>
            <ListItem {...listItemProps}>
                <ListItemText primary={valToDisplay} secondary={label}/>
            </ListItem>
        </>
    );
}

function calcValueToDisplay<T>(value?: T,
                               transform?: (value: T) => string,
                               hideOnNullValue?: boolean)
    : string | null {
    let valToDisplay: string | null = null;

    if (value != null) {
        if (transform == null) {
            transform = (value) => value.toString();
        }
        valToDisplay = transform(value);
    } else {
        if (!(!!hideOnNullValue)) {
            valToDisplay = "";
        }
    }

    return valToDisplay;
}

export default OptionalListItemView;