import React from 'react';
import {makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AppBarProfileView from "./AppBarProfileView";
import {Link} from "react-router-dom";
import Links from "../../navigation/Links";
import {Home as HomeIcon} from "@material-ui/icons";

interface OwnProps {
    title: string
}

const styles = makeStyles((theme: Theme) => ({
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
        },
        toolbar: {
            paddingRight: 24, // keep right padding when drawer closed
        },
        toolbarIcon: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: '0 8px',
            ...theme.mixins.toolbar,
        },
        menuButton: {
            marginLeft: 4,
            marginRight: 36,
        },
        pageTitle: {
            flexGrow: 1,
        },
    })
);

type Props = OwnProps;

const ApplicationAppBarView: React.FC<Props> = (props: Props) => {
    const {
        title
    } = props;

    const classes = styles();

    const menuOpen = false;

    return (
        <AppBar
            position="absolute"
            className={classes.appBar}
        >
            <Toolbar disableGutters={!menuOpen} className={classes.toolbar}>

                <IconButton
                    color="inherit"
                    aria-label="open menu"
                    className={
                        classes.menuButton
                    }
                    component={Link}
                    to={Links.Root}
                >
                    {/*<MenuIcon/>*/}
                    <HomeIcon/>
                </IconButton>

                <Typography
                    component="h1"
                    variant="h6"
                    color="inherit"
                    noWrap
                    className={classes.pageTitle}
                >
                    {title}
                </Typography>

                <AppBarProfileView/>

            </Toolbar>
        </AppBar>
    );
};

export default ApplicationAppBarView

