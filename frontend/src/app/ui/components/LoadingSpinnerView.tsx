import * as React from "react";
import {CircularProgress} from "@material-ui/core";
import {oc} from "ts-optchain.macro";

interface Props {
    message?: string
}

const LoadingSpinnerView = (props: Props) => {

    return (
        <>

            <CircularProgress/> <p> {oc(props.message)("")}</p>

        </>
    )

};


export default LoadingSpinnerView;