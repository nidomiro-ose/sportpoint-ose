import React from "react";
import {useKeycloak} from "react-keycloak";
import Button from "@material-ui/core/Button";
import {IconButton, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";
import Links from "../../navigation/Links";
import {AccountCircle} from "@material-ui/icons";


const styles = makeStyles({
    logInOutButton: {},
});


const AppBarProfileView: React.FC = () => {

    const classes = styles();
    const {keycloak} = useKeycloak();

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    function handleMenu(event: React.MouseEvent<HTMLElement>) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }


    function loggedOutView() {
        return (<>
                <Button className={classes.logInOutButton}
                        color="inherit"
                        href={keycloak.createLoginUrl()}>Login</Button>
            </>
        );
    }

    function loggedInView() {
        return (<>
            <IconButton
                aria-label="Account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <AccountCircle/>
            </IconButton>
            <Menu
                id="menu-appbar"

                anchorEl={anchorEl}
                getContentAnchorEl={null}

                elevation={0}

                keepMounted
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                open={open}
                onClose={handleClose}
            >
                {keycloak.hasRealmRole("global_admin") &&
                <MenuItem onClick={handleClose}
                          component={Link}
                          to={Links.AdminPage}>Admin Console</MenuItem>
                }
                <MenuItem onClick={handleClose}
                          component={Link}
                          to={Links.MyProfile}>My Profile</MenuItem>

                <MenuItem onClick={
                    () => {
                        handleClose();
                        keycloak.logout();
                    }
                }>Logout</MenuItem>
            </Menu>
        </>)
    }


    if (keycloak.authenticated === true) {
        return loggedInView();
    } else {
        return loggedOutView();
    }

};

export default AppBarProfileView