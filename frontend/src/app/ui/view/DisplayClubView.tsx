import {List, ListItem, ListItemText, Typography} from "@material-ui/core";
import * as React from "react";
import {oc} from "ts-optchain.macro";
import {postalAddressToString} from "../../dto/PostalAddressDto";
import ClubDto from "../../dto/ClubDto";
import OptionalListItemView from "../components/OptionalListItemView";
import {RouteComponentProps, withRouter} from "react-router";
import CompetitionDto from "../../dto/CompetitionDto";


interface OwnProps {
    club: ClubDto | null | undefined
}

type Props = RouteComponentProps<any> & OwnProps;

class DisplayClubView extends React.PureComponent<Props> {

    render() {
        const {
            club,
            history
        } = this.props;


        return <>
            <Typography variant={"h3"} gutterBottom>{oc(club).displayName()}</Typography>
            <List>
                <OptionalListItemView label={"Email"} value={oc(club).emailAddress(" - ")}/>

                <OptionalListItemView label={"Phone"} value={oc(club).phoneNumber(" - ")}/>

                <OptionalListItemView label={"Address"} value={oc(club).postalAddress()}
                                      transform={(value) => postalAddressToString(value)}/>

            </List>

            <br/>
            <Typography variant={"h4"} gutterBottom>{"Competitions of Club"}</Typography>
            <ListItem>
                <List>
                    {
                        oc(club).competitions.edges([])
                            .map(edge => edge.node)
                            .map(
                                (competition: CompetitionDto, i: number) =>
                                    <ListItem
                                        button
                                        key={competition.id}
                                        onClick={(event: React.MouseEvent) => {
                                            history.push("/competition/" + (oc(competition).id(-1)))
                                        }}
                                    >
                                        <ListItemText
                                            primary={("" + competition.name + " (" + competition.date + ")")}
                                            secondary={oc(competition.competitionType).name()}
                                        />
                                    </ListItem>
                            )
                    }
                </List>
            </ListItem>
        </>;
    }
}

export default withRouter(DisplayClubView);