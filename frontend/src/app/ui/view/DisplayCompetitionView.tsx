import * as React from "react";
import {useState} from "react";
import {Button, List, Typography} from "@material-ui/core";
import {oc} from "ts-optchain.macro";
import OptionalListItemView from "../components/OptionalListItemView";
import {RouteComponentProps, withRouter} from "react-router";
import CompetitionDto from "../../dto/CompetitionDto";
import {postalAddressToString} from "../../dto/PostalAddressDto";
import Links from "../../navigation/Links";
import {Link} from "react-router-dom";
import CompetitionParticipantsListDialog from "../dialogs/CompetitionParticipantsListDialog";
import {updateStateIfChanged} from "../../Utils";

interface OwnProps {
    competition: CompetitionDto | null | undefined
}

type Props = RouteComponentProps<any> & OwnProps;

interface State {
    participantsDialogOpen: boolean
}

const DisplayCompetitionView: React.FC<Props> = (props: Props) => {
    let {
        competition,
        history
    } = props;

    let [state, setState] = useState<State>({participantsDialogOpen: false});

    function openParticipantsDialog() {
        updateStateIfChanged(state, setState, {participantsDialogOpen: true});
    }

    function closeParticipantsDialog() {
        updateStateIfChanged(state, setState, {participantsDialogOpen: false});
    }

    if (competition == null) {
        competition = {};
    }

    let maxParticipants = oc(competition).maxParticipants();
    let participantsInfo: string | null = (maxParticipants) ? "" + maxParticipants : null;

    if (participantsInfo != null) {
        let currentParticipantsCount = oc(competition).currentParticipants(0);
        participantsInfo = currentParticipantsCount + " / " + participantsInfo
    }

    return (<>
            <Typography variant={"h3"} gutterBottom>{oc(competition).name()}</Typography>
            <Button variant="contained" component={Link}
                    to={Links.ParticipateAtCompetitionById.replace(":id", "" + oc(competition).id(-1))}>Participate</Button>
            <List>

                <OptionalListItemView label={"Date"} value={oc(competition).date()}
                                      transform={(value) => value.toLocaleString()}/>

                <OptionalListItemView label={"Organizer"} value={oc(competition).organizer.displayName()}
                                      onClick={(event: React.MouseEvent) => {
                                          history.push("/club/" + (oc(competition).organizer.id(-1)))
                                      }}/>

                <OptionalListItemView label={"Venue address"} value={oc(competition).venueAddress()}
                                      transform={(value) => postalAddressToString(value)}/>

                <OptionalListItemView label={"Participants"}
                                      value={participantsInfo}
                                      onClick={openParticipantsDialog}/>
                <CompetitionParticipantsListDialog participations={oc(competition).participants({})}
                                                   open={state.participantsDialogOpen}
                                                   fetchMore={() => {
                                                   }} onClose={closeParticipantsDialog}/>

                <OptionalListItemView label={"Competition type"} value={oc(competition).competitionType.name()}/>

                <OptionalListItemView label={"Description"} value={oc(competition).description()}/>

            </List>
        </>
    );
}

export default withRouter(DisplayCompetitionView);
