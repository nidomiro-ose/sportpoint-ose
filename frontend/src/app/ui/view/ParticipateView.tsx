import * as React from "react";
import {useState} from "react";
import {
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    List,
    ListItem,
    ListItemText,
    makeStyles,
    MenuItem,
    Paper,
    Select,
    Theme,
    Typography
} from "@material-ui/core";
import CompetitionDto from "../../dto/CompetitionDto";
import PersonDto from "../../dto/PersonDto";
import {oc} from "ts-optchain.macro";
import ClubDto from "../../dto/ClubDto";
import SportsClassDto from "../../dto/SportsClassDto";
import {RouteComponentProps, withRouter} from "react-router-dom";
import {GQLResult} from "../../services/graphql/GQLResult";
import ParticipationDto from "../../dto/ParticipationDto";
import {useSnackbar, WithSnackbarProps} from "notistack";
import {History} from "history";


const style = makeStyles((theme: Theme) => ({
        rootGridContainer: {
            marginTop: theme.spacing(1),
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        contentContainer: {
            height: "100%"
        },
        buttonContainer: {
            marginTop: theme.spacing(1)
        },
    })
);


interface OwnProps {
    competition: CompetitionDto | null
    person: PersonDto | null

    mutationResult?: GQLResult<ParticipationDto>
    backOnSuccess?: boolean

    preselectSportsClass?: SportsClassDto
    preselectClub?: ClubDto

    commitParticipation(sportsClassId: Number, clubId: Number): void
}

type Props = OwnProps & RouteComponentProps<any>

interface State {
    selectedClubId: Number
    selectedSportsClassId: Number
}


function handleSuccessOrErrorMessages(props: Props, snackbar: WithSnackbarProps, history: History, backOnSuccess: boolean) {

    let errors = oc(props.mutationResult).error.graphQLErrors([]);
    for (let error of errors) {
        let extensions = error.extensions;
        let message = "";

        if (extensions != null && extensions["errorCode"] != null) {
            message = "Error: " + extensions["errorCode"];
        } else {
            message = error.message
        }
        snackbar.enqueueSnackbar(message, {variant: "error"})
    }

    if (oc(props.mutationResult).data.id() != null) {
        snackbar.enqueueSnackbar("Success", {variant: "success"});
        if (backOnSuccess) {
            history.goBack()
        }
    }
}

const ParticipateView: React.FC<Props> = (props: Props) => {

    const classes = style();

    const snackbar = useSnackbar();

    const [state, setState] = useState<State>({
        selectedClubId: oc(props.preselectClub).id(-1),
        selectedSportsClassId: oc(props.preselectSportsClass).id(-1)
    });

    handleSuccessOrErrorMessages(props, snackbar, props.history, oc(props).backOnSuccess(true));


    function updateValue(name: string, value: Number) {
        let newState = {...state};

        if (name === "club") {
            newState.selectedClubId = value;
            if (state.selectedClubId !== newState.selectedClubId) {
                setState(newState)
            }
        } else if (name === "sportsClass") {
            newState.selectedSportsClassId = value;
            if (state.selectedSportsClassId !== newState.selectedSportsClassId) {
                setState(newState)
            }
        }
    }

    function commitParticipation() {
        const {selectedClubId, selectedSportsClassId} = state;

        if (selectedClubId > 0 && selectedSportsClassId > 0) {
            props.commitParticipation(selectedSportsClassId, selectedClubId)
        }
    }

    let competitionDateFormatted = "";
    let competitionDate = oc(props.competition).date();
    if (competitionDate != null) {
        competitionDateFormatted = new Date(competitionDate).toLocaleDateString()
    }

    const xs = 12;
    const sm = 6;
    const md = 4;
    const lg = 3;
    const xl = 2;


    return (<>
        <Typography variant={"h2"}>Participate at Competition</Typography>

        <Grid container spacing={1} className={classes.rootGridContainer}>
            <Grid container spacing={1}>

                <Grid item xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
                    <Paper className={classes.contentContainer}>
                        <List>
                            <ListItem>
                                <ListItemText
                                    primary={("" + oc(props.competition).name("") + " (" + competitionDateFormatted + ")")}
                                    secondary={"Competition"}
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemText
                                    primary={oc(props.competition).competitionType.name("")}
                                    secondary={"Competition-Type"}
                                />
                            </ListItem>
                        </List>
                    </Paper>
                </Grid>

                <Grid item xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
                    <Paper className={classes.contentContainer}>
                        <List>
                            <form autoComplete="off">
                                <ListItem>
                                    <FormControl>
                                        <InputLabel htmlFor="sportsClass">Sports-class</InputLabel>
                                        <Select
                                            value={state.selectedSportsClassId}
                                            onChange={
                                                (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
                                                    console.log("Changed to: ", event.target);
                                                    updateValue(oc(event.target).name(""), event.target.value as Number)
                                                }
                                            }
                                            inputProps={{
                                                name: 'sportsClass',
                                                id: 'sportsClass',
                                            }}
                                        >
                                            {
                                                oc(props.person).sportsClasses.edges([]).map(edge => {
                                                    return (<MenuItem key={edge.node.id}
                                                                      value={edge.node.id}>{edge.node.name}</MenuItem>)
                                                })
                                            }
                                        </Select>
                                        <FormHelperText>Select the sports-class to participate with</FormHelperText>
                                    </FormControl>
                                </ListItem>

                                <ListItem>
                                    <FormControl>
                                        <InputLabel htmlFor="club">Club</InputLabel>
                                        <Select
                                            value={state.selectedClubId}
                                            onChange={
                                                (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
                                                    console.log("Changed to: ", event.target);
                                                    updateValue(oc(event.target).name(""), event.target.value as Number)
                                                }
                                            }
                                            inputProps={{
                                                name: 'club',
                                                id: 'club',
                                            }}
                                        >
                                            {
                                                oc(props.person).clubs.edges([]).map(edge => {
                                                    return (<MenuItem key={edge.node.id}
                                                                      value={edge.node.id}>{edge.node.displayName}</MenuItem>)
                                                })
                                            }
                                        </Select>
                                        <FormHelperText>Select club to participate with</FormHelperText>
                                    </FormControl>
                                </ListItem>
                            </form>

                        </List>
                    </Paper>
                </Grid>

            </Grid>
            <Grid container spacing={1}>
                <Grid item xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
                    <Grid container spacing={1} className={classes.buttonContainer}>
                        <Grid item>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={commitParticipation}>Participate</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained"
                                    color="secondary"
                                    onClick={props.history.goBack}>Cancel</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>

    </>)
};


export default withRouter(ParticipateView);
