import * as React from "react";
import {List, ListItem, ListItemText, Typography} from "@material-ui/core";
import {oc} from "ts-optchain.macro";
import {RouteComponentProps, withRouter} from "react-router";
import CompetitionDto from "../../dto/CompetitionDto";

interface OwnProps {
    competitions: CompetitionDto[] | null | undefined
}

type Props = RouteComponentProps<any> & OwnProps;

class DisplayCompetitionListView extends React.PureComponent<Props> {

    render() {
        const {
            competitions,
            history
        } = this.props;

        let displayCompetitions: CompetitionDto[];
        if (competitions == null) {
            displayCompetitions = [];
        } else {
            displayCompetitions = competitions;
        }

        return (<>
            <Typography variant={"h3"} gutterBottom>{"AllCompetitions"}</Typography>
            <List>
                {
                    displayCompetitions.map(
                        (competition: CompetitionDto, i: number) =>
                            <ListItem
                                button
                                key={competition.id}
                                onClick={(event: React.MouseEvent) => {
                                    history.push("/competition/" + (oc(competition).id(-1)))
                                }}
                            >
                                <ListItemText
                                    primary={("" + competition.name + " (" + competition.date + ")")}
                                    secondary={oc(competition.competitionType).name()}
                                />
                            </ListItem>
                    )
                }
            </List>
        </>);
    }
}

export default withRouter(DisplayCompetitionListView);