import * as React from "react";
import {Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import Links from "../../navigation/Links";


const HomeView: React.FC = () => {
    return (
        <div className="App-header">
            <Typography variant={"h2"}>Home</Typography>

            <NavLink to={Links.Root}>Home</NavLink><br/>

            <NavLink to={Links.MyProfile}>My Profile</NavLink><br/>

            <NavLink to={Links.AllCompetitions}>Competitions</NavLink><br/>

            <NavLink to={Links.CompetitionById.replace(":id", "1")}>Competition 1</NavLink><br/>

            <NavLink to={Links.CompetitionById.replace(":id", "2")}>Competition 2</NavLink><br/>


        </div>
    )
};


export default HomeView;
