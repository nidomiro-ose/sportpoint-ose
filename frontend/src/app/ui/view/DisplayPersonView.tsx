import {List, ListItem, ListItemText, Typography} from "@material-ui/core";
import * as React from "react";
import {oc} from "ts-optchain.macro";
import {postalAddressToString} from "../../dto/PostalAddressDto";
import OptionalListItemView from "../components/OptionalListItemView";
import {RouteComponentProps, withRouter} from "react-router";
import PersonDto from "../../dto/PersonDto";
import SportsClassDto from "../../dto/SportsClassDto";


interface OwnProps {
    person: PersonDto | null | undefined
}

type Props = RouteComponentProps<any> & OwnProps;

class DisplayPersonView extends React.PureComponent<Props> {

    render() {
        const {
            person,
            history
        } = this.props;

        return <>
            <Typography variant={"h3"} gutterBottom>{oc(person).displayName()}</Typography>
            <List>
                <OptionalListItemView label={"Birthday"} value={oc(person).birthDate(" - ")}/>

                <OptionalListItemView label={"Email"} value={oc(person).emailAddress(" - ")}/>

                <OptionalListItemView label={"Phone"} value={oc(person).phoneNumber(" - ")}/>

                <OptionalListItemView label={"Address"} value={oc(person).postalAddress()}
                                      transform={(value) => postalAddressToString(value)}/>

            </List>

            <br/>
            <Typography variant={"h4"} gutterBottom>{"Sports-classes"}</Typography>
            <ListItem>
                <List>
                    {
                        oc(person).sportsClasses.edges([])
                            .map(edge => edge.node)
                            .map(
                                (sportsClass: SportsClassDto, i: number) =>
                                    <ListItem
                                        button
                                        key={sportsClass.id}
                                        onClick={(event: React.MouseEvent) => {
                                            history.push("/sports-class/" + (oc(sportsClass).id(-1)))
                                        }}
                                    >
                                        <ListItemText
                                            primary={sportsClass.name}
                                        />
                                    </ListItem>
                            )
                    }
                </List>
            </ListItem>
        </>;
    }
}

export default withRouter(DisplayPersonView);