import * as React from "react";
import {Button, Typography} from "@material-ui/core";
import {gql} from "apollo-boost";
import {Mutation, MutationResult} from "react-apollo";


const CREATE_EXAMPLE_DATA_MUTATION = gql`
    mutation CreateExampleData {
        result: createExampleData
    }
`;


const AdminPageView: React.FC = () => {


    return (
        <div className="App-header">
            <Typography variant={"h2"}>AdminPage</Typography>
            <br/>

            <Mutation<Boolean> mutation={CREATE_EXAMPLE_DATA_MUTATION}>
                {
                    (postMutation: any, result: MutationResult<Boolean>) => {


                        if (result.data == null) {
                            return <Button href={""} onClick={postMutation}>Create Example Data</Button>
                        } else {
                            return <p> Execution was {result.data ? "" : "not "}successful</p>
                        }

                    }
                }
            </Mutation>


        </div>
    )
};


export default AdminPageView;
