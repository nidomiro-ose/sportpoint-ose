import * as React from "react";
import FeedValuesDto, {EdgeDto} from "../../dto/FeedValuesDto";
import ParticipationDto from "../../dto/ParticipationDto";
import {Dialog, DialogContent, DialogTitle, List, ListItem, ListItemText} from "@material-ui/core";
import {oc} from "ts-optchain";
import {Link} from "react-router-dom";
import Links from "../../navigation/Links";


interface Props {
    participations: FeedValuesDto<ParticipationDto>,
    open: boolean,

    fetchMore(): void,

    onClose(): void
}

const CompetitionParticipantsListDialog: React.FC<Props> = (props: Props) => {

    //const theme = useTheme();
    //const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const fullScreen = false; // Think about closing, ...


    return (<>
        <Dialog
            open={props.open}
            onClose={props.onClose}
            fullScreen={fullScreen}
            aria-labelledby="competition-participants-dialog-title">

            <DialogTitle id="competition-participants-dialog-title">{"Participations"}</DialogTitle>
            <DialogContent>
                <List>
                    {
                        oc(props.participations).edges([]).map(
                            (edge: EdgeDto<ParticipationDto>, i: number) => {

                                let participation = edge.node;

                                let sportsClass = oc(participation).participatingSportsClass.name();
                                sportsClass = (sportsClass) ? " (" + sportsClass + ")" : "";

                                return (<>
                                    <ListItem
                                        button
                                        key={participation.id}

                                        component={Link}
                                        to={Links.PersonById.replace(":id", "" + oc(participation).participant.id(-1))}
                                    >
                                        <ListItemText
                                            primary={oc(participation).participant.displayName("")}
                                            secondary={oc(participation).participatingClub.displayName("") + sportsClass}
                                        />
                                    </ListItem>
                                </>)


                            }
                        )
                    }
                </List>
            </DialogContent>
        </Dialog>
    </>)
};

export default CompetitionParticipantsListDialog;
