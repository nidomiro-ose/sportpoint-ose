import React, {ComponentType} from 'react';
import ReactDOM from 'react-dom';
import App from './app/App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";
import ApolloProvider from "react-apollo/ApolloProvider";
import {createApolloClient} from "./app/ApolloInit";
import {KeycloakProvider} from "react-keycloak";
import keycloak, {keycloakAuthTokenProvider} from "./app/security/KeycloakInit";
import LoadingSpinnerView from "./app/ui/components/LoadingSpinnerView";
import {forceCast} from "./app/Utils";
import UICustomizations from "./UICutomizations";


ReactDOM.render(
    <KeycloakProvider keycloak={keycloak}
                      initConfig={{onLoad: 'check-sso'}}
                      LoadingComponent={forceCast<ComponentType>(<LoadingSpinnerView message={"Initializing App"}/>)}
    >
        <ApolloProvider client={
            createApolloClient(keycloakAuthTokenProvider)
        }>
            <BrowserRouter basename={process.env.PUBLIC_URL}>
                <UICustomizations>
                    <App/>
                </UICustomizations>
            </BrowserRouter>
        </ApolloProvider>
    </KeycloakProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
