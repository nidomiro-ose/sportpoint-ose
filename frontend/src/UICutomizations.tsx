import * as React from "react";
import {SnackbarProvider} from "notistack";
import {ThemeProvider} from "@material-ui/styles";
import {createMuiTheme, CssBaseline} from "@material-ui/core";


const theme = createMuiTheme({
    props: {
        MuiButtonBase: {
            disableRipple: true, // No more ripple, on the whole application
        },
    },
});


interface Props {
    children: JSX.Element[] | JSX.Element
}

const UICustomizations: React.FC<Props> = (props: Props) => {
    return (<>
        <ThemeProvider theme={theme}>
            <SnackbarProvider maxSnack={3}>
                <CssBaseline/>
                {props.children}
            </SnackbarProvider>
        </ThemeProvider>
    </>)
};

export default UICustomizations;